#!/bin/bash

################################################################################
# Copyright (c) 2021 ITK Engineering GmbH
#               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script prepares the thirdParty dependencies
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.."

REPO_ROOT="$PWD"

# requires by OSI conan build
if [[ -z "$WORKSPACE" ]]; then
  export WORKSPACE="$REPO_ROOT/.."
fi

# Set python command depending on OS
if [[ "${OSTYPE}" = "msys" ]]; then
  PYTHON_COMMAND="${PYTHON_WINDOWS_EXE}"
else
  PYTHON_COMMAND=python3
fi

# Detect system settings and create a conan profile
"$PYTHON_COMMAND" -m conans.conan profile new default --detect --force
"$PYTHON_COMMAND" -m conans.conan profile update settings.compiler.libcxx=libstdc++11 default

# export conan recipes of thirdParty dependencies to conans local cache
"$PYTHON_COMMAND" -m conans.conan export $REPO_ROOT/utils/ci/conan/recipe/osi openpass/testing
"$PYTHON_COMMAND" -m conans.conan export $REPO_ROOT/utils/ci/conan/recipe/fmi openpass/testing

"$PYTHON_COMMAND" -m conans.conan install FMILibrary/2.0.3@openpass/testing --build --install-folder="$REPO_ROOT/../deps" -g deploy
"$PYTHON_COMMAND" -m conans.conan install protobuf/3.20.0@ --build --install-folder="$REPO_ROOT/../deps" -g deploy -g cmake_paths -o protobuf:shared=True
mv $REPO_ROOT/../deps/protobuf $REPO_ROOT/../deps/protobuf-shared   # rename the deployed protobuf folder as protobuf-shared (to distinguish between protobuf static and shared)
"$PYTHON_COMMAND" -m conans.conan install protobuf/3.20.0@ --build --install-folder="$REPO_ROOT/../deps" -g deploy -o protobuf:shared=False
"$PYTHON_COMMAND" -m conans.conan install OSI/3.5.0@openpass/testing --build --install-folder="$REPO_ROOT/../deps" -g deploy

