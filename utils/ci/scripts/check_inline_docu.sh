#!/bin/bash

################################################################################
# Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script checks the inline documentation using doxygen
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../.." || exit 1

doxy_version="$(doxygen --version)"
echo "Doxygen Version $doxy_version"
doxygen Doxyfile

# version comparison - https://unix.stackexchange.com/a/285928
# under CC BY-SA 4.0 - https://creativecommons.org/licenses/by-sa/4.0/

# dealing with bugs in Doxygen 1.8.17 (or earlier)
# bug description - https://github.com/doxygen/doxygen/issues/7411
# fixed in 1.8.18 - https://github.com/doxygen/doxygen/pull/7483
if [ "$(printf '%s\n' "1.8.17" "$doxy_version" | sort -V | head -n1)" = "$doxy_version" ]; then
     echo "Filtering Doxygen warnings \"return type of member ... is not documented\" (see https://github.com/doxygen/doxygen/issues/7411)"
     sed -i '/warning: return type of member/d' DoxygenWarningLog.txt
fi

# dealing with bugs in Doxygen 1.9.4 (or earlier)
# bug description - https://github.com/doxygen/doxygen/issues/8091
# fixed in 1.9.5  - https://github.com/doxygen/doxygen/commit/01ded6f6963d4798ce1338246d6d946dbc3922d9
if [ "$(printf '%s\n' "1.9.4" "$doxy_version" | sort -V | head -n1)" = "$doxy_version" ]; then
     echo "Filtering Doxygen warnings \"Member OpenPASS_ ... is not documented\" (see https://github.com/doxygen/doxygen/issues/8091)"
     sed -i '/warning: Member OpenPASS_.*is not documented/d' DoxygenWarningLog.txt
fi

if [ -s DoxygenWarningLog.txt ]
then
     echo "ERROR: Doxygen warnings"
     cat DoxygenWarningLog.txt
     exit 1
else
     exit 0
fi
