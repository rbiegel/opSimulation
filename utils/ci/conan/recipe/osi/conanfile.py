################################################################################
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#               2021 ITK Engineering GmbH
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building OSI with Conan
################################################################################

from os import environ
from conans import ConanFile, CMake, tools, errors

class OsiConan(ConanFile):
    name = "OSI"
    version = "3.5.0"
    license = "Mozilla Public License 2.0"
    author = "Michael Scharfenberg michael.scharfenberg@itk-engineering.de"
    url = "https://github.com/OpenSimulationInterface"
    description = "The Open Simulation Interface (OSI) is a specification for interfaces between models and components of a distributed simulation"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": True, "fPIC": True, "protobuf:shared": True}
    generators = "cmake"
    #requires = "protobuf/3.20.0"     this should be used when distinguishing static and shared protobuf is possible via conan

    def configure(self):
        if self.settings.os == "Linux": #For Windows it is required that Protobuf is installed via pacman or pip in MSYS2
            self.generators = "cmake_find_package", "cmake_paths"
        if not environ.get("WORKSPACE"):
            raise errors.ConanInvalidConfiguration("WORKSPACE environment variable has to be set")

    def source(self):
        git = tools.Git()
        git.clone("https://github.com/OpenSimulationInterface/open-simulation-interface.git", "v3.5.0", "--recursive")
        self.run("find . -maxdepth 1 -name '*.proto' -exec sed -i '2i option cc_enable_arenas = true;' {} \;", win_bash=True)

    def build(self):
        cmake = CMake(self)
        cmake.configure(defs={
                            "CMAKE_INSTALL_PREFIX": "./temp-deploy",
                            "CMAKE_PREFIX_PATH": f"{environ.get('WORKSPACE')}/deps/protobuf-shared"
                        })
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", src="temp-deploy")

    def package_info(self):
        self.cpp_info.libs = ["osi3"]

