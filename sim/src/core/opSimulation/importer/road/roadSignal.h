/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/roadSignalInterface.h"

//-----------------------------------------------------------------------------
//! Class representing a road signal
//-----------------------------------------------------------------------------
class RoadSignal : public RoadSignalInterface
{
public:

    /// @brief RoadSignal constructor
    /// @param road     Pointer to the road interface
    /// @param signal   Specification of road signal
    RoadSignal(RoadInterface *road, const RoadSignalSpecification signal) :
        road{road},
        signal{signal}
    {}

    std::string GetCountry() const override;
    std::string GetCountryRevision() const override;
    std::string GetType() const override;
    std::string GetSubType() const override;
    std::string GetId() const override;
    std::optional<double> GetValue() const override;
    RoadSignalUnit GetUnit() const override;
    std::string GetText() const override;
    double GetS() const override;
    double GetT() const override;

    //-----------------------------------------------------------------------------
    //! Returns the road from which this section is a part of.
    //!
    //! @return                         Road from which this section is a part of
    //-----------------------------------------------------------------------------
    RoadInterface *GetRoad()
    {
        return road;
    }

    bool IsValidForLane(int laneId) const override;
    double GetHeight() const override;
    double GetWidth() const override;
    virtual double GetPitch() const;
    virtual double GetRoll() const;
    bool GetIsDynamic() const override;

    std::vector<std::string> GetDependencies() const override;

    double GetZOffset() const override;

    bool GetOrientation() const override;

    double GetHOffset() const override;

private:
    RoadInterface* road;
    const RoadSignalSpecification signal;

public:
    RoadSignal(const RoadSignal&) = delete;
    RoadSignal(RoadSignal&&) = delete;
    RoadSignal& operator=(const RoadSignal&) = delete;
    RoadSignal& operator=(RoadSignal&&) = delete;
    ~RoadSignal() override = default;
};
