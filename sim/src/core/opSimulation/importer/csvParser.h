/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <vector>
#include <string>

/// This class contains the information about parsing a CSV file
class CSVParser
{
public:
    CSVParser() = default;
    ~CSVParser() = default;

    /// @brief Parse a csv file
    /// @param file Path of the file to parse
    void ParseFile(const std::string& file);

    /// @brief Get the number of lines in the file to be parsed
    /// @return Number of lines in the file
    size_t GetNumberOfLines() const;

    /// @brief Parse the file at the given row and column
    /// @param row      Row number
    /// @param column   Column number
    /// @return Parsed information in the form of string
    std::string GetEntryAt(size_t row, size_t column) const;

private:
    std::vector<std::string> ParseLine(const std::string& line);
    std::vector<std::vector<std::string>> table;
};
