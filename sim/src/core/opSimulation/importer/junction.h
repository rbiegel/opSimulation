/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#ifndef JUNCTION_H
#define JUNCTION_H


#include "include/roadInterface/junctionInterface.h"
#include "connection.h"

#include <list>
#include <map>
#include <cstdio>


//! This class represents a junction
class Junction : public JunctionInterface
{
public:

    /// @brief Junction constructor
    /// @param id Id of the junction
    Junction(const std::string &id)
    {
        this->id = id;
    }
    ~Junction() override = default;  //!< default destructor

    ConnectionInterface* AddConnection(std::string id,std::string incomingRoadId, std::string connectingRoadId, ContactPointType contactPoint) override;

    void AddPriority(Priority priority) override;

    std::map<std::string, ConnectionInterface*> GetConnections() const override;

    const std::vector<Priority>& GetPriorities() const override;

    std::string GetId() const override;

private:
    std::string id;
    std::map<std::string, ConnectionInterface*> connections;
    std::vector<Priority> priorities;
};

#endif // JUNCTION_H
