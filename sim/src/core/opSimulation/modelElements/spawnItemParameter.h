/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  spawnItemParameter.h
//! @brief This file contains the internal representation of parameter set of a
//!        newly created agent spawn item.
//-----------------------------------------------------------------------------

#pragma once

#include "include/spawnPointInterface.h"

namespace core
{

/// This class represents the functionality of a spawn item (agent) which will be spawned by a spawn point
class SpawnItemParameter : public SpawnItemParameterInterface
{
public:
    SpawnItemParameter() = default;
    SpawnItemParameter(const SpawnItemParameter&) = delete;
    SpawnItemParameter(SpawnItemParameter&&) = delete;
    SpawnItemParameter& operator=(const SpawnItemParameter&) = delete;
    SpawnItemParameter& operator=(SpawnItemParameter&&) = delete;
    virtual ~SpawnItemParameter() = default;

    void SetPositionX(double positionX) override
    {
        this->positionX = positionX;
    }

    void SetPositionY(double positionY) override
    {
        this->positionY = positionY;
    }

    void SetVelocity(double velocity) override
    {
        this->velocity = velocity;
    }

    void SetAcceleration(double acceleration) override
    {
        this->acceleration = acceleration;
    }

    void SetGear(double gear) override
    {
        this->gear = gear;
    }

    void SetYaw(double yawAngle) override
    {
        this->yawAngle = yawAngle;
    }

    void SetNextTimeOffset(int nextTimeOffset) override
    {
        this->nextTimeOffset = nextTimeOffset;
    }

    void SetIndex(int index) override
    {
        this->index = index;
    }

    void SetVehicleModel(std::string vehicleModel) override
    {
        this->vehicleModel = vehicleModel;
    }

    double GetPositionX() const override
    {
        return positionX;
    }

    double GetPositionY() const override
    {
        return positionY;
    }

    double GetVelocity() const override
    {
        return velocity;
    }

    double GetAcceleration() const override
    {
        return acceleration;
    }

    double GetYaw() const override
    {
        return yawAngle;
    }

    /** 
     * @brief Returns the next time when the agent will be spawned
     * 
     * @return Time offset counted from the current scheduling time
     */
    int GetNextTimeOffset() const
    {
        return nextTimeOffset;
    }

    /** 
     * @brief Returns the index of the agent to be spawned next within the configured agent array
     * 
     * @return Index within configured agent array
     */
    int GetIndex() const
    {
        return index;
    }

    std::string GetVehicleModel() const override
    {
        return vehicleModel;
    }

private:
    double positionX = 0.0;
    double positionY = 0.0;
    double velocity = 0.0;
    double acceleration = 0.0;
    double gear = 0.0;
    double yawAngle = 0.0;

    int nextTimeOffset = -1;
    int index = 0;

    std::string vehicleModel = "";
};

} // namespace core


