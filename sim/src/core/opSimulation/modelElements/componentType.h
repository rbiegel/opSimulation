/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  componentType.h
//! @brief This file contains the internal representation of the type of a
//!        model component as given by the configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <map>

#include "include/modelInterface.h"
#include "include/parameterInterface.h"

namespace core {

/// This class represents the type of a model component as given by the configuration
class ComponentType
{
public:
    //! ComponentType constructor
    //!
    //! @param[in] name             Name of this component
    //! @param[in] isInit           Indicates if component is an init module
    //! @param[in] priority         Priority of this component
    //! @param[in] offsetTime       Offset time of this component
    //! @param[in] responseTime     Response time of this component
    //! @param[in] cycleTime        Cycle time of this component
    //! @param[in] modelLibrary     Name of the model library
    ComponentType(std::string name,
                                 bool isInit,
                                 int priority,
                                 int offsetTime,
                                 int responseTime,
                                 int cycleTime,
                                 const std::string &modelLibrary) :
        name(name),
        isInit(isInit),
        priority(priority),
        offsetTime(offsetTime),
        responseTime(responseTime),
        cycleTime(cycleTime),
        modelLibrary(modelLibrary){};

    ComponentType(const ComponentType &) = default; ///< Default copy constructor
    ComponentType(ComponentType &&) = delete;       ///< Delete move constructor

    /// @brief Default copy assignment operator
    /// @return Returns a reference to the component type
    ComponentType &operator=(const ComponentType &) = default;

    /// @brief Default move assignment operator
    /// @return Returns a reference to the component type
    ComponentType &operator=(ComponentType &&) = default;
    ~ComponentType() = default;

    /// Adds the provided channel ID to the stored list of input
    /// channel IDs.
    ///
    /// @param[in]  localLinkId     Corresponds to "id" of "ComponentTypeInput"
    /// @param[in]  channelId       ID of the channel to add
    /// @return Flag if adding the channel ID was successful
    bool AddInputLink(int localLinkId, int channelId);

    //! Adds the provided channel ID to the stored list of output
    //! channel IDs.
    //!
    //! @param[in]  localLinkId     Corresponds to "id" of "ComponentTypeOutput"
    //! @param[in]  channelId       ID of the channel to add
    //! @return Flag if adding the channel ID was successful
    bool AddOutputLink(int localLinkId, int channelId);

    //! Returns the map of IDs to stored input channel IDs.
    //!
    //! @return Map of IDs to stored input channel IDs
    const std::map<int, int> &GetInputLinks() const
    {
        return inputs;
    }

    //! Returns the map of IDs to stored output channel IDs.
    //!
    //! @return Map of IDs to stored output channel IDs
    const std::map<int, int> &GetOutputLinks() const
    {
        return outputs;
    }

    //! Checks if this component is configured as init module
    //!
    //! @return True if this component is an init module
    bool GetInit() const
    {
        return isInit;
    }

    //! Retrieves priority of this component
    //!
    //! @return ComponentType priority
    int GetPriority() const
    {
        return priority;
    }

    //! Retrieves offset time of this component
    //!
    //! @return Offset time of this component
    int GetOffsetTime() const
    {
        return offsetTime;
    }

    //! Retrieves response time of this component
    //!
    //! @return Response time of this component
    int GetResponseTime() const
    {
        return responseTime;
    }

    //! Retrieves cycle time of this component
    //!
    //! @return Cycle time of this component
    int GetCycleTime() const
    {
        return cycleTime;
    }

    //! Retrieves the name of the model library
    //!
    //! @return Name of the model library
    std::string GetModelLibrary()
    {
        return modelLibrary;
    }

    //! Retrieves the parameters of the module
    //!
    //! @return Parameters of the module
    const openpass::parameter::ParameterSetLevel1 &GetModelParameters()
    {
        return this->parameters;
    }

    //! Sets the parameters of the module
    //!
    //! @param[in] parameters   Parameters of the module
    void SetModelParameter(const openpass::parameter::ParameterSetLevel1 parameters)
    {
        this->parameters = parameters;
    }

    //! Retrieves the name of this component
    //!
    //! @return Name of this component
    std::string GetName() const
    {
        return name;
    }


private:
    std::string name = "";
    bool isInit = false;
    int priority = -999;
    int offsetTime = -999;
    int responseTime = -999;
    int cycleTime = -999;
    std::string modelLibrary = "";
    std::map<int, int> inputs;
    std::map<int, int> outputs;
    openpass::parameter::ParameterSetLevel1 parameters{};
};

} // namespace core
