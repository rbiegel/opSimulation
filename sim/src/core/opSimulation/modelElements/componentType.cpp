/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "componentType.h"

#include <algorithm>
#include <iostream>

#include "common/log.h"
#include "modelElements/parameters.h"

/// @cond FIXEX_DOXYGEN_ISSUE_WITH_REDEFINITION_OF_COMPONENT_TYPE
// Doxygen 1.9.1 has issues parsing core::ComponentType, colliding with the
// enum class ComponentType definition within globalDefinitions.h
//
// see also:
// https://stackoverflow.com/questions/11678567/doxygen-warning-documented-function-not-declared-or-defined
//
// Without this condition doxygen reports
// warning: documented symbol 'bool core::ComponentType::AddInputLink' was not declared or defined.
// warning: documented symbol 'bool core::ComponentType::AddOutputLink' was not declared or defined.

bool core::ComponentType::AddInputLink(int localLinkId, int channelId)
{
    if (!inputs.insert({localLinkId, channelId}).second)
    {
        LOG_INTERN(LogLevel::Error) << "input must be unique";
        return false;
    }

    return true;
}

bool core::ComponentType::AddOutputLink(int localLinkId, int channelId)
{
    if (!outputs.insert({localLinkId, channelId}).second)
    {
        LOG_INTERN(LogLevel::Error) << "output must be unique";
        return false;
    }

    return true;
}

/// @endcond