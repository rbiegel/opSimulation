/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <list>
#include <QXmlStreamWriter>

#include "include/observationInterface.h"

//! This class represents the RandomSeed and general statistics of the invocation
class RunStatistic
{
public:
    //! Displays the reason why the simulation stopped.
    //! Currently only due to time out.
    enum class StopReason
    {
        DueToTimeOut
    };

    //! RunStatistic constructor
    //!
    //! @param[in] randomSeed   Random seed used for this invocation
    RunStatistic(std::uint32_t randomSeed);

    //! Adds the reason why the simulation stopped
    //!
    //! @param[in] time     Stop time
    //! @param[in] reason   Reason why the simulation stopped
    void AddStopReason(int time, StopReason reason);

    //! Writes the RunStatistic information into xml file
    //!
    //! @param[in] fileStream   Stream which writes into xml file
    void WriteStatistics(QXmlStreamWriter* fileStream);

    // general
    int StopTime = -1;                                  //!< This stays on UNDEFINED_NUMBER, if due time out -> replace in c#
    bool EgoCollision = false;                          //!< Flag which shows whether the ego agent was involved in an accident
    std::map<std::string, double> distanceTraveled{};   //!< travel distance per agent
    double VisibilityDistance = -999.0;                 //!< Visibility distance of world in current run (defined in simulationConfig.xml)

    //! Convert boolean to string
    //!
    //! @param[in] b    Boolean to convert
    //! @return Boolean as string
    static QString BoolToString(bool b);

    ~RunStatistic() = default;

private:
    std::uint32_t _randomSeed;
    std::vector<int> _followerIds;

    static const QString StopReasonsStrings[];
    int _stopReasonIdx = static_cast<int>(StopReason::DueToTimeOut);
}; // class RunStatistic


