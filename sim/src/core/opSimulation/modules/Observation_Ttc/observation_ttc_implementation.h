/********************************************************************************
 * Copyright (c) 2016 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


#ifndef OBSERVATION_TTC_IMPLEMENTATION_H
#define OBSERVATION_TTC_IMPLEMENTATION_H

#include <string>
#include <tuple>
#include <QFile>
#include <QTextStream>
#include "agentInterface.h"
#include "observationInterface.h"

/**
* \addtogroup CoreModules_Basic openPASS CoreModules basic
* @{
* \addtogroup Observation_Ttc
* @{
* \brief logs the time to collision (ttc) of all agents
*
* This component logs the time to collision (ttc) of all agents.
*
* The file format is csv to easily access ttc data.
* Output data is the minimal ttc and the ttc at each time step of each agent.
*
* \section Observation_Ttc_Inputs Inputs
* none
*
* \section Observation_Ttc_Outputs Outputs
* none
*
* \section Observation_Ttc_ConfigParameters Parameters to be specified in runConfiguration.xml
* type | id | meaning | corresponding external paramter
* -----|----|---------|----------------------------------
* string | 0 | folder where simulation output shall be stored      | foldername for the result file
* string | 1 | filename for temporary simulation output            | file name for the temporary file
* string | 2 | filename for final simulation output (*.xml)        | file name for final result file
*
* @}
* @} */

/*!
 * \brief logs the time to collision (ttc) of all agents
 *
 * This component logs the time to collision (ttc) of all agents.
 *
 * The file format is csv to easily access ttc data.
 * Output data is the minimal ttc and the ttc at each time step of each agent.
 *
 * \ingroup Observation_Ttc
 */
class Observation_Ttc_Implementation : public ObservationInterface
{
public:
    /// Name of the current component
    const std::string COMPONENTNAME = "Observation_Ttc";

    /**
     * @brief Observation_Ttc_Implementation constructor
     *
     * @param[in]   stochastics             Pointer to the stochastics
     * @param[in]   world                   Pointer to the world
     * @param[in]   parameters              Pointer to the parameters of the module
     * @param[in]   callbacks               Pointer to the callbacks
     */
    Observation_Ttc_Implementation(StochasticsInterface *stochastics,
                                   WorldInterface *world,
                                   const ParameterInterface *parameters,
                                   const CallbackInterface *callbacks);
    Observation_Ttc_Implementation(const Observation_Ttc_Implementation &) = delete;
    Observation_Ttc_Implementation(Observation_Ttc_Implementation &&) = delete;
    Observation_Ttc_Implementation &operator=(const Observation_Ttc_Implementation &) = delete;
    Observation_Ttc_Implementation &operator=(Observation_Ttc_Implementation &&) = delete;
    ~Observation_Ttc_Implementation() override = default;

    //-----------------------------------------------------------------------------
    //! Called by framework in opSimulationManager before each simulation run starts
    //-----------------------------------------------------------------------------
    void OpSimulationManagerPreHook() override
    {
        return;   //dummy
    }

    //-----------------------------------------------------------------------------
    //! Called by framework in opSimulationManager after each simulation run ends
    //!
    //! @param[in]     filename      Name of file containing the simulation run results from the simulation
    //-----------------------------------------------------------------------------

    void OpSimulationManagerPostHook(const std::string &filename) override
    {
        Q_UNUSED(filename)
    } //dummy

    //-----------------------------------------------------------------------------
    //! Called by framework in simulation before all simulation runs start
    //!
    //! @param[in]     path          Directory where simulation results will be stored
    //-----------------------------------------------------------------------------

    void OpSimulationPreHook(const std::string &path) override;

    //-----------------------------------------------------------------------------
    //! Called by framework in simulation before each simulation run starts.
    //-----------------------------------------------------------------------------
    void OpSimulationPreRunHook() override;

    //-----------------------------------------------------------------------------
    //! Called by framework in simulation at each time step.
    //! Observation module can indicate end of simulation run here.
    //!
    //! @param[in]     time        Current scheduling time
    //! @param[in,out] runResult   Reference to run result
    //-----------------------------------------------------------------------------
    void OpSimulationUpdateHook(int time, RunResultInterface &runResult) override;

    //-----------------------------------------------------------------------------
    //! Called by framework in simulation after each simulation run ends.
    //! Observation module can observe the current simulation run here.
    //!
    //! @param[in]     runResult   Reference to run result
    //-----------------------------------------------------------------------------
    void OpSimulationPostRunHook(const RunResultInterface &runResult) override;

    //-----------------------------------------------------------------------------
    //! Called by framework in simulation after all simulation runs end.
    //-----------------------------------------------------------------------------

    void OpSimulationPostHook() override;

    //-----------------------------------------------------------------------------
    //! Called by framework in the simulation after all simulation runs end to
    //! transfer the observation module results to the opSimulationManager.
    //!
    //! @return                      File to be transferred

    const std::string OpSimulationResultFile() override
    {
        return "";   //dummy
    }

    //-----------------------------------------------------------------------------
    //! Finds the ID of the agent that is exactly in front of the agent.
    //!
    //! @param  ownID   Id of the agent
    //! @return Id of agent in front
    int findFrontAgentID(int ownID);

private:

    /**
    * \addtogroup Observation_Ttc
    * @{
    *    @name External Parameters
    *       @{*/

    //! foldername for the result file
    std::string Par_resultFolderName;
    //! file name for the temporary file
    std::string Par_tempFileName;
    //! file name for final result file
    std::string Par_finalFileName;

    /** @} @} */

    //! A map of all agents and their ttc at every time.
    std::map<int, std::map<int, double>> agentsTtc;
    //! A map to save the minimal Ttc of an agent.
    std::map<int, double> agentsMinTtc;
    //! time vector
    std::vector<int> timeVector;
    //! full path name of result file
    std::string fullResultFilePath;
    //! ID of each run
    int runID = 0;
};

#endif // OBSERVATION_TTC_IMPLEMENTATION_H
