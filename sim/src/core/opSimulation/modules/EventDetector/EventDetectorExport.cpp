/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  EventDetectorExport.cpp */
//-----------------------------------------------------------------------------

#include "EventDetectorExport.h"

//Different detectors
#include "CollisionDetector.h"
#include "ConditionalEventDetector.h"

/// The version of the current module - has to be incremented manually
const std::string version = "0.0.1";
static const CallbackInterface* Callbacks = nullptr;

/**
 * @brief   dll-function to obtain the version of the current module
 * 
 * @return  Version of the current module
 */
extern "C" EVENT_DETECTOR_SHARED_EXPORT const std::string& OpenPASS_GetVersion()
{
    return version;
}

/**
 * @brief   dll-function to create a CollisionDetector instance
 * 
 * @param[in]   world                   Pointer to the world
 * @param[in]   eventNetwork            Instance of the internal event logic
 * @param[in]   callbacks               Pointer to the callbacks
 * @param[in]   stochastics             Pointer to the stochastics
 * 
 * @return  A pointer to the created module instance.
 */
extern "C" EVENT_DETECTOR_SHARED_EXPORT EventDetectorInterface* OpenPASS_CreateCollisionDetectorInstance(
    WorldInterface* world,
    core::EventNetworkInterface* eventNetwork,
    const CallbackInterface* callbacks,
    StochasticsInterface* stochastics)
{
    Callbacks = callbacks;

    try
    {
        return static_cast<EventDetectorInterface*>(new CollisionDetector(world,
                                                                          eventNetwork,
                                                                          callbacks,
                                                                          stochastics));
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return nullptr;
    }
}

/**
 * @brief   dll-function to create a ConditionalEventDetector instance
 * 
 * @param[in]   world                       Pointer to the world
 * @param[in]   eventDetectorInformation    Event specific information collected from an openSCENARIO story
 * @param[in]   eventNetwork                Instance of the internal event logic
 * @param[in]   callbacks                   Pointer to the callbacks
 * @param[in]   stochastics                 Pointer to the stochastics
 * 
 * @return  A pointer to the created module instance.
 */
extern "C" EVENT_DETECTOR_SHARED_EXPORT EventDetectorInterface* OpenPASS_CreateConditionalDetectorInstance(
    WorldInterface* world,
    const openScenario::ConditionalEventDetectorInformation& eventDetectorInformation,
    core::EventNetworkInterface* eventNetwork,
    const CallbackInterface* callbacks,
    StochasticsInterface* stochastics)
{
    Callbacks = callbacks;

    try
    {
        return static_cast<EventDetectorInterface*>(new ConditionalEventDetector(world,
                                                                                 eventDetectorInformation,
                                                                                 eventNetwork,
                                                                                 callbacks,
                                                                                 stochastics));
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return nullptr;
    }
}

/**
 * @brief   dll-function to destroy/delete an instance of the module.
 * 
 * @param[in]   implementation  The instance that should be freed
 */
extern "C" EVENT_DETECTOR_SHARED_EXPORT void OpenPASS_DestroyInstance(EventDetectorInterface* implementation)
{
    delete implementation;
}

