/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  SpeedActionManipulator.h
*   \brief This manipulator adjusts the acceleration of an actor based on different variables
*/
//-----------------------------------------------------------------------------

#pragma once

#include "ManipulatorCommonBase.h"
#include "common/openScenarioDefinitions.h"

//-----------------------------------------------------------------------------
/** \brief This class emits the SpeedActionEvent.
* 	\details    This class emits the SpeedActionEvent which allows for an acceleration manipulation of actors.
*
* 	\ingroup Manipulator
*/
//-----------------------------------------------------------------------------
class SpeedActionManipulator : public ManipulatorCommonBase
{
public:
    /**
     * @brief SpeedActionManipulator constructor
     *
     * @param[in]   world           Pointer to the world
     * @param[in]   eventNetwork    Pointer to event network. Necessary for extracting the TriggeringEvent
     * @param[in]   callbacks       Pointer to the callbacks
     * @param[in]   action          OSC Speed action (Longitudinal action)
     * @param[in]   eventName       Name of the event used for identification
     */
    SpeedActionManipulator(WorldInterface *world,
                                                 core::EventNetworkInterface *eventNetwork,
                                                 const CallbackInterface *callbacks,
                                                 const openScenario::SpeedAction action,
                                                 const std::string &eventName);

    virtual ~SpeedActionManipulator() = default;

    /*!
    * \brief Triggers the functionality of this class
    *
    * \details Trigger gets called each cycle timestep.
    * This function is repsonsible for creating events
    *
    * @param[in]     time    Current time.
    */
    void Trigger(int time) override;

private:
    EventContainer GetEvents() override;

    const openScenario::SpeedAction action;
};
