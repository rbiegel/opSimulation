/********************************************************************************
 * Copyright (c) 2020-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  observation_entityRepository.cpp */
//-----------------------------------------------------------------------------

#include "observation_entityRepository.h"
#include "observation_entityRepositoryImplementation.h"

class DataBufferReadInterface;

const std::string Version = "0.2.0";    //!< The version of the current module - has to be incremented manually
static const CallbackInterface* Callbacks = nullptr;

//-----------------------------------------------------------------------------
//! dll-function to obtain the version of the current module
//!
//! @return                       Version of the current module
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT const std::string& OpenPASS_GetVersion()
{
    return Version;
}

//-----------------------------------------------------------------------------
//! dll-function to create an instance of the module
//!
//! @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
//! @param[in]     world          Pointer to the world
//! @param[in]     eventNetwork   Pointer to event network
//! @param[in]     parameters     Pointer to the parameters of the module
//! @param[in]     callbacks      Pointer to the callbacks
//! @param[in]     dataBuffer     Pointer to the data buffer that provides read-only access to the data
//! @return                       A pointer to the created module instance
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT ObservationInterface* OpenPASS_CreateInstance(
    StochasticsInterface* stochastics,
    WorldInterface* world,
    core::EventNetworkInterface* eventNetwork,
    const ParameterInterface* parameters,
    const CallbackInterface* callbacks,
    DataBufferReadInterface* dataBuffer)
{
    Callbacks = callbacks;

    try
    {
        return (ObservationInterface*)(new (std::nothrow) ObservationEntityRepositoryImplementation(eventNetwork,
                                                                                         stochastics,
                                                                                         world,
                                                                                         parameters,
                                                                                         callbacks,
                                                                                         dataBuffer));
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return nullptr;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return nullptr;
    }
}

//-----------------------------------------------------------------------------
//! dll-function to destroy/delete an instance of the module
//!
//! @param[in]     implementation    The instance that should be freed
//-----------------------------------------------------------------------------
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT void OpenPASS_DestroyInstance(ObservationInterface* implementation)
{
    delete implementation;
}

//! dll-function to call an instance of the module in opSimulationManager before each simulation run starts
//! 
//! @param[in]     implementation    The instance of the module
//! @return  returns true if the instance is invoked, false otherwise
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationManagerPreHook(ObservationInterface* implementation)
{
    try
    {
        implementation->OpSimulationManagerPreHook();
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in opSimulationManager after each simulation run ends
 * 
 * @param[in]     implementation    The instance that should be called
 * @param[in]     filename          Name of file containing the simulation run results from the simulation
 * @return  returns true if the instance is invoked, false otherwise 
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationManagerPostHook(ObservationInterface* implementation,
        const std::string& filename)
{
    try
    {
        implementation->OpSimulationManagerPostHook(filename);
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function called by framework in simulation before all simulation runs start
 * 
 * @param[in]     implementation    Pointer to the ObservationInterface
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPreHook(ObservationInterface* implementation)
{
    try
    {
        implementation->OpSimulationPreHook();
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in simulation before each simulation run starts be stored
 * 
 * @param[in]     implementation    The instance that should be called
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPreRunHook(ObservationInterface* implementation)
{
    try
    {
        implementation->OpSimulationPreRunHook();
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in simulation at each time step.
 * Observation module can indicate end of simulation run here.
 * 
 * @param[in]       implementation  The instance that should be called
 * @param[in]       time            Current scheduling time
 * @param[in,out]   runResult       Reference to run result
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationUpdateHook(ObservationInterface* implementation,
        int time,
        RunResultInterface& runResult)
{
    try
    {
        implementation->OpSimulationUpdateHook(time, runResult);
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in simulation after each simulation run ends.
 * Observation module can observe the current simulation run here.
 * 
 * @param[in]       implementation  The instance that should be called
 * @param[in,out]   runResult       Reference to run result
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPostRunHook(ObservationInterface* implementation,
        const RunResultInterface& runResult)
{
    try
    {
        implementation->OpSimulationPostRunHook(runResult);
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in simulation after all simulation runs end.
 * 
 * @param[in]       implementation  The instance that should be called
 * @return  returns true if the instance is invoked, false otherwise
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT bool OpenPASS_OpSimulationPostHook(ObservationInterface* implementation)
{
    try
    {
        implementation->OpSimulationPostHook();
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

/**
 * dll-function to call an instance of the module in simulation after all simulation 
 * runs end to transfer the observation module results to the opSimulationManager.
 * 
 * @param[in]       implementation  The instance that should be called
 * @return  File to be transferred
 */
extern "C" OBSERVATION_ENTITYREPOSITORY_SHARED_EXPORT const std::string OpenPASS_OpSimulationResultFile(
    ObservationInterface* implementation)
{
    try
    {
        return implementation->OpSimulationResultFile();
    }
    catch (const std::runtime_error& ex)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return "";
    }
    catch (...)
    {
        if (Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return "";
    }
}
