/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2016 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


//-----------------------------------------------------------------------------
//! @file  spawner_start.cpp
//! @brief This file contains the DLL wrapper.
//-----------------------------------------------------------------------------

#include "spawner_start.h"

#include <stdexcept>

#include "spawnPointInterface.h"
#include "spawner_start_implementation.h"

/// The version of the current module - has to be incremented manually
const std::string Version = "0.0.1";
static const CallbackInterface *Callbacks = nullptr;

/**
 * @brief dll-function to obtain the version of the current module
 * 
 * @return  Version of the current module
 */
extern "C" SPAWNPOINT_STARTSHARED_EXPORT const std::string &OpenPASS_GetVersion()
{
    return Version;
}

/**
 * @brief dll-function to create an instance of the module
 * 
 * @param[in]   stochastics   Pointer to the stochastics class loaded by the framework
 * @param[in]   world         Pointer to the world
 * @param[in]   parameters    Pointer to the parameters of the module
 * @param[in]   callbacks     Pointer to the callbacks
 * @return  A pointer to the created module instance
 */
extern "C" SPAWNPOINT_STARTSHARED_EXPORT SpawnPointInterface *OpenPASS_CreateInstance(
        StochasticsInterface *stochastics,
        WorldInterface *world,
        const ParameterInterface *parameters,
        const CallbackInterface *callbacks)
{
    Callbacks = callbacks;

    try
    {
        return (SpawnPointInterface*)(new (std::nothrow) SpawnPoint_Start_Implementation(
                                          stochastics,
                                          world,
                                          parameters,
                                          callbacks));
    }
    catch(const std::runtime_error &ex)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return nullptr;
    }
    catch(...)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return nullptr;
    }
}

/**
 * @brief dll-function to destroy/delete an instance of the module
 * 
 * @param[in]   implementation  The instance that should be freed
 */
extern "C" SPAWNPOINT_STARTSHARED_EXPORT void OpenPASS_DestroyInstance(SpawnPointInterface *implementation)
{
    delete implementation;
}

/// @brief dll-function to set spawn item
/// @param implementation The SpawnPointInterface
/// @param spawnItem 
/// @param maxIndex 
/// @return 
extern "C" SPAWNPOINT_STARTSHARED_EXPORT bool OpenPASS_SetSpawnItem(SpawnPointInterface *implementation,
                                                                        SpawnItemParameterInterface &spawnItem,
                                                                        int maxIndex)
{
    try
    {
        implementation->SetSpawnItem(spawnItem, maxIndex);
    }
    catch(const std::runtime_error &ex)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, ex.what());
        }

        return false;
    }
    catch(...)
    {
        if(Callbacks != nullptr)
        {
            Callbacks->Log(CbkLogLevel::Error, __FILE__, __LINE__, "unexpected exception");
        }

        return false;
    }

    return true;
}

