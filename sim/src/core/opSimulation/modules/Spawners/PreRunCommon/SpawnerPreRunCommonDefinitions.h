/********************************************************************************
 * Copyright (c) 2019-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>

#include "common/SpawnerDefinitions.h"
#include "common/commonTools.h"
#include "include/parameterInterface.h"

namespace SpawnerPreRunCommonDefinitions {
    using namespace SpawnPointDefinitions;

    /// Defines an area in which agents should be spawned
    struct SpawnArea
    {
        std::unique_ptr<LaneStreamInterface> laneStream;    //!< LaneStream to spawn on
        const SPosition sStart;                             //!< Start position on the stream
        const SPosition sEnd;                               //!< End position on the stream

        //! SpawnArea constructor
        //!
        //! @param[in] laneStream   LaneStream to spawn on
        //! @param[in] sStart       Start position on the stream
        //! @param[in] sEnd         End position on the stream
        SpawnArea(std::unique_ptr<LaneStreamInterface> laneStream,
                  const SPosition sStart,
                  const SPosition sEnd) :
            sStart(sStart),
            sEnd(sEnd)
        {
            this->laneStream = std::move(laneStream);
        }
    };

    ///Parameters of the PreRunCommonSpawner
    struct PreRunSpawnerParameters
    {
        const std::variant<double, openpass::parameter::StochasticDistribution> minimumSeparationBuffer; //!< Minimum distance between two agents
        const std::vector<SpawnArea> spawnAreas; //!< Areas to spawn in
        const AgentProfileLaneMaps agentProfileLaneMaps; //!< AgentProfiles to spawn, separated for left lane and other lanes
    };

    /// position and parameters of an agent to spawn
    struct SpawnInfo
    {
        SpawnParameter spawnParameter;  //!< Spawn parameter
        std::string roadId;             //!< Id of the road
        int laneId;                     //!< Id of the lane
        double s;                       //!< s coordinate on the stream
        double streamPosition;          //!< Position relative to the start of the stream
    };
} // SpawnPointPreRunDefinitions
