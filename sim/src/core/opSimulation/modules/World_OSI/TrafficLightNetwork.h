/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "OWL/DataTypes.h"
#include "common/openPassTypes.h"
#include "include/dataBufferInterface.h"
#include "WorldData.h"

using AcyclicPublisher = std::function<void(openpass::type::EntityId id, openpass::type::FlatParameterKey key, const openpass::databuffer::Acyclic& value)>;

//! Holds a set of linked traffic lights (i.e. for one junction) and manages their states
class TrafficLightController
{
public:
    //! \brief Updates the states of all traffic lights of this controller
    //!
    //! \param time     current simulation timestamp
    void UpdateStates(int time);

    //! Each controller consist of a sequence of phases, that define the state of each traffic light
    struct Phase
    {
        int duration;       ///< Duration of the phase
        std::vector<std::pair<OWL::Interfaces::TrafficLight*, CommonTrafficLight::State>> states; ///< State of each phase
        std::string stateDescriptor;    ///< Description of the state
        OWL::Id osiId;                  ///< OSI Id of the phase
    };

    /// @brief TrafficLightController constructor
    /// @param phases   List of phases
    /// @param delay    Delay in the controller
    TrafficLightController(std::vector<Phase>&& phases, double delay);

    /// @return Getter function to return pointer to the current phase
    inline Phase& getCurrentPhase () const { return *currentPhase; }

    /// @return Getter function to return the last time
    inline const int getLastTime() const { return lastTime; }

    /// @return Getter function to return if it is time to publish
    bool getTimeToPublish() const {return timeToPublish; }

    /// @brief Function to set the time to publish as false
    void setTimeToPublishFalse() {timeToPublish = false; }

private:
    std::vector<Phase> phases;
    std::vector<Phase>::iterator currentPhase;
    int timeRemainingInCurrentPhase;
    int lastTime;
    bool timeToPublish = true;
};

//!Holds all TrafficLightControllers and triggers them each timestep
class TrafficLightNetwork
{

public:
    TrafficLightNetwork();

    //! \brief Updates the states of all traffic lights in the network
    //!
    //! \param time     current simulation timestamp
    void UpdateStates(int time);

    //! \brief Adds a new controller to the network
    //!
    //! \param controller   new controller to add
    void AddController(TrafficLightController&& controller);

    /// @brief Publlish the global data
    /// @param publish Acyclic publisher
    void PublishGlobalData(AcyclicPublisher publish);

    /// @brief Function to register a OSI traffic light id
    /// @param id OSI traffic light id to register
    void registerOsiTrafficLightId(const OWL::Id id) { osiTrafficLightIds.emplace(id); }

    /// @return Getter function to return the list of all OSI traffic light ids
    const std::vector<OWL::Id> getOsiTrafficLightIds() const {return std::vector<OWL::Id> (osiTrafficLightIds.begin(), osiTrafficLightIds.end()) ;}

private:
    std::vector<TrafficLightController> controllers;
    std::set<OWL::Id> osiTrafficLightIds;
};
