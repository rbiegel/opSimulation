/********************************************************************************
 * Copyright (c) 2018-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <algorithm>

#include "OWL/Primitives.h"
#include "OWL/LaneGeometryJoint.h"
#include "common/commonTools.h"

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

namespace OWL {

namespace Interfaces {
class Lane;
}

namespace Primitive {

namespace bg = boost::geometry;

using point_t = bg::model::d2::point_xy<double>;
using polygon_t = bg::model::polygon<point_t>;

using point_i_t = bg::model::point<int, 2, bg::cs::cartesian>;
using box_i_t = bg::model::box<point_i_t>;


//! This struct represents a stream of generic quadrilaterals (LaneGeometryElements)
struct LaneGeometryElement
{
    /// @brief LaneGeometryElement constructor
    /// @param current Current lane geometry joint
    /// @param next    Next lane geometry joint
    /// @param lane    Pointer to the Lane interface
    LaneGeometryElement(const LaneGeometryJoint current,
                        const LaneGeometryJoint next,
                        const OWL::Interfaces::Lane* lane) :
        joints
        {
            current,
            next
        },
      lane(lane)
    {

    }

    /// This struct represents joints to the current lane geometry element
    struct Joints
    {
        LaneGeometryJoint current;  ///< Current lane geometry joint
        LaneGeometryJoint next;     ///< Next lane geometry joint
    } const joints;                 ///< Struct of joints

    const OWL::Interfaces::Lane* lane;  ///< Pointer to the Lane interface
};

} // namespace Primitive
} // namespace OWL
