/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2019 in-tech GmbH
 *               2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include "common/opMath.h"
#include "common/hypot.h"

namespace OWL {
    namespace Primitive {

/// This struct represents 3d vectors in cartesian coordinate system
struct Vector3
{
    /// x-value
    double x;
    /// y-value
    double y;
    /// z-value
    double z;
};

/// This struct represents the dimension of the object
struct Dimension
{
    /// Length of the object
    double length;
    /// Width of the object
    double width;
    /// Height of the object
    double height;
};

/// This struct represents the absolute coordinates (i. e. world coordinates)
struct AbsCoordinate
{
    /// x coordinate
    double x;
    /// y coordinate
    double y;
    /// Heading
    double hdg;
};

/// This struct represents the position of the reference point of the object in absolute coordinates (i. e. world coordinates)
struct AbsPosition
{
    AbsPosition() = default;

    AbsPosition(const AbsPosition &) = default;

    AbsPosition operator+(const AbsPosition &ref) const {
        return {x + ref.x,
                y + ref.y,
                z + ref.z
        };
    }

    AbsPosition operator-(const AbsPosition &ref) const {
        return {x - ref.x,
                y - ref.y,
                z - ref.z
        };
    }

    double distance() const {
        return openpass::hypot(x, openpass::hypot(y, z));
    }

    void RotateYaw(double angle) {
        double cosValue = std::cos(angle);
        double sinValue = std::sin(angle);
        double newX = x * cosValue - y * sinValue;
        double newY = x * sinValue + y * cosValue;
        x = newX;
        y = newY;
    }

    /// Position x
    double x;
    /// Position y
    double y;
    /// Position z
    double z;
};

/// This struct represents the velocity of the object in absolute coordinates (i. e. world coordinates)
struct AbsVelocity
{
    /// x-value of the velocity of the object
    double vx;
    /// y-value of the velocity of the object
    double vy;
    /// z-value of the velocity of the object
    double vz;
};

/// This struct represents the acceleration of the object in absolute coordinates (i. e. world coordinates)
struct AbsAcceleration
{
    /// x-value of the acceleration of the object
    double ax;
    /// y-value of the acceleration of the object
    double ay;
    /// z-value of the acceleration of the object
    double az;
};

/// This struct represents the orientation of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientation
{
    /// Yaw angles of the object
    double yaw;
    /// Pitch angles of the object
    double pitch;
    /// Roll angles of the object
    double roll;
};

/// This struct represents the orientation rate of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientationRate
{
    /// Yaw rate of the object
    double yawRate;
    /// Pitch rate of the object
    double pitchRate;
    /// Roll rate of the object
    double rollRate;
};

/// This struct represents the orientation acceleration of the object in absolute coordinates (i. e. world coordinates)
struct AbsOrientationAcceleration
{
    /// Yaw acceleration of the object
    double yawAcceleration;
    /// Pitch acceleration of the object
    double pitchAcceleration;
    /// Roll acceleration of the object
    double rollAcceleration;
};

/// This struct represents the road coordinates
struct RoadCoordinate
{
    /// s coordinate (describes the longitudinal position along the road)
    double s;
    /// t coordinate (describes the lateral position relative to the center of the road)
    double t;
    /// Yaw angle
    double yaw;
};

/// This struct represents the velocity of the object in road coordinates
struct RoadVelocity
{
    /// s-value of the velocity of the object
    double vs;
    /// t-value of the velocity of the object
    double vt;
    /// z-value of the velocity of the object
    double vz;
};

/// This struct represents the acceleration of the object in road coordinates
struct RoadAcceleration
{
    /// s-value of the acceleration of the object
    double as;
    /// t-value of the acceleration of the object
    double at;
    /// z-value of the acceleration of the object
    double az;
};

/// This struct represents the orientation rate of the object in road coordinates
struct RoadOrientationRate
{
    /// Heading rate
    double hdgRate;
};

/// This struct represents the lane position
struct LanePosition
{
    double v;   ///< TODO
    double w;   ///< TODO
};

/// This struct represents the velocity of the object
struct LaneVelocity
{
    double vv;  ///< TODO
    double vw;  ///< TODO
    double vz;  ///< TODO
};

/// This struct represents the acceleration of the object
struct LaneAcceleration
{
    double av;  ///< Todo
    double aw;  ///< TODO
    double az;  ///< TODO
};

/// This struct represents the orientation of the lane
struct LaneOrientation
{
    double hdg; ///< Heading angle
};

/// This struct represents the rate of orientation of the lane
struct LaneOrientationRate
{
    /// Rate of heading angle
    double hdgRate;
};

    } // namespace Primitive
} // namespace OWL
