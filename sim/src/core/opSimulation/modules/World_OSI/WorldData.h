/********************************************************************************
 * Copyright (c) 2020 HLRS, University of Stuttgart
 *               2018-2021 in-tech GmbH
 *               2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  WorldData.h
//! @brief This file provides access to underlying data structures for
//!        scenery and dynamic objects
//-----------------------------------------------------------------------------

#pragma once

#include <unordered_map>

#include "OWL/DataTypes.h"
#include "OWL/TrafficLight.h"
#include "OWL/MovingObject.h"
#include "include/roadInterface/roadInterface.h"
#include "include/roadInterface/junctionInterface.h"
#include "include/worldInterface.h"
#include "include/callbackInterface.h"
#include "common/openScenarioDefinitions.h"
#include "common/hypot.h"
#include <osi3/osi_groundtruth.pb.h>
#include <osi3/osi_sensorview.pb.h>
#include <osi3/osi_sensorviewconfiguration.pb.h>

#ifdef USE_PROTOBUF_ARENA
#include <google/protobuf/arena.h>
#endif

namespace OWL {

using Lane                  = Interfaces::Lane;                     ///< Lane interface
using LaneBoundary          = Interfaces::LaneBoundary;             ///< Lane boundary interface
using LogicalLaneBoundary   = Interfaces::LogicalLaneBoundary;      ///< Logical lane boundary interface
using ReferenceLine         = Interfaces::ReferenceLine;            ///< Reference line interface
using Section               = Interfaces::Section;                  ///< Section interface
using Road                  = Interfaces::Road;                     ///< Road interface
using Junction              = Interfaces::Junction;                 ///< Junction interface
using StationaryObject      = Implementation::StationaryObject;     ///< Stationary object implementation
using MovingObject          = Implementation::MovingObject;         ///< Moving object implementation
using TrafficSign           = Implementation::TrafficSign;          ///< Traffic sign implementation
using RoadMarking       = Implementation::RoadMarking;              ///< Road marking implementation

using CLane              = const Interfaces::Lane;                  ///< Constant of the lane
using CSection           = const Interfaces::Section;               ///< Constant of the section
using CRoad              = const Interfaces::Road;                  ///< Constant of the road in the world
using CStationaryObject  = const Implementation::StationaryObject;  ///< Constant of the stationary object
using CMovingObject      = const Implementation::MovingObject;      ///< Constant of the moving object

using Lanes = std::vector<Lane*>;                                   ///< List of pointers to the lane
using Sections = std::vector<Section*>;                             ///< List of pointers to the section
using Roads = std::vector<Road*>;                                   ///< List of pointers to the road
using StationaryObjects = std::vector<StationaryObject*>;           ///< List of pointers to the stationary object
using MovingObjects = std::vector<MovingObject*>;                   ///< List of pointers to the moving object

using CLanes = const std::vector<CLane*>;                           ///< Constant list of pointers to the lane
using CSections = const std::vector<CSection*>;                     ///< Constant list of pointers to the section
using CRoads = const std::vector<CRoad*>;                           ///< Constant list of pointers to the road
using CStationaryObjects = const std::vector<CStationaryObject*>;   ///< Constant list of pointers to the stationary object
using CMovingObjects = const std::vector<CMovingObject*>;           ///< Constant list of pointers to the moving object

template<typename T>
using IdMapping = std::map<OWL::Id, std::unique_ptr<T>>;            ///< The map (container class) for registering Ids

/// The OWL type of a signal of OpenDrive
enum class SignalType
{
    TrafficSign,
    TrafficLight,
    Other
};

#ifdef USE_PROTOBUF_ARENA
//If using protobuf arena the SensorView must not be manually deleted.
struct DoNothing
{
    void operator() (osi3::SensorView*) {}
};
using SensorView_ptr = std::unique_ptr<osi3::SensorView, DoNothing>;
#else
using SensorView_ptr = std::unique_ptr<osi3::SensorView>;   ///< Pointer to the OSI sensor view
#endif

namespace Interfaces {

//!This class contains the entire road network and all objects in the world
class WorldData
{
public:
    virtual ~WorldData() = default; //!< default destructor

    //!Deletes all objects, roads, sections and lanes
    virtual void Clear() = 0;

    /*!
     * \brief Creates a OSI SensorView
     *
     * \param[in]   conf      SensorViewConfiguration to create SensorView from
     * \param[in]   agentId   The Id of the associated Agent
     * \param[in]   time      The time at which sensor view is retrieved
     * 
     *
     * \return      A OSI SensorView with filtered GroundTruth
     */
    virtual SensorView_ptr GetSensorView(osi3::SensorViewConfiguration& conf, int agentId, int time) = 0;

    /*!
     * \brief Frees temporary objects
     *
     * A temporary arena is used to allocate SensorViews and copies of GroundTruth. This
     * method should be called after each time step.
     *
     * \note This method only provides a temporary solution against leaking arena memory and will be removed in the near future.
     */
    virtual void ResetTemporaryMemory() = 0;

    /**
     * @brief Retrieves the OSI ground truth
     * 
     * @return OSI ground truth
     */
    virtual const osi3::GroundTruth& GetOsiGroundTruth() const = 0;

    //!Returns a map of all Roads with their OSI Id
    //!
    //! \return map of all Roads with their OSI Id
    virtual const std::unordered_map<std::string, Road*>& GetRoads() const = 0;

    //!Creates a new MovingObject linked to an AgentAdapter and returns it
    //!
    //! \param id            Unique ID
    //! \return new MovingObject linked to an AgentAdapter
    virtual Interfaces::MovingObject &AddMovingObject(const Id id) = 0;

    //!Creates a new StationaryObject linked to a TrafficObjectAdapter and returns it
    //!
    //! \param id            Unique ID
    //! \param linkedObject  Object of type TrafficObjectAdapter which will be linked to new StationaryObject
    //! \return new StationaryObject linked to a TrafficObjectAdapter
    virtual Interfaces::StationaryObject& AddStationaryObject(const Id id, void* linkedObject) = 0;

    //!Creates a new TrafficSign and returns it
    //! \param id            Unique ID
    //! \param odId          OpenDRIVE Id
    //! \return new static traffic sign
    virtual Interfaces::TrafficSign& AddTrafficSign(const Id id, const std::string odId) = 0;

    //!Creates a new TrafficLight and returns it
    //! \param ids           OSI IDs
    //! \param odId          OpenDRIVE Id
    //! \param openDriveType OpenDrive type (e.g "1.000.001 for generic three lights traffic light)
    //! \return new traffic light
    virtual Interfaces::TrafficLight& AddTrafficLight(const std::vector<Id> ids,
                                                      const std::string odId,const std::string& openDriveType) = 0;

    //!Creates a new RoadMarking and returns it
    //! \param id            Unique ID
    //! \return road marking
    virtual Interfaces::RoadMarking& AddRoadMarking(const Id id) = 0;

    //! Adds a traffic sign to the assigned signs of lane
    //!
    //! \param laneId         OSI Id of the lane
    //! \param trafficSign    traffic sign to assign
    //! \param specification  Definition of sign in OpenSCENARIO 
    virtual void AssignTrafficSignToLane(OWL::Id laneId, Interfaces::TrafficSign& trafficSign, const RoadSignalInterface& specification) = 0;

    //! Adds a road marking to the assigned road markings of lane
    //!
    //! \param laneId         OSI Id of the lane
    //! \param roadMarking    RoadMarking to assign
    //! \param specification  Definition of sign in OpenSCENARIO 
    virtual void AssignRoadMarkingToLane(OWL::Id laneId, Interfaces::RoadMarking& roadMarking, const RoadSignalInterface& specification) = 0;

    //! Adds a road marking to the assigned road markings of lane
    //!
    //! \param laneId         OSI Id of the lane
    //! \param roadMarking    RoadMarking to assign
    //! \param specification  Definition of object in OpenSCENARIO 
    virtual void AssignRoadMarkingToLane(OWL::Id laneId, Interfaces::RoadMarking& roadMarking, const RoadObjectInterface& specification) = 0;

    //! Adds a traffic light to the assigned traffic lights of lane
    //!
    //! \param laneId         OSI Id of the lane
    //! \param trafficLight   Traffic light to assign
    //! \param specification  Definition of sign in OpenSCENARIO 
    virtual void AssignTrafficLightToLane(OWL::Id laneId, Interfaces::TrafficLight &trafficLight, const RoadSignalInterface& specification) = 0;

    //!Deletes the moving object with the specified Id
    //!
    //! \param id   Unique ID
    virtual void RemoveMovingObjectById(Id id) = 0; // change Id to MovingObject

    //!Returns the mapping of OpenDrive Ids to OSI Ids for trafficSigns
    //!
    //! \return the mapping of OpenDrive Ids to OSI Ids for trafficSigns
    virtual const std::unordered_map<std::string, Id>& GetTrafficSignIdMapping() const = 0;

    //!Return the OWL type of a signal of OpenDrive or type Unknown if there is no signal with this Id
    //!
    //! \param id   Unique ID 
    //! \return OWL type of a signal of OpenDrive
    virtual SignalType GetSignalType(Id id) const = 0;

    /**
     * @brief Returns an invalid lane
     * 
     * @return  invalid lane
     */
    virtual const Implementation::InvalidLane& GetInvalidLane() const = 0;

    //!Returns a map of all lanes with their OSI Id
    //!
    //! @return lanes with their OSI Id
    virtual const IdMapping<Lane>& GetLanes() const = 0;

    /**
     * @brief Returns the lane with given OSI Id
     * 
     * @param[in] id    OSI Id   
     * @return  lane with given OSI Id
     */
    virtual const Lane& GetLane(Id id) const = 0;

    /**
     * @brief Returns the lane boundaries with given OSI Id
     * 
     * @param[in] id    OSI Id
     * @return  lane boundaries with given OSI Id
     */
    virtual const LaneBoundary& GetLaneBoundary(Id id) const = 0;

    /**
     * @brief Returns a map of all junctions with their OSI Id
     * 
     * @return  map of all junctions with their OSI Id
     */
    virtual const std::map<std::string, Junction*>& GetJunctions() const = 0;

    /**
     * @brief Returns the traffic sign with given OSI Id
     * 
     * @param[in] id    OSI Id
     * @return  traffic sign with given OSI Id
     */
    virtual TrafficSign& GetTrafficSign(Id id) = 0;

    /**
     * @brief Returns the traffic light with given OSI Id
     * 
     * @param[in] id    OSI Id
     * @return  traffic light with given OSI Id
     */
    virtual TrafficLight& GetTrafficLight(Id id) = 0;
    
    /**
     * @brief Returns the stationary object with given Id
     * 
     * @param[in] id    OSI Id
     * @return  stationary object with given Id
     */
    virtual const StationaryObject& GetStationaryObject(Id id) const = 0;

    /**
     * @brief Returns the moving object with given Id
     * 
     * @param[in] id    OSI Id
     * @return  moving object with given Id
     */
    virtual const MovingObject& GetMovingObject(Id id) const = 0;

    //! Sets the road graph as imported from OpenDrive
    //!
    //! \param roadGraph        graph representation of road network
    //! \param vertexMapping    mapping from roads (with direction) to the vertices of the roadGraph
    virtual void SetRoadGraph (const RoadGraph&& roadGraph, const RoadGraphVertexMapping&& vertexMapping) = 0;

    /**
     * @brief Sets the turning rates for junctions
     * 
     * @param  turningRates Turning rate for junctions
     */  
    virtual void SetTurningRates(const TurningRates& turningRates) = 0;

    /**
     * @brief Returns the graph representation of the road network
     * 
     * @return  graph representation of the road network
     */
    virtual const RoadGraph& GetRoadGraph() const = 0;

    /**
     * @brief Returns the mapping from roads (with direction) to the vertices of the roadGraph
     * 
     * @return  mapping from roads (with direction) to the vertices of the roadGraph
     */
    virtual const RoadGraphVertexMapping& GetRoadGraphVertexMapping() const = 0;

    /**
     * @brief Retrieves the turning rates for junctions
     * 
     * @return  turning rates
     */  
    virtual const TurningRates& GetTurningRates() const = 0;

    //!Creates a new lane with parameters specified by the OpenDrive lane
    //!
    //! \param laneId               Unique ID
    //! \param logicalLaneId        Unique ID
    //! \param odSection            OpenDrive section to add lane to
    //! \param odLane               OpenDrive lane to add
    //! \param laneBoundaries       OSI Ids of the left lane boundaries of the new lane
    //! \param logicalLaneBoundary  Id of the logical lane boundary
    virtual void AddLane(const Id laneId, const Id logicalLaneId, RoadLaneSectionInterface& odSection, const RoadLaneInterface& odLane, const std::vector<Id> laneBoundaries, const Id logicalLaneBoundary) = 0;

    //! Creates a new lane boundary specified by the OpenDrive RoadMark
    //!
    //! \param id               Unique ID
    //! \param odLaneRoadMark   OpenDrive roadMark (= laneBoundary) to add
    //! \param sectionStart     Start s coordinate of the section
    //! \param side             Specifies which side of a double line to add (or Single if not a double line)
    //! \return Osi id of the newly created laneBoundary
    virtual Id AddLaneBoundary(const Id id, const RoadLaneRoadMark &odLaneRoadMark, double sectionStart, LaneMarkingSide side) = 0;

    //! Creates a new logical lane boundary specified by the OpenDrive RoadMark
    //!
    //! \param id               Unique ID
    //! \return Osi id of the newly created laneBoundary
    virtual Id AddLogicalLaneBoundary(const Id id) = 0;

    //! Creates a new reference line specified by the OpenDrive RoadMark
    //!
    //! \param id               Unique ID
    //! \return Osi id of the newly created reference line
    virtual Id AddReferenceLine(const Id id) = 0;
    
    //! Sets the ids of the center lane boundaries for a section
    //!
    //! \param odSection                OpenDrive section for which to set the center line
    //! \param laneBoundaryIds          Osi ids of the center lane boundaries
    //! \param logicalLaneBoundaryId    Osi id of the center logical lane boundary
    //! \param referenceLine            Osi id of the reference line
    virtual void SetCenterLaneBoundary(const RoadLaneSectionInterface& odSection, std::vector<Id> laneBoundaryIds, Id logicalLaneBoundaryId, Id referenceLine) = 0;

    //!Creates a new section with parameters specified by the OpenDrive section
    //!
    //! \param odRoad    OpenDrive road to add section to
    //! \param odSection OpenDrive section to add
    virtual void AddSection(const RoadInterface& odRoad, const RoadLaneSectionInterface& odSection) = 0;

    //!Creates a new road with parameters specified by the OpenDrive road
    //!
    //! \param odRoad    OpenDrive road to add
    virtual void AddRoad(const RoadInterface& odRoad) = 0;

    //!Creates a new junction with parameters specified by the OpenDrive junction
    //!
    //! \param odJunction    OpenDrive junction to add
    virtual void AddJunction(const JunctionInterface* odJunction) = 0;

    //!Adds a connection road (path) to a junction
    //!
    //! \param odJunction   OpenDrive junction
    //! \param odRoad       OpenDrive road
    virtual void AddJunctionConnection(const JunctionInterface* odJunction, const RoadInterface& odRoad) = 0;

    //!Adds a priority entry of to connecting roads to a junction
    //!
    //! \param odJunction   OpenDrive junction
    //! \param high         High priority
    //! \param low          Low priority
    virtual void AddJunctionPriority(const JunctionInterface* odJunction, const std::string& high, const std::string& low) = 0;

    //!Adds a new lane geometry joint to the end of the current list of joints of the specified lane
    //! and a new geometry element and the boundary points of all affected right lane boundaries
    //!
    //! @param odLane       OpenDrive lane
    //! @param pointLeft    left point of the new joint
    //! @param pointCenter  reference point of the new joint
    //! @param pointRight   right point of the new joint
    //! @param sOffset      s offset of the new joint
    //! @param t_left       t coordinate of the left point
    //! @param t_right      t coordinate of the right point
    //! @param curvature    curvature of the lane at sOffset
    //! @param heading      heading of the lane at sOffset
    virtual void AddLaneGeometryPoint(const RoadLaneInterface& odLane,
                                      const Common::Vector2d& pointLeft,
                                      const Common::Vector2d& pointCenter,
                                      const Common::Vector2d& pointRight,
                                      const double sOffset,
                                      const double t_left,
                                      const double t_right,
                                      const double curvature,
                                      const double heading) = 0;

    //! Adds a new boundary point to the center line of the specified section
    //!
    //! \param odSection    section to add boundary point to
    //! \param pointCenter  point to add
    //! \param sOffset      s offset of the new point
    //! \param heading      heading of the road at sOffset
    virtual void AddCenterLinePoint(const RoadLaneSectionInterface& odSection,
                                    const Common::Vector2d& pointCenter,
                                    const double sOffset,
                                    double heading) = 0;

    //!Sets successorOdLane as successor of odLane
    //!
    //! \param odLane               lane for which the successor will be set
    //! \param successorOdLane      successor lane
    //! \param atBeginOfOtherLane   lane connects to the start of the other lane
    virtual void AddLaneSuccessor(/* const */ RoadLaneInterface& odLane,
            /* const */ RoadLaneInterface& successorOdLane, bool atBeginOfOtherLane) = 0;

    //!Sets predecessorOdLane as predecessor of odLane
    //!
    //! \param odLane               lane for which the predecessor will be set
    //! \param predecessorOdLane    predecessor lane
    //! \param atBeginOfOtherLane   lane connects to the start of the other lane
    virtual void AddLanePredecessor(/* const */ RoadLaneInterface& odLane,
            /* const */ RoadLaneInterface& predecessorOdLane, bool atBeginOfOtherLane) = 0;
    
    /// Sets intersection info
    /// @param intersectionInfo         information on intersection
    /// @param crossIntersectionInfo    infromation about cross intersection
    virtual void AddIntersectionInfo(IntersectionInfo intersectionInfo, IntersectionInfo crossIntersectionInfo) = 0;

    //!Sets successorRoad as successor of road in OSI
    //!
    //! @param  road            Road in OSI                   
    //! @param  successorRoad   Successor of the road in OSI
    virtual void SetRoadSuccessor(const RoadInterface& road,  const RoadInterface& successorRoad) = 0;

    //!Sets predecessorRoad as predecessor of road in OSI
    //!
    //! @param  road            Road in OSI                   
    //! @param  predecessorRoad Predecessor of the road in OSI
    virtual void SetRoadPredecessor(const RoadInterface& road,  const RoadInterface& predecessorRoad) = 0;

    //!Sets successorSection as successor of section in OSI
    //!
    //! @param  section             Section in OSI                           
    //! @param  successorSection    Successor of the section in OSI 
    virtual void SetSectionSuccessor(const RoadLaneSectionInterface& section,  const RoadLaneSectionInterface& successorSection) = 0;

    //!Sets predecessorSection as predecessor of section in OSI
    //!
    //! @param  section             Section in OSI                           
    //! @param  predecessorSection  Predecessor of the section in OSI 
    virtual void SetSectionPredecessor(const RoadLaneSectionInterface& section,  const RoadLaneSectionInterface& predecessorSection) = 0;

    //!Sets a junction as the successor of a road
    //!
    //! @param  road                Road in OSI                               
    //! @param  successorJunction   Junction as the successor of a road  
    virtual void SetRoadSuccessorJunction(const RoadInterface& road,  const JunctionInterface* successorJunction) = 0;

    //!Sets a junction as the predecessor of a road
    //!
    //! @param  road                Road in OSI                               
    //! @param  predecessorJunction Junction as the predecessor of a road 
    virtual void SetRoadPredecessorJunction(const RoadInterface& road,  const JunctionInterface* predecessorJunction) = 0;

    //!Currently not implemented
    /// @param firstOdSection  First opendrive road lane section interface
    /// @param secondOdSection Second opendrive road lane section interface
    /// @param lanePairs       Pair of lane ids
    /// @param isPrev
    virtual void ConnectLanes(/*const*/ RoadLaneSectionInterface& firstOdSection,
                                        /*const*/ RoadLaneSectionInterface& secondOdSection,
                                        const std::map<int, int>& lanePairs,
                                        bool isPrev) = 0;


    static constexpr double THRESHOLD_ILLUMINATION_LEVEL1 = 0.01;       //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level1
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL2 = 1.0;        //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level2
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL3 = 3.0;        //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level3
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL4 = 10.0;       //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level4
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL5 = 20.0;       //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level5
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL6 = 400.0;      //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level6
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL7 = 1000.0;     //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level7
    static constexpr double THRESHOLD_ILLUMINATION_LEVEL8 = 10000.0;    //!< Upper threshold for osi3::EnvironmentalConditions::AmbientIllumination::Level8
    static constexpr double THRESHOLD_FOG_DENSE = 50.0;         //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Dense
    static constexpr double THRESHOLD_FOG_THICK = 200.0;        //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Thick
    static constexpr double THRESHOLD_FOG_LIGHT = 1000.0;       //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Light
    static constexpr double THRESHOLD_FOG_MIST = 2000.0;        //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Mist
    static constexpr double THRESHOLD_FOG_POOR = 4000.0;        //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Poor_Visibility
    static constexpr double THRESHOLD_FOG_MODERATE = 10000.0;   //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Moderate_Visibility
    static constexpr double THRESHOLD_FOG_GOOD = 40000.0;       //!< Upper threshold for osi3::EnvironmentalConditions::Fog::Good_Visibility
    static constexpr double THRESHOLD_PRECIPITATION_NONE = 0.1;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_None 
    static constexpr double THRESHOLD_PRECIPITATION_VERY_LIGHT = 0.5;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_Very_Light 
    static constexpr double THRESHOLD_PRECIPITATION_LIGHT = 1.9;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_Light 
    static constexpr double THRESHOLD_PRECIPITATION_MODERATE = 8.1;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_Moderate
    static constexpr double THRESHOLD_PRECIPITATION_HEAVY = 34.0;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_Heavy
    static constexpr double THRESHOLD_PRECIPITATION_VERY_HEAVY = 149.0;       //!< Upper threshold for osi3::EnvironmentalConditions::Precipitation_Very_Heavy
    static constexpr double DEFAULT_HUMIDITY = 43.0;                         ///< Default humiditiy 
    static constexpr double DEFAULT_ATMOSPHERIC_PRESSURE = 100000.0;         ///< Default atmospheric pressure in Pascal
    static constexpr double DEFAULT_TEMPERATURE = 293.15;                   ///< Default temperature in Fahrenheit

    //! \brief Translates an openScenario environment to OSI3
    //!
    //! The following thresholds are used for a mapping of illumination level:
    //! - \see THRESHOLD_ILLUMINATION_LEVEL1
    //! - \see THRESHOLD_ILLUMINATION_LEVEL2
    //! - \see THRESHOLD_ILLUMINATION_LEVEL3
    //! - \see THRESHOLD_ILLUMINATION_LEVEL4
    //! - \see THRESHOLD_ILLUMINATION_LEVEL5
    //! - \see THRESHOLD_ILLUMINATION_LEVEL6
    //! - \see THRESHOLD_ILLUMINATION_LEVEL7
    //! - \see THRESHOLD_ILLUMINATION_LEVEL8
    //!
    //! The following thresholds are used for a mapping of fog:
    //! - \see THRESHOLD_FOG_DENSE
    //! - \see THRESHOLD_FOG_THICK
    //! - \see THRESHOLD_FOG_LIGHT
    //! - \see THRESHOLD_FOG_MIST
    //! - \see THRESHOLD_FOG_POOR
    //! - \see THRESHOLD_FOG_MODERATE
    //! - \see THRESHOLD_FOG_GOOD
    //!
    //! \param[in]  environment OpenScenario environment
    virtual void SetEnvironment(const openScenario::EnvironmentAction& environment) = 0;

    //!Resets the world for new run; deletes all moving objects
    virtual void Reset() = 0;

    /*!
     * \brief Retrieves the OWL Id of an agent
     *
     * \param[in]    agentId    Agent id (as used in AgentInterface)
     *
     * \return  OWL Id of the underlying OSI object
     */
    virtual OWL::Id GetOwlId(int agentId) const = 0;

    /*!
     * \brief Retrieves the simualtion framework Id of an agent associated to the given OWL Id
     *
     * \param[in]   owlId   OWL Id
     *
     * \return  Agent Id of the associated agent
     */
    virtual int GetAgentId(const OWL::Id owlId) const = 0;
};

}

/// Class representing world data
class WorldData : public Interfaces::WorldData
{
public:

#ifdef USE_PROTOBUF_ARENA
    using GroundTruth_ptr = osi3::GroundTruth*;
#else
    using GroundTruth_ptr = std::unique_ptr<osi3::GroundTruth>; ///< Unique pointer to the OSI ground data
#endif

    /**
     * @brief WorldData constructor
     *
     * @param[in]   callbacks       Pointer to the callbacks
     */
    WorldData(const CallbackInterface* callbacks);

    ~WorldData() override;

    void Clear() override;

    SensorView_ptr GetSensorView(osi3::SensorViewConfiguration& conf, int agentId, int time) override;

    void ResetTemporaryMemory() override;

    const osi3::GroundTruth& GetOsiGroundTruth() const override;

    /*!
     * \brief Retrieves a filtered OSI GroundTruth
     *
     * \param[in]   conf        The OSI SensorViewConfiguration to be used for filtering
     * \param[in]   reference   Host of the sensor
     * \param[in]   time        Time at which the filtered OSI ground truth is retrieved
     *
     * \return      A OSI GroundTruth filtered by the given SensorViewConfiguration
     */
    GroundTruth_ptr GetFilteredGroundTruth(const osi3::SensorViewConfiguration& conf, const Interfaces::MovingObject& reference, int time);

    /*!
     * \brief Retrieves the TrafficSigns located in the given sector (geometric shape)
     *
     * \param[in]   origin              Origin of the sector shape
     * \param[in]   radius              Radius of the sector shape
     * \param[in]   leftBoundaryAngle   Left boundary angle of the sector shape
     * \param[in]   rightBoundaryAngle  Right boundary angle of the sector shape
     *
     * \return      Vector of TrafficSing pointers located in the given sector
     */
    std::vector<const Interfaces::TrafficSign*> GetTrafficSignsInSector(const Primitive::AbsPosition& origin,
                                                                        double radius,
                                                                        double leftBoundaryAngle,
                                                                        double rightBoundaryAngle);

    /*!
     * \brief Retrieves TrafficLights located in the given sector (geometric shape)
     *
     * \param[in]   origin              Origin of the sector shape
     * \param[in]   radius              Radius of the sector shape
     * \param[in]   leftBoundaryAngle   Left boundary angle of the sector shape
     * \param[in]   rightBoundaryAngle  Right boundary angle of the sector shape
     *
     * \return      Vector of TrafficLight pointers located in the given sector
     */
    std::vector<const Interfaces::TrafficLight*> GetTrafficLightsInSector(const Primitive::AbsPosition& origin,
                                                                          double radius,
                                                                          double leftBoundaryAngle,
                                                                          double rightBoundaryAngle);

    /*!
     * \brief Retrieves the RoadMarkings located in the given sector (geometric shape)
     *
     * \param[in]   origin              Origin of the sector shape
     * \param[in]   radius              Radius of the sector shape
     * \param[in]   leftBoundaryAngle   Left boundary angle of the sector shape
     * \param[in]   rightBoundaryAngle  Right boundary angle of the sector shape
     *
     * \return      Vector of RoadMarking pointers located in the given sector
     */
    std::vector<const Interfaces::RoadMarking*> GetRoadMarkingsInSector(const Primitive::AbsPosition& origin,
                                                                        double radius,
                                                                        double leftBoundaryAngle,
                                                                        double rightBoundaryAngle);

    /*!
     * \brief Retrieves the StationaryObjects located in the given sector (geometric shape)
     *
     * \param[in]   origin              Origin of the sector shape
     * \param[in]   radius              Radius of the sector shape
     * \param[in]   leftBoundaryAngle   Left boundary angle of the sector shape
     * \param[in]   rightBoundaryAngle  Right boundary angle of the sector shape
     *
     * \return      Vector of StationaryObject pointers located in the given sector
     */
    std::vector<const Interfaces::StationaryObject*> GetStationaryObjectsInSector(const Primitive::AbsPosition& origin,
                                                                                  double radius,
                                                                                  double leftBoundaryAngle,
                                                                                  double rightBoundaryAngle);

    /*!
     * \brief Retrieves the MovingObjects located in the given sector (geometric shape)
     *
     * \param[in]   origin              Origin of the sector shape
     * \param[in]   radius              Radius of the sector shape
     * \param[in]   leftBoundaryAngle   Left boundary angle of the sector shape
     * \param[in]   rightBoundaryAngle  Right boundary angle of the sector shape
     *
     * \return      Vector of MovingObject pointers located in the given sector
     */
    std::vector<const Interfaces::MovingObject*> GetMovingObjectsInSector(const Primitive::AbsPosition& origin,
                                                                          double radius,
                                                                          double leftBoundaryAngle,
                                                                          double rightBoundaryAngle);

    /*!
     * \brief Add the information about the host vehicle to the given SensorView
     *
     * \param host_id       id of the host vehicle
     * \param sensorView    SensorView to modify
     */
    void AddHostVehicleToSensorView(Id host_id, osi3::SensorView& sensorView);

    OWL::Id GetOwlId(int agentId) const override;
    int GetAgentId(const OWL::Id owlId) const override;

    void SetRoadGraph (const RoadGraph&& roadGraph, const RoadGraphVertexMapping&& vertexMapping) override;
    void SetTurningRates (const TurningRates& turningRates) override;
    void AddLane(const Id laneId, const Id logicalLaneId, RoadLaneSectionInterface &odSection, const RoadLaneInterface& odLane, const std::vector<Id> laneBoundaries, const Id logicalLaneBoundary) override;
    Id AddLaneBoundary(const Id id, const RoadLaneRoadMark &odLaneRoadMark, double sectionStart, LaneMarkingSide side) override;
    Id AddLogicalLaneBoundary(const Id id) override;
    Id AddReferenceLine(const Id id) override;
    void SetCenterLaneBoundary(const RoadLaneSectionInterface& odSection, std::vector<Id> laneBoundaryIds, Id logicalLaneBoundaryId, Id referenceLine) override;
    void AddSection(const RoadInterface& odRoad, const RoadLaneSectionInterface& odSection) override;
    void AddRoad(const RoadInterface& odRoad) override;
    void AddJunction(const JunctionInterface* odJunction) override;
    void AddJunctionConnection(const JunctionInterface* odJunction, const RoadInterface& odRoad) override;
    void AddJunctionPriority(const JunctionInterface* odJunction,  const std::string& high, const std::string& low) override;

    void AddLaneSuccessor(/* const */ RoadLaneInterface& odLane,
                                      /* const */ RoadLaneInterface& successorOdLane, bool atBeginOfOtherLane) override;
    void AddLanePredecessor(/* const */ RoadLaneInterface& odLane,
                                        /* const */ RoadLaneInterface& predecessorOdLane, bool atBeginOfOtherLane) override;
    
    /// @brief Add an intersection information to the worlddata
    /// @param intersectionInfo         Information on intersection
    /// @param crossIntersectionInfo    information about crossintersection
    void AddIntersectionInfo(IntersectionInfo intersectionInfo, IntersectionInfo crossIntersectionInfo) override;


    void ConnectLanes(/*const*/ RoadLaneSectionInterface& firstOdSection,
                                /*const*/ RoadLaneSectionInterface& secondOdSection,
                                const std::map<int, int>& lanePairs,
                                bool isPrev) override;
    void SetRoadSuccessor(const RoadInterface& road,  const RoadInterface& successorRoad) override;
    void SetRoadPredecessor(const RoadInterface& road,  const RoadInterface& predecessorRoad) override;
    void SetRoadSuccessorJunction(const RoadInterface& road,  const JunctionInterface* successorJunction) override;
    void SetRoadPredecessorJunction(const RoadInterface& road,  const JunctionInterface* predecessorJunction) override;
    void SetSectionSuccessor(const RoadLaneSectionInterface& section,  const RoadLaneSectionInterface& successorSection) override;
    void SetSectionPredecessor(const RoadLaneSectionInterface& section,  const RoadLaneSectionInterface& predecessorSection) override;

    Interfaces::MovingObject& AddMovingObject(const Id id) override;
    Interfaces::StationaryObject& AddStationaryObject(const Id id, void* linkedObject) override;
    Interfaces::TrafficSign& AddTrafficSign(const Id id, const std::string odId) override;
    Interfaces::TrafficLight& AddTrafficLight(const std::vector<Id> ids, const std::string odId,const std::string& type) override;
    Interfaces::RoadMarking& AddRoadMarking(const Id id) override;

    void AssignTrafficSignToLane(OWL::Id laneId, Interfaces::TrafficSign& trafficSign, const RoadSignalInterface& specification) override;
    void AssignRoadMarkingToLane(OWL::Id laneId, Interfaces::RoadMarking& roadMarking, const RoadSignalInterface& specification) override;
    void AssignRoadMarkingToLane(OWL::Id laneId, Interfaces::RoadMarking& roadMarking, const RoadObjectInterface& specification) override;
    void AssignTrafficLightToLane(OWL::Id laneId, Interfaces::TrafficLight &trafficLight, const RoadSignalInterface& specification) override;

    void RemoveMovingObjectById(Id id) override;

    void AddLaneGeometryPoint(const RoadLaneInterface& odLane,
                              const Common::Vector2d& pointLeft,
                              const Common::Vector2d& pointCenter,
                              const Common::Vector2d& pointRight,
                              const double sOffset,
                              const double t_left,
                               const double t_right,
                              const double curvature,
                              const double heading) override;

    void AddCenterLinePoint(const RoadLaneSectionInterface& odSection,
                            const Common::Vector2d& pointCenter,
                            const double sOffset,
                            double heading) override;

    /**
     * @brief Retrieves the StationaryObjects
     * 
     * @return the map which assigns stationary objects to ids
    */
    const IdMapping<StationaryObject>& GetStationaryObjects() const;

    const Interfaces::StationaryObject& GetStationaryObject(Id id) const override;
    const Interfaces::MovingObject& GetMovingObject(Id id) const override;

    const RoadGraph& GetRoadGraph() const override;
    const RoadGraphVertexMapping& GetRoadGraphVertexMapping() const override;
    const IdMapping<Lane>& GetLanes() const override;
    const Lane& GetLane(Id id) const override;
    const LaneBoundary& GetLaneBoundary(Id id) const override;
    const TurningRates& GetTurningRates() const override;
    const std::unordered_map<std::string, Road*>& GetRoads() const override;
    const std::map<std::string, Junction*>& GetJunctions() const override;
    Interfaces::TrafficSign& GetTrafficSign(Id id) override;
    Interfaces::TrafficLight& GetTrafficLight(Id id) override;
    const Implementation::InvalidLane& GetInvalidLane() const override {return invalidLane;}

    const std::unordered_map<std::string, Id>& GetTrafficSignIdMapping() const override
    {
        return trafficSignIdMapping;
    }

    SignalType GetSignalType(Id id) const override;

    void SetEnvironment(const openScenario::EnvironmentAction& environment) override;

    /// @brief Check if close to sector boundaries
    /// @param distanceToCenter         Distance to the center
    /// @param angle                    Angle of curvature
    /// @param leftBoundaryAngle        Angle of curvature of left boundary
    /// @param rightBoundaryAngle       Angle of curvature of right boundary
    /// @param maxDistanceToBoundary    Maximum distance to boundary
    /// @return True, if close to sector boundary
    static bool IsCloseToSectorBoundaries(double distanceToCenter,
                                          double angle,
                                          double leftBoundaryAngle,
                                          double rightBoundaryAngle,
                                          double maxDistanceToBoundary)
    {
        const double rightAngleDifference = angle - rightBoundaryAngle;
        const double leftAngleDifference = angle - leftBoundaryAngle;

        //For angles > 90° the center of the sector is the closest point
        const double distanceToRightBoundary = std::abs(rightAngleDifference) >= M_2_PI ? distanceToCenter
                                                                                        : distanceToCenter * std::sin(rightAngleDifference);
        const double distanceToLeftBoundary = std::abs(leftAngleDifference) >= M_2_PI ? distanceToCenter
                                                                                      : distanceToCenter * std::sin(leftAngleDifference);
        return distanceToRightBoundary <= maxDistanceToBoundary || distanceToLeftBoundary <= maxDistanceToBoundary;
    }

    /*!
     * \brief Applies a sector-shaped filter on a list of objects
     *
     * \param[in]   objects               List of objects to apply filter to
     * \param[in]   origin                Origin of sector-shape
     * \param[in]   radius                Radius of the sector-shape. Negative value results in empty result
     * \param[in]   leftBoundaryAngle     Angle of left boundary of sector-shape (view from origin) [-PI, PI] [rad]
     * \param[in]   rightBoundaryAngle    Angle of right boundary of sector-shape (view from origin) [-PI, PI] [rad]
     *
     * \return      Vector of objects inside the sector-shape
     */
    template<typename T>
    std::vector<const T*> ApplySectorFilter(const std::vector<T*>& objects,
                                            const Primitive::AbsPosition& origin,
                                            double radius,
                                            double leftBoundaryAngle,
                                            double rightBoundaryAngle)
    {
        std::vector<const T*> filteredObjects;
        constexpr double EPS = 1e-9;
        bool anglesDiffer = std::abs(leftBoundaryAngle - rightBoundaryAngle) > EPS;

        if (!anglesDiffer || radius <= 0.0)
        {
            return filteredObjects;
        }
        bool wrappedAngle = leftBoundaryAngle < rightBoundaryAngle;
        anglesDiffer = std::abs(leftBoundaryAngle - rightBoundaryAngle) > EPS;

        for (const auto& object : objects)
        {
            const auto& absPosition = object->GetReferencePointPosition();
            const auto relativePosition = absPosition - origin;
            const auto distance = relativePosition.distance();
            const auto dimension = object->GetDimension();
            const auto diagonal = openpass::hypot(dimension.width, dimension.length);

            if (distance > radius + diagonal)
            {
                continue;
            }

            if (anglesDiffer && distance > 0.0)
            {
                double direction = std::atan2(relativePosition.y, relativePosition.x);

                if (wrappedAngle)
                {
                    if (direction < rightBoundaryAngle && direction > leftBoundaryAngle)
                    {
                        if (!IsCloseToSectorBoundaries(distance, direction, leftBoundaryAngle, rightBoundaryAngle, diagonal))
                        {
                            continue;
                        }
                    }
                }
                else if (direction < rightBoundaryAngle || direction > leftBoundaryAngle)
                {
                    if (!IsCloseToSectorBoundaries(distance, direction, leftBoundaryAngle, rightBoundaryAngle, diagonal))
                    {
                        continue;
                    }
                }
            }

            filteredObjects.push_back(object);
        }

        return filteredObjects;
    }

    void Reset() override;

    /**
     * @brief Initializes OSI traffic light with default values
     * 
     * @param[in] pTrafficLight OSI traffic light
     */
    void initializeDefaultTrafficLight(osi3::TrafficLight *pTrafficLight);

protected:
    //-----------------------------------------------------------------------------
    //! Provides callback to LOG() macro
    //!
    //! @param[in]     logLevel    Importance of log
    //! @param[in]     file        Name of file where log is called
    //! @param[in]     line        Line within file where log is called
    //! @param[in]     message     Message to log
    //-----------------------------------------------------------------------------
    void Log(CbkLogLevel logLevel,
             const char* file,
             int line,
             const std::string& message)
    {
        if (callbacks)
        {
            callbacks->Log(logLevel,
                           file,
                           line,
                           message);
        }
    }

private:
    const CallbackInterface* callbacks;
    uint64_t next_free_uid{0};
    std::unordered_map<std::string, Id>         trafficSignIdMapping;

    IdMapping<Lane>                         lanes;
    IdMapping<LaneBoundary>                 laneBoundaries;
    IdMapping<LogicalLaneBoundary>          logicalLaneBoundaries;
    IdMapping<ReferenceLine>                referenceLines;
    IdMapping<StationaryObject>             stationaryObjects;
    IdMapping<MovingObject>                 movingObjects;
    IdMapping<Interfaces::TrafficSign>      trafficSigns;
    IdMapping<Interfaces::TrafficLight>     trafficLights;
    IdMapping<Interfaces::RoadMarking>      roadMarkings;

    std::unordered_map<std::string, Road*> roadsById;
    std::map<std::string, Junction*> junctionsById;

    std::unordered_map<const RoadInterface*, std::unique_ptr<Road>>                 roads;
    std::unordered_map<const RoadLaneSectionInterface*, std::unique_ptr<Section>>   sections;
    std::unordered_map<const JunctionInterface*, std::unique_ptr<Junction>>         junctions;

    std::unordered_map<const RoadLaneInterface*, osi3::Lane*> osiLanes;

    RoadGraph roadGraph;
    RoadGraphVertexMapping vertexMapping;
    TurningRates turningRates;

#ifdef USE_PROTOBUF_ARENA
    google::protobuf::Arena arena;
    google::protobuf::Arena tempArena;
#endif
    GroundTruth_ptr osiGroundTruth;

    const Implementation::InvalidLane invalidLane;
    };

}
