/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * \file  basicDataBufferGlobal.h
 * \brief This file contains DLL export declarations
 */

#pragma once

#include <QtGlobal>

#if defined(BASIC_DATABUFFER_LIBRARY)
/// Export of the dll-functions
#  define BASIC_DATABUFFER_SHARED_EXPORT Q_DECL_EXPORT
#else
/// Import of the dll-functions
#  define BASIC_DATABUFFER_SHARED_EXPORT Q_DECL_IMPORT
#endif


