/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  manipulatorLibrary.h
//! @brief This file contains the internal representation of the library of a
//!        manipulator.
//-----------------------------------------------------------------------------

#pragma once

#include <QLibrary>
#include "bindings/manipulatorBinding.h"
#include "include/manipulatorInterface.h"
#include "include/callbackInterface.h"

class PublisherInterface;

namespace core
{

//! This class represents the library of a manipulator
class ManipulatorLibrary
{
public:
    //! Type of function pointer that points to dll-function that obtain the version information of the module
    typedef const std::string &(*ManipulatorInterface_GetVersion)();
    //! Type of function pointer that points to dll-function that creates an instance of the module
    typedef ManipulatorInterface *(*ManipulatorInterface_CreateInstanceType)(WorldInterface *world,
                                                                             openScenario::ManipulatorInformation manipulatorInformation,
                                                                             EventNetworkInterface* eventNetwork,
                                                                             const CallbackInterface *callbacks);
    //! Type of function pointer that points to dll-function that creates a default instance of the module
    typedef ManipulatorInterface *(*ManipulatorInterface_CreateDefaultInstanceType)(WorldInterface *world,
                                                                             std::string manipulatorType,
                                                                             EventNetworkInterface* eventNetwork,
                                                                             const CallbackInterface *callbacks,
                                                                             PublisherInterface* publisher);
    //! Type of function pointer that points to dll-function that destroys/deletes an instance of the module
    typedef void (*ManipulatorInterface_DestroyInstanceType)(ManipulatorInterface *implementation);

    /**
     * @brief ManipulatorLibrary constructor
     * 
     * @param[in] libraryPath   Path to the library
     * @param[in] callbacks     Pointer to the callbacks
     */
    ManipulatorLibrary(const std::string &libraryPath,
                       CallbackInterface *callbacks) :
        libraryPath(libraryPath),
        callbacks(callbacks)
    {}
    ManipulatorLibrary(const ManipulatorLibrary&) = delete;
    ManipulatorLibrary(ManipulatorLibrary&&) = delete;
    ManipulatorLibrary& operator=(const ManipulatorLibrary&) = delete;
    ManipulatorLibrary& operator=(ManipulatorLibrary&&) = delete;

    //-----------------------------------------------------------------------------
    //! Destructor, deletes the stored library (unloads it if necessary), if the list
    //! of stored spawn points is empty.
    //-----------------------------------------------------------------------------
    virtual ~ManipulatorLibrary();

    //-----------------------------------------------------------------------------
    //! Creates a QLibrary based on the path from the constructor and stores function
    //! pointer for getting the library version, creating and destroying instances
    //! and setting the spawn item (see typedefs for corresponding signatures).
    //!
    //! @return                 Null pointer
    //-----------------------------------------------------------------------------
    bool Init();

    //-----------------------------------------------------------------------------
    //! Find manipulator in the sored list of manipulators, then call the "destroy
    //! instance" function pointer with its implementation and remove it from the list
    //! of stored manipulators.
    //!
    //! @param[in]  manipulator         Manipulator to release
    //! @return                         Flag if the release was successful
    //-----------------------------------------------------------------------------
    bool ReleaseManipulator(Manipulator *manipulator);

    //! Make sure that the library exists and is loaded, then call the "create instance"
    //! function pointer using the parameters from the manipulator instance to get
    //! a manipulator interface, which is then used to instantiate a manipulator
    //! which is also stored in the list of manipulators.
    //!
    //! @param[in]  manipulatorInformation  Manipulator information containing actions of the scenario
    //! @param[in]  eventNetwork            The event network within which the manipulator inserts events
    //! @param[in]  world                   World instance
    //! @return                             Instance of manipulator
    //-----------------------------------------------------------------------------
    Manipulator *CreateManipulator(const openScenario::ManipulatorInformation manipulatorInformation,
                                   EventNetworkInterface* eventNetwork,
                                   WorldInterface* world);
    //-----------------------------------------------------------------------------
    //! Make sure that the library exists and is loaded, then call the "create default instance"
    //! function pointer using the parameters from the manipulator instance to get
    //! a manipulator interface, which is then used to instantiate a manipulator
    //! which is also stored in the list of manipulators.
    //!
    //! @param[in]  manipulatorType     The type of the manipulator to be instantiated
    //! @param[in]  eventNetwork        The event network within which the manipulator inserts events
    //! @param[in]  world               World instance
    //! @param[in]  publisher           Publisher instance
    //! @return                         Instance of manipulator
    //-----------------------------------------------------------------------------
    Manipulator *CreateManipulator(const std::string& manipulatorType,
                                   EventNetworkInterface* eventNetwork,
                                   WorldInterface* world,
                                   PublisherInterface* publisher);

private:
    const std::string DllGetVersionId = "OpenPASS_GetVersion";
    const std::string DllCreateInstanceId = "OpenPASS_CreateInstance";
    const std::string DllCreateDefaultInstanceId = "OpenPASS_CreateDefaultInstance";
    const std::string DllDestroyInstanceId = "OpenPASS_DestroyInstance";

    std::string libraryPath;
    std::vector<Manipulator*> manipulators;
    QLibrary *library = nullptr;
    CallbackInterface *callbacks;
    ManipulatorInterface_GetVersion getVersionFunc;
    ManipulatorInterface_CreateInstanceType createInstanceFunc;
    ManipulatorInterface_CreateDefaultInstanceType createDefaultInstanceFunc;
    ManipulatorInterface_DestroyInstanceType destroyInstanceFunc;
};

} // namespace core


