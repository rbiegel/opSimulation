/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  schedulerTasks.h
//! @brief This file contains the generic schedule managing component
//-----------------------------------------------------------------------------

#pragma once

#include <list>
#include <set>

#include "tasks.h"

namespace core::scheduling {

/**
 * @brief managing timing of all given tasks
 * @details The SchedulerTasks class sorts tasks for each phase (bootstrap,
 *           spawning, pre-agent, nonrecurring, recurring agent, synchronize, 
 *           finalize)
 *           Returns all tasks for given timestamp.
 *
 * @ingroup opSimulation
 */
class SchedulerTasks
{
public:
    /**
     * @brief SchedulerTasks constructor
     * 
     * @param[in] bootstrapTasks                List of TaskItems all bootstrap tasks
     * @param[in] spawningTasks                 List of TaskItems all spawning tasks
     * @param[in] preAgentTasks                 List of TaskItems all pre-agent tasks
     * @param[in] synchronizeTasks              List of TaskItems all synchronize tasks
     * @param[in] finalizeTasks                 List of TaskItems all finalize tasks
     * @param[in] scheduledTimestampsInterval   Scheduled timestamps
     */
    SchedulerTasks(std::vector<TaskItem> bootstrapTasks,
                   std::vector<TaskItem> spawningTasks,
                   std::vector<TaskItem> preAgentTasks,
                   std::vector<TaskItem> synchronizeTasks,
                   std::vector<TaskItem> finalizeTasks,
                   int scheduledTimestampsInterval);

    /**
    * @brief ScheduleNewRecurringTasks
    *
    * @details insert given tasks to intern recurring tasks
    *
    * @param[in]     newTasks   List of TaskItems      new tasks
    */
    void ScheduleNewRecurringTasks(std::vector<TaskItem> newTasks);

    /**
    * @brief ScheduleNewNonRecurringTasks
    *
    * @details insert given tasks to intern nonrecurring tasks
    *
    * @param[in]    newTasks    List of TaskItems      new tasks
    */
    void ScheduleNewNonRecurringTasks(std::vector<TaskItem> newTasks);

    /**
    * @brief DeleteAgentTasks
    *
    * @details filter all tasks to remove tasks of given agent id
    *
    * @param[in]     agents     List of int agent ids to remove from tasks
    */
    void DeleteAgentTasks(std::vector<int> &agents);

    /**
    * @brief DeleteAgentTasks
    *
    * @details filter all tasks to remove tasks of given agent id
    *
    * @param[in]     agentId    Id of the agent
    */
    void DeleteAgentTasks(int agentId);

    /**
    * @brief GetNextTimestamp
    *
    * @details calculates with scheduledTimestamps next timestamp
    *
    * @param[in]    timestamp   Timestamp
    * @return    next timestamp
    */
    int GetNextTimestamp(int timestamp);

    /**
    * @brief GetTasks
    *
    * @param[in]     timestamp  Timestamp
    * @return    list of TaskItems  all tasks for given timestamp
    */
    std::vector<TaskItem> GetTasks(int timestamp);

    /**
    * @brief GetSpawningTasks
    *
    * @param[in]    timestamp  Timestamp
    * @return    list of TaskItems  all spawning tasks for given timestamp
    */
    std::vector<TaskItem> GetSpawningTasks(int timestamp);

    /**
    * @brief GetPreAgentTasks
    *
    * @param[in]    timestamp   Timestamp
    * @return       list of TaskItems all preAgent tasks for given timestamp
    */
    std::vector<TaskItem> GetPreAgentTasks(int timestamp);

    /**
    * @brief ConsumeNonRecurringTasks
    *
    * @details calls PullNonRecurringTasks
    *
    * @param[in]    timestamp  Timestamp
    * @return    list of TaskItems  all init tasks for given timestamp
    */
    std::vector<TaskItem> ConsumeNonRecurringAgentTasks(int timestamp);

    /**
    * @brief PullNonRecurringTasks
    *
    * @details clear all init tasks after returning them
    *
    * @param[in]    timestamp      Timestamp
    * @param[out]   currentTasks   List of TaskItems  all init tasks for given timestamp
    */
    void PullNonRecurringTasks(int timestamp, std::vector<TaskItem> &currentTasks);

    /**
    * @brief GetRecurringAgentTasks
    *
    * @param[in]    timestamp   Timestamp
    * @return       list of TaskItems all recurring agent tasks for given timestamp
    */
    std::vector<TaskItem> GetRecurringAgentTasks(int timestamp);

    /**
    * @brief GetSynchronizeTasks
    *
    * @param[in]    timestamp   Timestamp
    * @return       list of TaskItems all synchronize tasks for given timestamp
    */
    std::vector<TaskItem> GetSynchronizeTasks(int timestamp);

    /**
    * @brief GetBootstrapTasks
    *
    * @return       list of TaskItems all bootstrap tasks
    */
    std::multiset<TaskItem> GetBootstrapTasks();

    /**
    * @brief GetFinalizeTasks
    *
    * @return       list of TaskItems all finalize tasks
    */
    std::multiset<TaskItem> GetFinalizeTasks();

    /// Container for scheduled timestamps
    std::set<int> scheduledTimestamps;

    /// Bootstrap tasks
    Tasks bootstrapTasks;
    /// Spawning tasks
    Tasks spawningTasks;
    /// Pre-agent tasks
    Tasks preAgentTasks;
    /// Nonrecurring tasks
    Tasks nonRecurringAgentTasks;
    /// Recurring agent tasks
    Tasks recurringAgentTasks;
    /// Synchronize tasks
    Tasks synchronizeTasks;
    /// Finalize tasks
    Tasks finalizeTasks;

private:
    /**
    * @brief CreateNewScheduledTimestamps
    *
    * @details renew scheduled timestamps
    */
    void CreateNewScheduledTimestamps();

    /**
    * @brief GetTasks
    *
    * @param[in]     timestamp          Timestamp
    * @param[in]     tasks              Tasks to filter by current timestamp
    * @param[in]     currentTasks       List of TaskItems  filtered tasks
    */
    void GetTasks(int timestamp, std::multiset<TaskItem> &tasks, std::vector<TaskItem> &currentTasks);

    /**
    * @brief UpdateScheduledTimestamps
    *
    * @details update scheduled timestamps with new schedule times
    *
    * @param[in]     cycleTime  Cycletime of task
    * @param[in]     delay      Delay/offset of task
    */
    void UpdateScheduledTimestamps(int cycleTime, int delay);

    /**
    * @brief UpdateScheduledTimestamps
    *
    * @details call UpdateScheduledTimestamps for each task
    *
    * @param[out]     tasks     New scheduled tasks
    */
    void UpdateScheduledTimestamps(std::multiset<TaskItem> &tasks);

    /**
    * @brief ScheduleNewTasks
    *
    * @details call UpdateScheduledTimestamps for each task
    *
    * @param[out]     tasks     New scheduled tasks
    */
    void ScheduleNewTasks(Tasks &tasks, std::vector<TaskItem> newTasks);

    /**
    * @brief ClearNonrecurringTasks
    *
    * @details clears non recurring tasks
    */
    void ClearNonrecurringTasks();

    /**
    * @brief ExpandUpperBoundary
    *
    * @details if reaching upper limit of currently scheduled timestamps
    *           next timestamps have to be calculated
    *
    * @param[in]     timestamp     Timestamp
    */
    void ExpandUpperBoundary(int timestamp);

    int scheduledTimestampsInterval;
    int upperBoundOfScheduledTimestamps;
    int lowerBoundOfScheduledTimestamps;
};

} // namespace openpass::scheduling
