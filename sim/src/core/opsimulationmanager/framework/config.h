/********************************************************************************
 * Copyright (c) 2017-2018 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  config.h
//! @brief This file contains the representation of opSimulationManager
//!        configuration.
//-----------------------------------------------------------------------------

#pragma once

#include <string>
#include <optional>
#include <vector>

namespace SimulationManager {
namespace Configuration {

class SimulationConfig;
using SimulationConfigs = std::vector<SimulationConfig>; ///< Simulation configurations as the list of simulation config

///
/// \brief Used as value storage of parsed configuration parameters
///
struct Config
{
public:

    /// @brief Configuration parameters constructor
    /// @param logLevel                     Level of the log
    /// @param logFileOpSimulationManager   Openpass log file simulation manager
    /// @param simulation                   TODO
    /// @param libraries 
    /// @param simulationConfigs 
    Config(
        std::optional<int> logLevel,
        std::optional<std::string> logFileOpSimulationManager,
        std::optional<std::string> simulation,
        std::optional<std::string> libraries,
        SimulationConfigs simulationConfigs) :
        logLevel{CheckOrDefault(logLevel.value_or(defaultLogLevel))},
        logFileOpSimulationManager{logFileOpSimulationManager.value_or(defaultLogFileOpSimulationManager)},
        simulation{simulation.value_or(defaultSimulation)},
        libraries{libraries.value_or(defaultLibraries)},
        simulationConfigs{simulationConfigs}
    {}

    Config() :
        logLevel{defaultLogLevel},
        logFileOpSimulationManager{defaultLogFileOpSimulationManager},
        simulation{defaultSimulation},
        libraries{defaultLibraries},
        simulationConfigs{}
    {}

    const int logLevel;                             ///< TODO
    const std::string logFileOpSimulationManager;   ///< TODO
    const std::string simulation;                   ///< TODO
    const std::string libraries;                    ///< TODO
    const SimulationConfigs simulationConfigs;      ///< TODO

private:
    static constexpr int defaultLogLevel = 0;
    static constexpr char defaultLogFileOpSimulationManager[] = "opSimulationManager.log";
    static constexpr char defaultSimulation[] = "opSimulation";
    static constexpr char defaultLibraries[] = "lib";

    //-------------------------------------------------------------------------
    //! \brief Checks if the passed value is in between the minimum and maximum
    //!  	   log level boundaries.
    //! \param[in] logLevel Value to be checked.
    //! \returns logLevel if passed, defaultLogLevel if not
    //-------------------------------------------------------------------------
    static int CheckOrDefault(int logLevel)
    {
        return (logLevel >= 0 && logLevel <= 5) ? logLevel : defaultLogLevel;
    }
};

} // namespace Configuration
} // namespace SimulationManager
