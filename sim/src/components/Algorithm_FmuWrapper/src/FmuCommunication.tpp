#include "fmuFileHelper.h"

#include <sstream>

template <size_t FMI>
FmuCommunication<FMI>::FmuCommunication(std::string componentName,
                                        fmu_check_data_t& cdata,
                                        std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                                        const ParameterInterface *parameters,
                                        WorldInterface *world,
                                        AgentInterface *agent,
                                        const CallbackInterface *callbacks) :
        cdata(cdata),
        componentName(componentName),
        callbacks(callbacks),
        agentIdString(FmuFileHelper::CreateAgentIDString(agent->GetId()))
{
}

template <size_t FMI>
void FmuCommunication<FMI>::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
    if (callbacks)
    {
        callbacks->Log(logLevel, file, line, message);
    }
}

template <size_t FMI>
void FmuCommunication<FMI>::SetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue> fmuValuesIn, size_t size, VariableType dataType)
{
    fmi_t_data dataIn;
    int i = 0;
    switch(dataType)
    {
        case VariableType::String:
            dataIn.emplace<std::vector<fmi_string_t>>();
            std::get<std::vector<fmi_string_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_string_t>>(dataIn)[i++].emplace<FMI>(fmuValue.stringValue);
            }
            SetStringFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_string_t>>(dataIn), size);
            break;
        case VariableType::Double:
            dataIn.emplace<std::vector<fmi_real_t>>();
            std::get<std::vector<fmi_real_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_real_t>>(dataIn)[i++].emplace<FMI>(fmuValue.realValue);
            }
            SetRealFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_real_t>>(dataIn), size);
            break;
        case VariableType::Int:
        case VariableType::Enum:
            dataIn.emplace<std::vector<fmi_integer_t>>();
            std::get<std::vector<fmi_integer_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_integer_t>>(dataIn)[i++].emplace<FMI>(fmuValue.intValue);
            }
            SetIntegerFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_integer_t>>(dataIn), size);
            break;
        case VariableType::Bool:
            dataIn.emplace<std::vector<fmi_boolean_t>>();
            std::get<std::vector<fmi_boolean_t>>(dataIn).resize(fmuValuesIn.size());
            for(FmuValue fmuValue : fmuValuesIn)
            {
                std::get<std::vector<fmi_boolean_t>>(dataIn)[i++].emplace<FMI>(fmuValue.boolValue);
            }
            SetBooleanFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_boolean_t>>(dataIn), size);
            break;
        default:
            LOGWARN("Could not set value on FMU, because of unknown datatype");
            break;
    }
}

template <size_t FMI>
void FmuCommunication<FMI>::GetFMI(std::vector<int> valueReferencesVec, std::vector<FmuValue>& fmuValuesOut, size_t size, VariableType dataType)
{
    fmi_t_data dataOut;
    int i = 0;
    switch(dataType)
    {
        case VariableType::String:
        {
            dataOut.emplace<std::vector<fmi_string_t>>();
            GetStringFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_string_t>>(dataOut), size);
            auto dataOutConverted = std::get<std::vector<fmi_string_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_string_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.stringValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Double:
        {
            dataOut.emplace<std::vector<fmi_real_t>>();
            GetRealFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_real_t>>(dataOut), size);
            auto dataOutConverted = std::get<std::vector<fmi_real_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_real_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.realValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Int:
        case VariableType::Enum:
        {
            dataOut.emplace<std::vector<fmi_integer_t>>();
            GetIntegerFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_integer_t>>(dataOut), size);
            fmuValuesOut.resize(std::get<std::vector<fmi_integer_t>>(dataOut).size());
            for(fmi_integer_t dataOutEntry : std::get<std::vector<fmi_integer_t>>(dataOut))
            {
                FmuValue fmuValue;
                fmuValue.intValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        case VariableType::Bool:
        {
            dataOut.emplace<std::vector<fmi_boolean_t>>();
            GetBooleanFMIFromIntRef(valueReferencesVec, std::get<std::vector<fmi_boolean_t>>(dataOut), size);
            auto dataOutConverted = std::get<std::vector<fmi_boolean_t>>(dataOut);
            fmuValuesOut.resize(dataOutConverted.size());
            for(fmi_boolean_t dataOutEntry : dataOutConverted)
            {
                FmuValue fmuValue;
                fmuValue.boolValue = std::get<FMI>(dataOutEntry);
                fmuValuesOut[i++] = fmuValue;
            }
            break;
        }
        default:
            LOGWARN("FmuCommunication: Could not get value on FMU, because of unknown datatype");
            break;
    }

}

template <size_t FMI>
void FmuCommunication<FMI>::SetStringFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_string_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);;
    fmi_string_t_data dataIn = FmuHelper::FmiDataFromString<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetStringFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetStringFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}
template <size_t FMI>
void FmuCommunication<FMI>::SetRealFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_real_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);;
    fmi_real_t_data dataIn = FmuHelper::FmiDataFromReal<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetRealFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetRealFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);
}
template <size_t FMI>
void FmuCommunication<FMI>::SetIntegerFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_integer_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);;
    fmi_integer_t_data dataIn = FmuHelper::FmiDataFromInteger<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetIntegerFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetIntegerFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}
template <size_t FMI>
void FmuCommunication<FMI>::SetBooleanFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_boolean_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);;
    fmi_boolean_t_data dataIn = FmuHelper::FmiDataFromBoolean<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetBooleanFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetBooleanFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}

template <size_t FMI>
void FmuCommunication<FMI>::SetStringFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_string_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromVector<FMI>(valueReferencesVec, size);;
    fmi_string_t_data dataIn = FmuHelper::FmiDataFromString<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetStringFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetStringFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}
template <size_t FMI>
void FmuCommunication<FMI>::SetRealFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_real_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromVector<FMI>(valueReferencesVec, size);;
    fmi_real_t_data dataIn = FmuHelper::FmiDataFromReal<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetRealFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetRealFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);
}
template <size_t FMI>
void FmuCommunication<FMI>::SetIntegerFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_integer_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromVector<FMI>(valueReferencesVec, size);;
    fmi_integer_t_data dataIn = FmuHelper::FmiDataFromInteger<FMI>(dataVecIn, size);

    for (fmi_integer_t data : dataVecIn)
    {
        std::stringstream ss;
        ss << FmuHelper::log_prefix(agentIdString, componentName);
        ss << " ";
        ss << std::get<FMI>(data);
        LOGDEBUG(ss.str());
    }

    if(FMI == FMI1)
        SetIntegerFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetIntegerFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}
template <size_t FMI>
void FmuCommunication<FMI>::SetBooleanFMI(std::vector<fmi_value_reference_t> valueReferencesVec, std::vector<fmi_boolean_t> dataVecIn, size_t size)
{
    if(valueReferencesVec.size() <= 0 || dataVecIn.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromVector<FMI>(valueReferencesVec, size);;
    fmi_boolean_t_data dataIn = FmuHelper::FmiDataFromBoolean<FMI>(dataVecIn, size);

    if(FMI == FMI1)
        SetBooleanFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataIn), size);
    else if(FMI == FMI2)
        SetBooleanFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataIn), size);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataIn);

}

template <size_t FMI>
void FmuCommunication<FMI>::GetStringFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_string_t>& dataVecOut, size_t size)
{
    if(valueReferencesVec.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);
    dataVecOut.resize(size);
    fmi_string_t_data dataOut;

    if(FMI == FMI1)
    {
        dataOut.emplace<FMI>(new fmi1_string_t[size]);
        GetStringFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataOut), size);
    }
    else if(FMI == FMI2)
    {
        dataOut.emplace<FMI2>(new fmi2_string_t[size]);
        GetStringFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataOut), size);
    }
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    for(int i = 0; i < size; i++)
        dataVecOut[i].emplace<FMI>(std::get<FMI>(dataOut)[i]);

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataOut);

}

template <size_t FMI>
void FmuCommunication<FMI>::GetRealFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_real_t>& dataVecOut, size_t size)
{
    if(valueReferencesVec.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);
    dataVecOut.resize(size);
    fmi_real_t_data dataOut;

    if(FMI == FMI1)
    {
        dataOut.emplace<FMI1>(new fmi1_real_t[size]);
        GetRealFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataOut), size);
    }
    else if(FMI == FMI2)
    {
        dataOut.emplace<FMI2>(new fmi2_real_t[size]);
        GetRealFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataOut), size);
    }
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    for(int i = 0; i < size; i++)
        dataVecOut[i].emplace<FMI>(std::get<FMI>(dataOut)[i]);

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataOut);

}

template <size_t FMI>
void FmuCommunication<FMI>::GetIntegerFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_integer_t>& dataVecOut, size_t size)
{
    if(valueReferencesVec.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);
    dataVecOut.resize(size);
    fmi_integer_t_data dataOut;

    if(FMI == FMI1)
    {
        dataOut.emplace<FMI1>(new fmi1_integer_t[size]);
        GetIntegerFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataOut), size);
    }
    else if(FMI == FMI2)
    {
        dataOut.emplace<FMI2>(new fmi2_integer_t[size]);
        GetIntegerFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataOut), size);
    }
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    for(int i = 0; i < size; i++)
        dataVecOut[i].emplace<FMI>(std::get<FMI>(dataOut)[i]);

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataOut);

}

template <size_t FMI>
void FmuCommunication<FMI>::GetBooleanFMIFromIntRef(std::vector<int> valueReferencesVec, std::vector<fmi_boolean_t>& dataVecOut, size_t size)
{
    if(valueReferencesVec.size() <= 0)
        return;

    fmi_value_references_t valueReferences = FmuHelper::GetReferencesFromIntVector<FMI>(valueReferencesVec, size);
    dataVecOut.resize(size);
    fmi_boolean_t_data dataOut;

    if(FMI == FMI1)
    {
        dataOut.emplace<FMI1>(new fmi1_boolean_t[size]);
        GetBooleanFMI1(std::get<FMI1>(valueReferences), std::get<FMI1>(dataOut), size);
    }
    else if(FMI == FMI2)
    {
        dataOut.emplace<FMI2>(new fmi2_boolean_t[size]);
        GetBooleanFMI2(std::get<FMI2>(valueReferences), std::get<FMI2>(dataOut), size);
    }
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    for(int i = 0; i < size; i++)
        dataVecOut[i].emplace<FMI>(std::get<FMI>(dataOut)[i]);

    delete std::get<FMI>(valueReferences);
    delete std::get<FMI>(dataOut);

}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetStringFMI1(fmi1_value_reference_t valueReferences[], fmi1_string_t dataIn[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_set_string(cdata.fmu1,
                                       valueReferences,       // array of value reference
                                       size,             // number of elements
                                       dataIn);     // array of values

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 string variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 string variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetStringFMI2(fmi2_value_reference_t valueReferences[], fmi2_string_t dataIn[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_set_string(cdata.fmu2,
                                       valueReferences,       // array of value reference
                                       size,             // number of elements
                                       dataIn);     // array of values

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 string variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 string variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetRealFMI1(fmi1_value_reference_t valueReferences[], fmi1_real_t dataIn[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_set_real(cdata.fmu1,
                                     valueReferences,       // array of value reference
                                     size,             // number of elements
                                     dataIn);     // array of values

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 real variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 real variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetRealFMI2(fmi2_value_reference_t valueReferences[], fmi2_real_t dataIn[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_set_real(cdata.fmu2,
                                     valueReferences,       // array of value reference
                                     size,             // number of elements
                                     dataIn);     // array of values

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 real variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 real variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetIntegerFMI1(fmi1_value_reference_t valueReferences[], fmi1_integer_t dataIn[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_set_integer(cdata.fmu1,
                                        valueReferences,       // array of value reference
                                        size,             // number of elements
                                        dataIn);     // array of values

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 integer variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 integer variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetIntegerFMI2(fmi2_value_reference_t valueReferences[], fmi2_integer_t dataIn[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_set_integer(cdata.fmu2,
                                        valueReferences,       // array of value reference
                                        size,             // number of elements
                                        dataIn);     // array of values

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 integer variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 integer variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetBooleanFMI1(fmi1_value_reference_t valueReferences[], fmi1_boolean_t dataIn[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_set_boolean(cdata.fmu1,
                                        valueReferences,       // array of value reference
                                        size,             // number of elements
                                        dataIn);     // array of values

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 boolean variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 1 boolean variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::SetBooleanFMI2(fmi2_value_reference_t valueReferences[], fmi2_boolean_t dataIn[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_set_boolean(cdata.fmu2,
                                        valueReferences,       // array of value reference
                                        size,             // number of elements
                                        dataIn);     // array of values

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 boolean variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Setting a fmi 2 boolean variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetStringFMI1(fmi1_value_reference_t valueReferences[], fmi1_string_t dataout[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_get_string(cdata.fmu1,
                                       valueReferences,
                                       size,
                                       dataout);

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 string variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 string variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetStringFMI2(fmi2_value_reference_t valueReferences[], fmi2_string_t dataout[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_get_string(cdata.fmu2,
                                       valueReferences,
                                       size,
                                       dataout);

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 string variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 string variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetRealFMI1(fmi1_value_reference_t valueReferences[], fmi1_real_t dataout[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_get_real(cdata.fmu1,
                                     valueReferences,
                                     size,
                                     dataout);

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 real variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 real variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetRealFMI2(fmi2_value_reference_t valueReferences[], fmi2_real_t dataout[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_get_real(cdata.fmu2,
                                     valueReferences,
                                     size,
                                     dataout);

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 real variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 real variable returned an error");
    }
    return fmiStatus;
}


template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetIntegerFMI1(fmi1_value_reference_t valueReferences[], fmi1_integer_t dataout[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_get_integer(cdata.fmu1,
                                        valueReferences,
                                        size,
                                        dataout);

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 integer variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 integer variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetIntegerFMI2(fmi2_value_reference_t valueReferences[], fmi2_integer_t dataout[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;

    fmiStatus = fmi2_import_get_integer(cdata.fmu2,
                                        valueReferences,
                                        size,
                                        dataout);

    if(dataout[0] != 0)
    {
        fmi2_status_t fmiStatusx = fmi2_status_ok;
    }
    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 integer variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 integer variable returned an error");
    }
    return fmiStatus;
}


template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetBooleanFMI1(fmi1_value_reference_t valueReferences[], fmi1_boolean_t dataout[], size_t size)
{
    fmi1_status_t fmiStatus = fmi1_status_ok;

    fmiStatus = fmi1_import_get_boolean(cdata.fmu1,
                                        valueReferences,
                                        size,
                                        dataout);

    if (fmiStatus == fmi1_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 boolean variable returned a warning");
    }
    else if (fmiStatus == fmi1_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 1 boolean variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
fmi_status_t FmuCommunication<FMI>::GetBooleanFMI2(fmi2_value_reference_t valueReferences[], fmi2_boolean_t dataout[], size_t size)
{
    fmi2_status_t fmiStatus = fmi2_status_ok;


    fmiStatus = fmi2_import_get_boolean(cdata.fmu2,
                                        valueReferences,
                                        size,
                                        dataout);

    if (fmiStatus == fmi2_status_warning)
    {
        LOGWARN(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 boolean variable returned a warning");
    }
    else if (fmiStatus == fmi2_status_error)
    {
        LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString) + ": Getting a fmi 2 boolean variable returned an error");
    }
    return fmiStatus;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::PrepareFmuInit()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_prep_init(&cdata);
    else if(FMI == FMI2)
        status = fmi2_cs_prep_init(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    if (status == jm_status_error)
    {
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Error in prepare fmu init");
    }

    HandleFmiStatus(status, "prep init");

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiEndHandling()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_end_handling(&cdata);
    else if(FMI == FMI2)
        status = fmi2_end_handling(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    if (status == jm_status_error)
    {
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Error in FMU end handling");
    }

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiSimulateStep(double time)
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_simulate_step(&cdata, time);
    else if(FMI == FMI2)
        status = fmi2_cs_simulate_step(&cdata, time);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    HandleFmiStatus(status, "simulation step");

    return status;
}

template <size_t FMI>
jm_status_enu_t FmuCommunication<FMI>::FmiPrepSimulate()
{
    jm_status_enu_t status;

    if(FMI == FMI1)
        status = fmi1_cs_prep_simulate(&cdata);
    else if(FMI == FMI2)
        status = fmi2_cs_prep_simulate(&cdata);
    else
        LOGERROR(FmuHelper::log_prefix(agentIdString, componentName) + "Unknown fmi version");

    HandleFmiStatus(status, "prep simulate");

    return status;
}

template <size_t FMI>
void FmuCommunication<FMI>::HandleFmiStatus(const jm_status_enu_t &fmiStatus, const std::string &logPrefix)
{
    switch (fmiStatus)
    {
        case jm_status_success:
            LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " successful");
            break;

        case jm_status_warning:
            LOGDEBUG(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " returned with warning");
            break;

        case jm_status_error:
            FmiEndHandling();
            LOGERRORANDTHROW(FmuHelper::log_prefix(agentIdString, componentName) + logPrefix + " returned with error")
    }
}

template <size_t FMI>
FmuVariables FmuCommunication<FMI>::GetFmuVariables()
{
    FmuVariables fmuVariables;

    if (FMI == FMI1)
    {
        std::vector<std::pair<std::string, FmuVariable1>> fmuVariables1;
        fmi1_import_variable_list_t *fmuVariableList = fmi1_import_get_variable_list(cdata.fmu1);
        size_t fmuVariableCount = fmi1_import_get_variable_list_size(fmuVariableList);

        for (size_t i = 0; i < fmuVariableCount; ++i)
        {
            fmi1_import_variable_t *fmuVar = fmi1_import_get_variable(fmuVariableList, static_cast<unsigned int>(i));
            const std::string fmuVarName(fmi1_import_get_variable_name(fmuVar));
            const fmi1_value_reference_t fmuValueReference = fmi1_import_get_variable_vr(fmuVar);
            const fmi1_base_type_enu_t fmuVarType = fmi1_import_get_variable_base_type(fmuVar);
            const VariableType variableType = FmiTypeToCType(fmuVarType);
            const fmi1_causality_enu_t causality = fmi1_import_get_causality(fmuVar);
            const fmi1_variability_enu_t variability = fmi1_import_get_variability(fmuVar);
            fmuVariables1.push_back(std::make_pair(fmuVarName, *std::make_shared<FmuVariable1>(fmuValueReference, variableType, causality, variability)));
        }
        fmuVariables = std::unordered_map<std::string, FmuVariable1>(fmuVariables1.begin(), fmuVariables1.end());
        fmi1_import_free_variable_list(fmuVariableList);
    }
    else if (FMI == FMI2)
    {
        std::vector<std::pair<std::string, FmuVariable2>> fmuVariables2;
        fmi2_import_variable_list_t *fmuVariableList = fmi2_import_get_variable_list(cdata.fmu2, 0);
        size_t fmuVariableCount = fmi2_import_get_variable_list_size(fmuVariableList);

        for (size_t i = 0; i < fmuVariableCount; ++i)
        {

            fmi2_import_variable_t *fmuVar = fmi2_import_get_variable(fmuVariableList, static_cast<unsigned int>(i));
            const std::string fmuVarName(fmi2_import_get_variable_name(fmuVar));
            const fmi2_value_reference_t fmuValueReference = fmi2_import_get_variable_vr(fmuVar);
            const fmi2_base_type_enu_t fmuVarType = fmi2_import_get_variable_base_type(fmuVar);
            const VariableType variableType = FmiTypeToCType(fmuVarType);
            const fmi2_causality_enu_t causality = fmi2_import_get_causality(fmuVar);
            const fmi2_variability_enu_t variability = fmi2_import_get_variability(fmuVar);

            fmuVariables2.push_back(std::make_pair(fmuVarName, *std::make_shared<FmuVariable2>(fmuValueReference, variableType, causality, variability)));
        }
        fmuVariables = std::unordered_map<std::string, FmuVariable2>(fmuVariables2.begin(), fmuVariables2.end());
        fmi2_import_free_variable_list(fmuVariableList);
    }
    else
    {
        LOGERRORANDTHROW("Invalid FMI version");
    }
    return fmuVariables;
}

template <size_t FMI>
VariableType FmuCommunication<FMI>::FmiTypeToCType(const fmi1_base_type_enu_t fmiType)
{
    switch (fmiType)
    {
        case fmi1_base_type_bool:
            return VariableType::Bool;

        case fmi1_base_type_int:
            return VariableType::Int;

        case fmi1_base_type_real:
            return VariableType::Double;

        case fmi1_base_type_str:
            return VariableType::String;

        case fmi1_base_type_enum:
            return VariableType::Enum;

        default:
            throw std::runtime_error("Invalid type is not supported.");
    }
}

template <size_t FMI>
VariableType FmuCommunication<FMI>::FmiTypeToCType(const fmi2_base_type_enu_t fmiType) {
    switch (fmiType) {
        case fmi2_base_type_bool:
            return VariableType::Bool;

        case fmi2_base_type_int:
            return VariableType::Int;

        case fmi2_base_type_real:
            return VariableType::Double;

        case fmi2_base_type_str:
            return VariableType::String;

        case fmi2_base_type_enum:
            return VariableType::Enum;

        default:
            throw std::runtime_error("Invalid type is not supported.");
    }
}