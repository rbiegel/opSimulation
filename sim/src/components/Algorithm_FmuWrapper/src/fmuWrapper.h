/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2017-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/

#pragma once

#include "include/modelInterface.h"
#include "common/primitiveSignals.h"
#include "FmuCommunication.h"

#include <cassert>
#include <cerrno>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <tuple>
#include <typeindex>
#include <unordered_map>
#include <vector>
#include <map>
#include <filesystem>

extern "C" {
#include "fmilib.h"
#include "fmuChecker.h"
}

#include "include/fmuHandlerInterface.h"
#include "include/fmuWrapperInterface.h"


std::string log_prefix(const std::string &agentIdString, const std::string &componentName);

static constexpr bool DEFAULT_LOGGING {true};
static constexpr bool DEFAULT_CSV_OUTPUT {true};

/// class representing implementation of algorithm for FmuWrapper
class AlgorithmFmuWrapperImplementation : public UnrestrictedModelInterface, public FmuWrapperInterface
{
public:
    /// Component Name
    const std::string COMPONENTNAME = "AlgorithmFmuWrapper";

    AlgorithmFmuWrapperImplementation(std::string componentName,
                                        bool isInit,
                                        int priority,
                                        int offsetTime,
                                        int responseTime,
                                        int cycleTime,
                                        WorldInterface* world,
                                        StochasticsInterface *stochastics,
                                        const ParameterInterface *parameters,
                                        PublisherInterface * const publisher,
                                        const CallbackInterface *callbacks,
                                        AgentInterface* agent);

    AlgorithmFmuWrapperImplementation(const AlgorithmFmuWrapperImplementation&) = delete;
    AlgorithmFmuWrapperImplementation(AlgorithmFmuWrapperImplementation&&) = delete;
    AlgorithmFmuWrapperImplementation& operator=(const AlgorithmFmuWrapperImplementation&) = delete;
    AlgorithmFmuWrapperImplementation& operator=(AlgorithmFmuWrapperImplementation&&) = delete;

    virtual ~AlgorithmFmuWrapperImplementation();

    /*!
     * \brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update taks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * \param[in]     data           Referenced signal (copied by sending component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;
    
    /*!
     * \brief Update outputs.
     *
     * Function is called by framework when this Component.has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * \param[out]    data           Referenced signal (copied by this component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;
    
    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    void Trigger(int time) override;
    void Init() override;

    [[nodiscard]] const FmuHandlerInterface *GetFmuHandler() const override;
    [[nodiscard]] const FmuVariables &GetFmuVariables() const override;

    /// @brief Get FmuValue
    /// @param valueReference 
    /// @param variableType     Type of variable
    /// @return Returns FMU value
    [[nodiscard]] const FmuValue& GetValue(int valueReference, VariableType variableType) const override;
    
    /// @brief Set FmuValue
    /// @param valueReference 
    /// @param variableType     Type of variable
    /// @param fmuValue         FMU value
    void SetValue(const FmuValue &fmuValue, int valueReference, VariableType variableType) override;

    /// @return Returns the priority of the FMU
    [[nodiscard]] int getPriority() const override;

    /// @return Returns the FMI version
    fmi_version_enu_t getFmiVersion() override;

    /// @brief Set FMU values
    /// @param valueReferences  List of FMU value references 
    /// @param fmuValuesIn      List of FMU values
    /// @param dataType         Variable data type
    void SetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType) override;

    /// @brief Get FMU values
    /// @param valueReferences  List of FMU value references 
    /// @param fmuValuesOut     List of FMU values
    /// @param dataType         Variable data type    
    void GetFmuValues(std::vector<int> valueReferences, std::vector<FmuValue>& fmuValuesOut, VariableType dataType) override;

private:

    /*!
     * \brief Constructs an output base path from the core results directory and agent id
     */
    void SetOutputPath();

    /*!
     * \brief Sets up filenames for FMU data output and logging.
     */
    void SetupFilenames();

    /*!
     * \brief Sets up FMU checker log output.
     *
     * Directory "Log" will be created beneath FMU path.
     */
    void SetupLog();

    /*!
     * \brief Sets up FMU data log output.
     *
     * Directory "Output" will be created beneath FMU path.
     */
    void SetupOutput();

    /*!
     * \brief Sets up FMU unzipping into unique paths and settings.
     */
    void SetupUnzip();

    /*!
     * \brief Initializes the FMU
     *
     * Calls the FMU checker which unzips and validates the FMU and parses the configuration
     * of the FMU. The type of the FMU is detectd and stored for later use. After that an
     * initializing simulation step is performed on the FMU.
     *
     * Supported types of FMU are ACC and SWW
     */
    void InitFmu();

    /*!
     * \brief Safely cretes a directory.
     *
     * Create a directory with all missing directory along its path. Throws an error
     * if the directory cannot be created for any reason.
     *
     * \param[in]   path        Path of directory to be created (relative or absolute)
     *
     * \throws      std::runtime_error
     */
    void MkDirOrThrowError(const std::filesystem::path& path);

    struct fmu_check_data_t cdata;   //!< check data to be passed around between the FMIL functions

    int startTime;               //!< Time of FMU start (simulation timebase)

    const CallbackInterface*  callbacks;     //!< callback interface

    std::string FMU_absPath;        //!< Absolute path to the FMU file including
    std::string FMU_configPath;     //!< Relative path to the FMU file (originating in core config directory)
    std::string tmpPath;            //!< Temporary path used for unzipping the FMU archive
    std::string outputPath;         //!< Output base directory (inside core results directory)
    std::string logFileFullName;    //!< Absolute path to the log file
    std::string logFileName;        //!< Name of the log file
    std::string outputFileFullName; //!< Absolute path to the CSV output file
    std::string outputFileName;     //!< Name of the CSV output file

    std::string agentIdString;    //!< agent identifier as string

    //! Mapping from FMU variable name to it's FMI value reference and C++ type id. Provided to type-specific wrapper implementation on construction.
    FmuVariables fmuVariables;

    //! Mapping from FMI value reference and C++ type id to FmuWrapper value (union). Provided to type-specific wrapper implementation on construction.
    std::map<ValueReferenceAndType, FmuValue> fmuVariableValues;

    bool isInitialized{false};                                                              //!< Specifies, if the FMU has already be initialized
    FmuHandlerInterface* fmuHandler = nullptr;                                              //!< Points to the instance of the FMU type-specific implementation
    std::string fmuType;        //!< Type of the FMU
    std::string componentName;
    int priority;
};
