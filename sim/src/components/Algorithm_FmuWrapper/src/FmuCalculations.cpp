/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2019-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/

#include "FmuCalculations.h"
#include "common/worldDefinitions.h"


double FmuCalculations::CalculateSpeedLimit(double range, AgentInterface* agent)
{
    double speedLimit = 999.0;
    auto trafficSigns = agent->GetEgoAgent().GetTrafficSignsInRange(range);
    auto trafficSignsBehind = agent->GetEgoAgent().GetTrafficSignsInRange(std::numeric_limits<double>::lowest());
    trafficSigns.insert(trafficSigns.end(), trafficSignsBehind.begin(), trafficSignsBehind.end());
    std::sort(trafficSigns.begin(), trafficSigns.end(), [](const CommonTrafficSign::Entity& lhs, const CommonTrafficSign::Entity& rhs)
    {return lhs.relativeDistance > rhs.relativeDistance;});
    for (const auto& sign : trafficSigns)
    {
        if (sign.type == CommonTrafficSign::Type::EndOfMaximumSpeedLimit
            || sign.type == CommonTrafficSign::Type::EndOffAllSpeedLimitsAndOvertakingRestrictions
            || sign.type == CommonTrafficSign::Type::SpeedLimitZoneEnd
            || sign.type == CommonTrafficSign::Type::TrafficCalmedDistrictEnd)
        {
            break;
        }
        if (sign.type == CommonTrafficSign::Type::MaximumSpeedLimit
            || sign.type == CommonTrafficSign::Type::SpeedLimitZoneBegin)
        {
            speedLimit = sign.value;
            break;
        }
        if (sign.type == CommonTrafficSign::Type::TrafficCalmedDistrictBegin)
        {
            speedLimit = 7.0 /3.6;
            break;
        }
    }
    return speedLimit;
}

int FmuCalculations::CalculateLaneCount(Side side, AgentInterface* agent)
{
    if (!agent->GetEgoAgent().HasValidRoute())
    {
        return 0;
    }
    const auto relativeLanes = agent->GetEgoAgent().GetRelativeLanes(0).at(0);
    return std::count_if(relativeLanes.lanes.cbegin(), relativeLanes.lanes.cend(),
                         [&](const auto& lane)
                         {
                             return (lane.type == LaneType::Driving
                                     ||lane.type == LaneType::Exit
                                     || lane.type == LaneType::Entry
                                     || lane.type == LaneType::OnRamp
                                     || lane.type == LaneType::OffRamp)
                                    && ((side == Side::Left) ? (lane.relativeId > 0) : (lane.relativeId < 0))
                                    && lane.inDrivingDirection;
                         });
}


std::vector<SensorFusionObjectInfo> FmuCalculations::CalculateSensorFusionInfo(osi3::SensorData& sensorDataIn, WorldInterface* world, AgentInterface* agent)
{
    auto worldData = static_cast<OWL::WorldData*>(world->GetWorldData());

    std::vector<std::pair<WorldObjectInterface*, int>> objects;
    std::transform(sensorDataIn.moving_object().cbegin(),
                   sensorDataIn.moving_object().cend(),
                   std::back_inserter(objects),
                   [&](const osi3::DetectedMovingObject& object) {
                       unsigned long osi_id = object.header().ground_truth_id(0).value();
                       return std::make_pair<WorldObjectInterface*, int>(world->GetAgent(osi_id), object.header().sensor_id_size());
                   });
    std::transform(sensorDataIn.stationary_object().cbegin(),
                   sensorDataIn.stationary_object().cend(),
                   std::back_inserter(objects),
                   [&](const osi3::DetectedStationaryObject& object) {
                       unsigned long osi_id = object.header().ground_truth_id(0).value();
                       return std::make_pair<WorldObjectInterface*, int>(worldData->GetStationaryObject(osi_id).GetLink<WorldObjectInterface>(), object.header().sensor_id_size());
                   });

    std::vector<SensorFusionObjectInfo> sensorFusionInfo;

    std::transform(objects.cbegin(),
                   objects.cend(),
                   std::back_inserter(sensorFusionInfo),
                   [&](std::pair<const WorldObjectInterface*, int> entry)
                   {
                       const auto object = entry.first;
                       const auto numberOfSensors = entry.second;
                       auto obstruction = agent->GetEgoAgent().GetObstruction(object, {ObjectPointPredefined::FrontCenter, ObjectPointRelative::Leftmost, ObjectPointRelative::Rightmost});
                       auto [xDistance, yDistance] = CommonHelper::GetCartesianNetDistance(agent->GetBoundingBox2D(), object->GetBoundingBox2D());

                       return SensorFusionObjectInfo{
                               object->GetId(),
                               numberOfSensors,
                               obstruction.valid ? obstruction.lateralDistances.at(ObjectPointPredefined::FrontCenter) : NAN,
                               obstruction.valid ? obstruction.lateralDistances.at(ObjectPointRelative::Leftmost) : NAN,
                               obstruction.valid ? obstruction.lateralDistances.at(ObjectPointRelative::Rightmost) : NAN,
                               agent->GetEgoAgent().GetDistanceToObject(object, ObjectPointPredefined::Reference, ObjectPointPredefined::Reference).value_or(NAN),
                               agent->GetEgoAgent().GetNetDistance(object).value_or(NAN),
                               xDistance,
                               yDistance,
                               object->GetRoadPosition(ObjectPointPredefined::FrontCenter).cbegin()->second.laneId,
                               object->GetVelocity().Length(),
                               object->GetVelocity().x,
                               object->GetVelocity().y,
                               object->GetYaw()
                       };
                   });

    return sensorFusionInfo;
}