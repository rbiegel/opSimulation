/*******************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#pragma once
#include <QString>
#include <map>
#include <google/protobuf/util/json_util.h>
#include <optional>
#include <filesystem>

namespace FmuFileHelper
{

struct TraceEntry           ///< Parameters of trace entry
{
    std::string message;    ///< Message of the trace
    int time;               ///< Time of the trace entry
    std::string osiType;    ///< Type of OSI
};

std::string CreateAgentIDString (int agentId);
QString CreateOrOpenOutputFolder(const QString& outputDir, const QString& componentName, std::optional<const std::string> appendedFolder);

std::string GenerateTraceFileName(const std::string outputType, const std::pair<const std::string, FmuFileHelper::TraceEntry>& fileToOutputTrace);

void WriteBinaryTrace(const std::string &message, const QString &fileName, const QString& componentName, int time, std::string osiType,
                      std::map<std::string, FmuFileHelper::TraceEntry> &targetOutputTracesMap);
void WriteTracesToFile(const QString& outputDir, const std::map<std::string, FmuFileHelper::TraceEntry>& fileToOutputTracesMap);
void WriteJson(const google::protobuf::Message& message, const QString& fileName, const QString& outputDir);
std::filesystem::path temporaryDirectoryName();
QString CreateFmuJsonOutputDir(std::string fmuName, int agentId, const std::string runtimeOutputDir);
QString CreateFmuTraceOutputDir(std::string fmuName, int agentId, const std::string runtimeOutputDir);
} // namespace FmuFileHelper