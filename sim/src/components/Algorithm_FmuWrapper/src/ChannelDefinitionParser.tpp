

template <size_t FMI>
ChannelDefinitionParser<FMI>::ChannelDefinitionParser(const FmuVariables &fmuVariables, fmi_version_enu_t fmiVersion) :
    fmiVersion(fmiVersion),
    unmappedFmuVariables(fmuVariables)
{
    fmuOutputs.emplace<FMI>();
    fmuRealInputs.emplace<FMI>();
    fmuIntegerInputs.emplace<FMI>();
    fmuBooleanInputs.emplace<FMI>();
    fmuStringInputs.emplace<FMI>();
    fmuIntegerParameters.emplace<FMI>();
    fmuDoubleParameters.emplace<FMI>();
    fmuBoolParameters.emplace<FMI>();
    fmuStringParameters.emplace<FMI>();
}

template <size_t FMI>
bool ChannelDefinitionParser<FMI>::AddOutputChannel(const std::string& outputType, const std::string& variableName)
{
    auto unmappedVariableItem = std::get<FMI>(unmappedFmuVariables).find(variableName);
    auto fmuOutput = stringToSignalValueMap.find(outputType);

    if (unmappedVariableItem == std::get<FMI>(unmappedFmuVariables).end())
    {
        return false;
    }

    if (fmuOutput == stringToSignalValueMap.end())
    {
        return false;
    }

    const auto& [valueReference, variableTypeInFmu, casuality, variablility] = unmappedVariableItem->second;
    const auto& [fmuOutputType, variableTypeInWrapper] = fmuOutput->second;

    if (variableTypeInFmu != variableTypeInWrapper)
    {
        return false;
    }

    std::get<FMI>(fmuOutputs)[fmuOutputType] = valueReference;

    std::get<FMI>(unmappedFmuVariables).erase(unmappedVariableItem);

    return true;
}

template <size_t FMI>
bool ChannelDefinitionParser<FMI>::AddInputChannel(const std::string& inputType, const std::string& variableName)
{

    const auto pos = inputType.find('_');
    const auto variableType = inputType.substr(0, pos);
    std::variant<double, size_t> additionalParameter;
    if (variableType == "SpeedLimit" || variableType == "RoadCurvature")
    {
        const auto rangeString = inputType.substr(pos + 1);
        additionalParameter = std::stod(rangeString);
    }
    else if (variableType.substr(0,12)  == "SensorFusion")
    {
        const auto indexString = inputType.substr(pos + 1);
        additionalParameter = static_cast<size_t>(std::stoul(indexString));
    }

    auto unmappedVariableItem = std::get<FMI>(unmappedFmuVariables).find(variableName);
    auto fmuInput = stringToFmuTypeMap.find(variableType);

    if (unmappedVariableItem == std::get<FMI>(unmappedFmuVariables).end())
    {
        return false;
    }

    if (fmuInput == stringToFmuTypeMap.end())
    {
        return false;
    }

    const auto& [valueReference, variableTypeInFmu, casuality, variablility] = unmappedVariableItem->second;
    const auto& [fmuInputType, variableTypeInWrapper] = fmuInput->second;

    if (variableTypeInFmu != variableTypeInWrapper)
    {
        return false;
    }

    if (variableTypeInFmu == VariableType::Double)
    {
        std::get<FMI>(fmuRealInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::Int)
    {
        std::get<FMI>(fmuIntegerInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::Bool)
    {
        std::get<FMI>(fmuBooleanInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }
    else if (variableTypeInFmu == VariableType::String)
    {
        std::get<FMI>(fmuStringInputs).emplace_back(fmuInputType, additionalParameter, valueReference);
    }

    std::get<FMI>(unmappedFmuVariables).erase(unmappedVariableItem);

    return true;
}

template <size_t FMI>
bool ChannelDefinitionParser<FMI>::ParseOutputSignalTypes()
{
    std::vector<SignalType> nonExistingOutputSignals;
    for (const auto& [signalType, values] : valuesOfSignalType)
    {
        for (const auto signalValue : values)
        {
            auto valueFound = std::find_if(std::get<FMI>(fmuOutputs).cbegin(), std::get<FMI>(fmuOutputs).cend(),
                                           [&](const auto& pair) {return pair.first == signalValue;}) != std::get<FMI>(fmuOutputs).cend();
            if (valueFound)
            {
                outputSignals.emplace(signalType);
                if (std::find(nonExistingOutputSignals.cbegin(), nonExistingOutputSignals.cend(), signalType) != nonExistingOutputSignals.cend())
                {
                    return false;
                }
            }
            else
            {
                nonExistingOutputSignals.emplace_back(signalType);
                if (std::find(outputSignals.cbegin(), outputSignals.cend(), signalType) != outputSignals.cend())
                {
                    return false;
                }
            }
        }
    }
    return true;
}

template <size_t FMI>
FmuOutputs ChannelDefinitionParser<FMI>::GetFmuOutputs()
{
    return fmuOutputs;
}

template <size_t FMI>
std::set<SignalType> ChannelDefinitionParser<FMI>::GetOutputSignals()
{
    return outputSignals;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuRealInputs()
{
    return fmuRealInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuIntegerInputs()
{
    return fmuIntegerInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuBooleanInputs()
{
    return fmuBooleanInputs;
}

template <size_t FMI>
FmuInputs ChannelDefinitionParser<FMI>::GetFmuStringInputs()
{
    return fmuStringInputs;
}

template <size_t FMI>
FmuParameters<std::string> ChannelDefinitionParser<FMI>::GetFmuStringParameters()
{
    return fmuStringParameters;
}

template <size_t FMI>
FmuParameters<int> ChannelDefinitionParser<FMI>::GetFmuIntegerParameters()
{
    return fmuIntegerParameters;
}

template <size_t FMI>
FmuParameters<double> ChannelDefinitionParser<FMI>::GetFmuDoubleParameters()
{
    return fmuDoubleParameters;
}

template <size_t FMI>
FmuParameters<bool> ChannelDefinitionParser<FMI>::GetFmuBoolParameters()
{
    return fmuBoolParameters;
}
