/*******************************************************************************
* Copyright (c) 2021, 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#include "SignalTranslator.h"

#include <google/protobuf/message.h>
#include <memory>
#include <sstream>

#include "sim/src/components/Algorithm_FmuWrapper/src/FmuHelper.h"

#include "common/acquirePositionSignal.h"
#include "common/dynamicsSignal.h"
#include "common/sensorDataSignal.h"
#include "common/speedActionSignal.h"
#include "common/stringSignal.h"
#include "common/trajectorySignal.h"
#include "components/Algorithm_FmuWrapper/src/variant_visitor.h"
#include "include/signalInterface.h"
#include "osi3/osi_trafficcommand.pb.h"
#include "osi3/osi_trafficupdate.pb.h"
#include "osi3/osi_hostvehicledata.pb.h"

#ifdef USE_EXTENDED_OSI
#include "osi3/sl45_motioncommand.pb.h"
#include "components/Algorithm_FmuWrapper/src/FmuHelper.h"
#include "components/Algorithm_FmuWrapper/src/ChannelDefinitionParser.h"

#endif


void AddTrafficCommandActionFromOpenScenarioTrajectory(osi3::TrafficAction *trafficAction, const openScenario::Trajectory &trajectory)
{
    if (trajectory.timeReference.has_value())
    {
        auto trajectoryAction = trafficAction->mutable_follow_trajectory_action();
        for (const auto& trajectoryPoint : trajectory.points)
        {
            auto statePoint = trajectoryAction->add_trajectory_point();
            statePoint->mutable_timestamp()->set_seconds(static_cast<google::protobuf::int64>(trajectoryPoint.time));
            statePoint->mutable_timestamp()->set_nanos(static_cast<google::protobuf::uint32>(std::fmod(trajectoryPoint.time * 1e9, 1e9)));
            statePoint->mutable_position()->set_x(trajectoryPoint.x);
            statePoint->mutable_position()->set_y(trajectoryPoint.y);
            statePoint->mutable_orientation()->set_yaw(trajectoryPoint.yaw);
        }
    }
    else
    {
        auto followPathAction = trafficAction->mutable_follow_path_action();
        for (const auto& trajectoryPoint : trajectory.points)
        {
            auto statePoint = followPathAction->add_path_point();
            statePoint->mutable_position()->set_x(trajectoryPoint.x);
            statePoint->mutable_position()->set_y(trajectoryPoint.y);
            statePoint->mutable_orientation()->set_yaw(trajectoryPoint.yaw);
        }
    }
}

void AddTrafficCommandActionFromOpenScenarioPosition(osi3::TrafficAction *trafficAction,
                                                                const openScenario::Position &position,
                                                                WorldInterface *const worldInterface,
                                                                const std::function<void(const std::string &)> &errorCallback)
{
    auto acquireGlobalPositionAction = trafficAction->mutable_acquire_global_position_action();

    std::visit(variant_visitor{
                       [&acquireGlobalPositionAction](const openScenario::WorldPosition &worldPosition) {
                           acquireGlobalPositionAction->mutable_position()->set_x(worldPosition.x);
                           acquireGlobalPositionAction->mutable_position()->set_y(worldPosition.y);
                           if (worldPosition.z.has_value())
                               acquireGlobalPositionAction->mutable_position()->set_z(worldPosition.z.value());
                           if (worldPosition.r.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_roll(worldPosition.r.value());
                           if (worldPosition.p.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_pitch(worldPosition.p.value());
                           if (worldPosition.h.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_yaw(worldPosition.h.value());
                       },
                       [&worldInterface, &errorCallback, &acquireGlobalPositionAction](const openScenario::RelativeObjectPosition &relativeObjectPosition) {
                           const auto entityRef = relativeObjectPosition.entityRef;
                           const auto referencedAgentInterface = worldInterface->GetAgentByName(entityRef);
                           if (!referencedAgentInterface)
                               errorCallback("Reference to agent '" + entityRef + "' could not be resolved");

                           acquireGlobalPositionAction->mutable_position()->set_x(referencedAgentInterface->GetPositionX() + relativeObjectPosition.dx);
                           acquireGlobalPositionAction->mutable_position()->set_y(referencedAgentInterface->GetPositionY() + relativeObjectPosition.dy);
                           if (relativeObjectPosition.orientation.has_value())
                           {
                               const auto orientation = relativeObjectPosition.orientation.value();
                               if (orientation.r.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_roll(orientation.r.value());
                               if (orientation.p.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_pitch(orientation.p.value());
                               if (orientation.h.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_yaw(orientation.h.value());
                           }
                       },
                       [&errorCallback](auto &&other) {
                           errorCallback("Position variant not supported for 'openScenario::AcquirePositionAction'");
                       }},
               position);
}

/// @brief Translator function for Sensordata signal
struct SensorDataSignalTranslator : public InputSignalTranslator
{

    /// @brief Create a corresponding input signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    SensorDataSignalTranslator(WorldInterface &world,
                               AgentInterface &agent,
                               const CallbackInterface &callbackInterface) :
        InputSignalTranslator(world, agent, callbackInterface)
    {
    }

    /// @brief Translate input sensor data signal to protobuf message
    /// @param signalInterface Input sensor data signal
    /// @return  protobuf message
    const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface> signalInterface, [[maybe_unused]] const google::protobuf::Message *const) override
    {
        if (const auto signal = std::dynamic_pointer_cast<SensorDataSignal const>(signalInterface)) /*[[likely]]*/
            return &signal->sensorData;

        const auto errorMessage = FmuHelper::log_prefix(std::to_string(agent.GetId())) + "AlgorithmFmuHandler invalid signaltype";
        callbackInterface.LOGERROR(errorMessage);
        throw std::runtime_error(errorMessage);

    }
};

/// @brief Translator function for Trajectory signal
struct TrajectorySignalTranslator : public InputSignalTranslator
{

    /// @brief Create a corresponding input signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    TrajectorySignalTranslator(WorldInterface &world,
                               AgentInterface &agent,
                               const CallbackInterface &callbackInterface) :
        InputSignalTranslator(world, agent, callbackInterface)
    {
    }

    const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface> signalInterface, const google::protobuf::Message *const message) override
    {
        const auto signal = std::dynamic_pointer_cast<TrajectorySignal const>(signalInterface);
        const auto trafficCommand = dynamic_cast<const osi3::TrafficCommand *const>(message);

        if (!trafficCommand || !signal || signal->componentState != ComponentState::Acting ) /*[[unlikely]]*/
            return trafficCommand;

        const auto newTrafficCommand = new osi3::TrafficCommand{};
        newTrafficCommand->CopyFrom(*trafficCommand);
        AddTrafficCommandActionFromOpenScenarioTrajectory(newTrafficCommand->add_action(), signal->trajectory);

        return newTrafficCommand;
    }
};

/// @brief Translator function for acquireposition signal
struct AcquirePositionSignalTranslator : public InputSignalTranslator
{

    /// @brief Create a corresponding input signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    AcquirePositionSignalTranslator(WorldInterface &world,
                                    AgentInterface &agent,
                                    const CallbackInterface &callbackInterface) :
        InputSignalTranslator(world, agent, callbackInterface)
    {
    }

    const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface> signalInterface, const google::protobuf::Message *const message) override
    {
        const auto signal = std::dynamic_pointer_cast<AcquirePositionSignal const>(signalInterface);
        const auto trafficCommand = dynamic_cast<const osi3::TrafficCommand *const>(message);
        if (!trafficCommand || !signal || signal->componentState != ComponentState::Acting) /*[[unlikely]]*/
            return trafficCommand;

        const auto newTrafficCommand = new osi3::TrafficCommand{};
        newTrafficCommand->CopyFrom(*trafficCommand);
        std::stringstream agentIdStringStream;
        agentIdStringStream << agent.GetId();
        const std::string agentIdString = agentIdStringStream.str();
        AddTrafficCommandActionFromOpenScenarioPosition(newTrafficCommand->add_action(),
                                                                   signal->position,
                                                                   &world,
                                                                   [this, &agentIdString](const std::string &message) { callbackInterface.LOGERROR(FmuHelper::log_prefix(agentIdString) + message);});
        return newTrafficCommand;
    }
};

/// @brief Translator function for string signal signal
struct StringSignalTranslator : public InputSignalTranslator
{

    /// @brief Create a corresponding input signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    StringSignalTranslator(WorldInterface &world,
                           AgentInterface &agent,
                           const CallbackInterface &callbackInterface) :
        InputSignalTranslator(world, agent, callbackInterface)
    {
    }

    const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface> signalInterface, const google::protobuf::Message *const message) override
    {
        const auto signal = std::dynamic_pointer_cast<StringSignal const>(signalInterface);
        const auto trafficCommand = dynamic_cast<const osi3::TrafficCommand *const>(message);
        if (!trafficCommand || !signal || signal->componentState != ComponentState::Acting) /*[[unlikely]]*/
            return trafficCommand;

        const auto newTrafficCommand = new osi3::TrafficCommand{};
        newTrafficCommand->CopyFrom(*trafficCommand);
        newTrafficCommand->add_action()->mutable_custom_action()->set_command(signal->payload);
        return newTrafficCommand;
    }
};

/// @brief Translator function for Speedaction signal
struct SpeedActionSignalTranslator : public InputSignalTranslator
{

    /// @brief Create a corresponding input signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    SpeedActionSignalTranslator(WorldInterface &world,
                                AgentInterface &agent,
                                const CallbackInterface &callbackInterface) :
        InputSignalTranslator(world, agent, callbackInterface)
    {
    }

    const google::protobuf::Message * translate(std::shared_ptr<const SignalInterface> signalInterface, const google::protobuf::Message *const message) override
    {
        const auto signal = std::dynamic_pointer_cast<SpeedActionSignal const>(signalInterface);
        const auto trafficCommand = dynamic_cast<const osi3::TrafficCommand *const>(message);
        if (!trafficCommand || !signal || signal->componentState != ComponentState::Acting) /*[[unlikely]]*/
            return trafficCommand;

        const auto newTrafficCommand = new osi3::TrafficCommand{};
        newTrafficCommand->CopyFrom(*trafficCommand);
        newTrafficCommand->add_action()->mutable_speed_action()->set_absolute_target_speed(signal->targetSpeed);
        return newTrafficCommand;
    }
};

InputSignalTranslator::InputSignalTranslator(WorldInterface &world,
                                             AgentInterface &agent,
                                             const CallbackInterface &callbackInterface):
       world(world),
       agent(agent),
       callbackInterface(callbackInterface)
{
}

std::optional<std::shared_ptr<InputSignalTranslator>> InputSignalTranslatorFactory::build(int localLinkId,
                                                                                          WorldInterface& world,
                                                                                          AgentInterface& agent,
                                                                                          const CallbackInterface& callbackInterface)
{
    switch (localLinkId)
    {
        case 2:
            return std::make_shared<SensorDataSignalTranslator>(world, agent, callbackInterface);
#ifdef USE_EXTENDED_OSI
        case 10:
            return std::make_shared<TrajectorySignalTranslator>(world, agent, callbackInterface);
        case 11:
            return std::make_shared<AcquirePositionSignalTranslator>(world, agent, callbackInterface);
        case 12:
            return std::make_shared<StringSignalTranslator>(world, agent, callbackInterface);
        case 13:
            return std::make_shared<SpeedActionSignalTranslator>(world, agent, callbackInterface);
#endif
        default:
            return std::nullopt;
    }
}

/// @brief Translator function for DynamicsOutput signal
struct DynamicsOutputSignalTranslator : public OutputSignalTranslator
{

    /// @brief Create a corresponding output signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    DynamicsOutputSignalTranslator(WorldInterface &world,
                                   AgentInterface &agent,
                                   const CallbackInterface &callbackInterface) :
        OutputSignalTranslator(world, agent, callbackInterface)
    {
    }

    std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const message) override
    {
        double acceleration{};
        double velocityX{};
        double velocityY{};
        double positionX{};
        double positionY{};
        double yaw{};
        double yawRate{};
        double yawAcceleration{};
        double roll{};
        double steeringWheelAngle{};
        double centripetalAcceleration{};
        double travelDistance{};
        const auto bb_center_offset_x = agent.GetVehicleModelParameters().boundingBoxCenter.x;
        const auto previousPositionX = agent.GetPositionX();
        const auto previousPositionY = agent.GetPositionY();
        const auto agentIdString = std::to_string(agent.GetId());

#ifdef USE_EXTENDED_OSI
        if (auto motionCommand = dynamic_cast<const setlevel4to5::MotionCommand *const>(message))
        {
            setlevel4to5::DynamicState dynamicState;
            if (motionCommand->trajectory().trajectory_point_size() > 0) /**/
            {
                dynamicState = motionCommand->trajectory().trajectory_point(0);
            }
            else
            {
                dynamicState = motionCommand->current_state();
            }

            acceleration = dynamicState.acceleration();
            yaw = dynamicState.heading_angle();
            positionX = dynamicState.position_x() - bb_center_offset_x * std::cos(yaw);
            positionY = dynamicState.position_y() - bb_center_offset_x * std::sin(yaw);
            velocityX = dynamicState.velocity() * std::cos(yaw);
            velocityY = dynamicState.velocity() * std::sin(yaw);
        }
#endif
        if (auto trafficUpdate = dynamic_cast<const osi3::TrafficUpdate *const>(message))
        {
            if (trafficUpdate->update_size() > 0)
            {
                const auto &baseMoving = trafficUpdate->update(0).base();
                velocityX = baseMoving.velocity().x();
                velocityY = baseMoving.velocity().y();
                yaw = baseMoving.orientation().yaw();
                roll = baseMoving.orientation().roll();
                acceleration = baseMoving.acceleration().x() * std::cos(yaw) + baseMoving.acceleration().y() * std::sin(yaw);
                centripetalAcceleration = -baseMoving.acceleration().x() * std::sin(yaw) + baseMoving.acceleration().y() * std::cos(yaw);
                positionX = baseMoving.position().x() - bb_center_offset_x * std::cos(yaw);
                positionY = baseMoving.position().y() - bb_center_offset_x * std::sin(yaw);
                yawRate = baseMoving.orientation_rate().yaw();
                yawAcceleration = baseMoving.orientation_acceleration().yaw();

            }
            else
            {
                callbackInterface.LOGWARN(FmuHelper::log_prefix(agentIdString) + "Received empty TrafficUpdate message");
            }
        }
        else
        {
            const auto errorMessage = FmuHelper::log_prefix(agentIdString) + "Cannot provide DynamicsSignal, as neither TrafficUpdate nor MotionCommand are connected";
            // data->callbackInterface->LOGERROR(errorMessage); ToDo: After Ticket 234 this must line must be activated and go through the tests
            //throw std::runtime_error(errorMessage);   //Todo : Discuss
            return nullptr;
        }

        double deltaX = positionX - previousPositionX;
        double deltaY = positionY - previousPositionY;
        travelDistance = std::sqrt(deltaX * deltaX + deltaY * deltaY);

        //TODO is this code neeede?
        /*
        constexpr double EPSILON = 0.001;

        if (std::abs(positionX) < EPSILON && std::abs(positionY) < EPSILON)
        {
            positionX = previousPositionX;
            positionY = previousPositionY;
        }

        if(travelDistance == 0.0)
        {
            double deltaX = positionX - previousPositionX;
            double deltaY = positionY - previousPositionY;
            travelDistance = std::sqrt(deltaX * deltaX + deltaY * deltaY);
        }
         */

        //previousPosition = {positionX, positionY};

        return std::make_shared<DynamicsSignal const>(ComponentState::Acting,
                                                      acceleration,
                                                      velocityX,
                                                      velocityY,
                                                      positionX,
                                                      positionY,
                                                      yaw,
                                                      yawRate,
                                                      yawAcceleration,
                                                      roll,
                                                      steeringWheelAngle,
                                                      centripetalAcceleration,
                                                      travelDistance);
    }
};

/// @brief Translator function for acceleartion signal
struct AccelerationSignalTranslator : public OutputSignalTranslator
{

    /// @brief Create a corresponding output signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    AccelerationSignalTranslator(WorldInterface &world,
                                 AgentInterface &agent,
                                 const CallbackInterface &callbackInterface) :
            OutputSignalTranslator(world, agent, callbackInterface)
    {
    }

    std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const message) override
    {
        //TODO find a way to add locallink 1-3 in signal translator
        /*
        if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::AccelerationSignal) != outputSignals.cend())
        {
            auto acceleration = GetFmuOutputValue(SignalValue::AccelerationSignal_Acceleration, VariableType::Double).realValue;
            return std::make_shared<AccelerationSignal const>(componentState, acceleration);
        }
        else
        {
            return std::make_shared<AccelerationSignal const>(ComponentState::Disabled, 0.0);
        }
         */
        return nullptr;
    }
};

/// @brief Translator function for Longitudinal signal
struct LongitudinalSignalTranslator : public OutputSignalTranslator
{

    /// @brief Create a corresponding output signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    LongitudinalSignalTranslator(WorldInterface &world,
                                AgentInterface &agent,
                                const CallbackInterface &callbackInterface) :
            OutputSignalTranslator(world, agent, callbackInterface)
    {
    }

    std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const message) override
    {
        //TODO find a way to add locallink 1-3 in signal translator
        /*
        if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::LongitudinalSignal) != outputSignals.cend())
        {
            auto accPedalPos = GetFmuOutputValue(SignalValue::LongitudinalSignal_AccPedalPos, VariableType::Double).realValue;
            auto brakePedalPos = GetFmuOutputValue(SignalValue::LongitudinalSignal_BrakePedalPos, VariableType::Double).realValue;
            auto gear = GetFmuOutputValue(SignalValue::LongitudinalSignal_Gear, VariableType::Int).intValue;
            return std::make_shared<LongitudinalSignal const>(componentState, accPedalPos, brakePedalPos, gear);
        }
        else
        {
            return std::make_shared<LongitudinalSignal const>(ComponentState::Disabled, 0.0, 0.0, 0.0);
        }
         */
        return nullptr;
    }
};

/// @brief Translator function for Steering signal
struct SteeringSignalTranslator : public OutputSignalTranslator
{

    /// @brief Create a corresponding output signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    SteeringSignalTranslator(WorldInterface &world,
                             AgentInterface &agent,
                             const CallbackInterface &callbackInterface) :
            OutputSignalTranslator(world, agent, callbackInterface)
    {
    }

    std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const message) override
    {
        //TODO find a way to add locallink 1-3 in signal translator
        /*
        if (std::find(outputSignals.cbegin(), outputSignals.cend(), SignalType::SteeringSignal) != outputSignals.cend())
        {
            auto steeringWheelAngle = GetFmuOutputValue(SignalValue::SteeringSignal_SteeringWheelAngle, VariableType::Double).realValue;
            return std::make_shared<SteeringSignal const>(componentState, steeringWheelAngle);
        }
        else
        {
            return std::make_shared<SteeringSignal const>(ComponentState::Disabled, 0.0);
        }
         */
        return nullptr;
    }
};

/// @brief Translator function for SensorDataOutput signal
struct SensorDataOutputSignalTranslator : public OutputSignalTranslator
{
    /// @brief Create a sensor data output signal translator object
    /// @param world                Reference to the world interface
    /// @param agent                Reference to the agent interface
    /// @param callbackInterface    Reference to the call back interface
    SensorDataOutputSignalTranslator(WorldInterface &world,
                                     AgentInterface &agent,
                                     const CallbackInterface &callbackInterface) :
            OutputSignalTranslator(world, agent, callbackInterface)
    {
    }

    std::shared_ptr<const SignalInterface> translate(const google::protobuf::Message *const message) override
    {
        return std::make_shared<SensorDataSignal const>(*dynamic_cast<const osi3::SensorData *const>(message));
    }
};

OutputSignalTranslator::OutputSignalTranslator(WorldInterface &world,
                                               AgentInterface &agent,
                                               const CallbackInterface &callbackInterface):
        world(world),
        agent(agent),
        callbackInterface(callbackInterface)
{
}

std::optional<std::shared_ptr<OutputSignalTranslator>> OutputSignalTranslatorFactory::build(int localLinkId,
                                                                                            WorldInterface& world,
                                                                                            AgentInterface& agent,
                                                                                            const CallbackInterface& callbackInterface)
{
    switch (localLinkId)
    {
        case 0:
            return std::make_shared<DynamicsOutputSignalTranslator>(world, agent, callbackInterface);
        case 1:
            return std::make_shared<AccelerationSignalTranslator>(world, agent, callbackInterface);
        case 2:
            return std::make_shared<LongitudinalSignalTranslator>(world, agent, callbackInterface);
        case 3:
            return std::make_shared<SteeringSignalTranslator>(world, agent, callbackInterface);
        case 6:
            return std::make_shared<SensorDataOutputSignalTranslator>(world, agent, callbackInterface);
        default:
            return std::nullopt;
    }
}
