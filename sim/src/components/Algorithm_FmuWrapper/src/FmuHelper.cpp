/********************************************************************************
* Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*               2019-2021 in-tech GmbH
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* SPDX-License-Identifier: EPL-2.0
********************************************************************************/


#include "FmuHelper.h"

std::string FmuHelper::log_prefix(const std::string &agentIdString)
{
    return "Agent " + agentIdString + ": ";
}

std::string FmuHelper::log_prefix(const std::string &agentIdString, const std::string &componentName)
{
    return "Agent " + agentIdString + ": Component " + componentName + ": ";
}

void FmuHelper::AppendMessages(std::string& appendedMessage, std::string& message)
{
    auto length = intToBytes(message.length());
    std::string messageLength{length.begin(), length.end()};
    appendedMessage = appendedMessage + messageLength + message;
}

std::vector<unsigned char> FmuHelper::intToBytes(int paramInt)
{
    std::vector<unsigned char> arrayOfByte(4);
    for (int i = 0; i < 4; i++)
        arrayOfByte[3 - i] = (paramInt >> (i * 8));
    return arrayOfByte;
}

osi3::SensorViewConfiguration FmuHelper::GenerateDefaultSensorViewConfiguration()
{
    osi3::SensorViewConfiguration viewConfiguration;

    viewConfiguration.mutable_sensor_id()->set_value(0);

    viewConfiguration.mutable_mounting_position()->mutable_orientation()->set_pitch(0.0);
    viewConfiguration.mutable_mounting_position()->mutable_orientation()->set_roll(0.0);
    viewConfiguration.mutable_mounting_position()->mutable_orientation()->set_yaw(0.0);

    viewConfiguration.mutable_mounting_position()->mutable_position()->set_x(0.0);
    viewConfiguration.mutable_mounting_position()->mutable_position()->set_y(0.0);
    viewConfiguration.mutable_mounting_position()->mutable_position()->set_z(0.0);

    viewConfiguration.set_field_of_view_horizontal(M_2_PI);
    viewConfiguration.set_range(std::numeric_limits<double>::max());

    return viewConfiguration;
}


std::string FmuHelper::GenerateString(std::string operation, std::string name, VariableType datatype, FmuValue value)
{
    std::stringstream retString;
    retString << operation + " " + VariableTypeToStringMap(datatype) + " value '" + name + "': ";
    switch (datatype)
    {
        case VariableType::String:
            retString << value.stringValue;
            break;
        case VariableType::Double:
            retString << value.realValue;
            break;
        case VariableType::Int:
            retString << value.intValue;
            break;
        case VariableType::Bool:
            retString << value.boolValue;
            break;
        case VariableType::Enum:
            retString << value.intValue;
            break;

    }
    return retString.str();
}


void FmuHelper::AddTrafficCommandActionFromOpenScenarioTrajectory(osi3::TrafficAction *trafficAction, const openScenario::Trajectory& trajectory)
{
    if (trajectory.timeReference.has_value())
    {
        auto trajectoryAction = trafficAction->mutable_follow_trajectory_action();
        for (const auto& trajectoryPoint : trajectory.points)
        {
            auto statePoint = trajectoryAction->add_trajectory_point();
            statePoint->mutable_timestamp()->set_seconds(static_cast<google::protobuf::int64>(trajectoryPoint.time));
            statePoint->mutable_timestamp()->set_nanos(static_cast<google::protobuf::uint32>(std::fmod(trajectoryPoint.time * 1e9, 1e9)));
            statePoint->mutable_position()->set_x(trajectoryPoint.x);
            statePoint->mutable_position()->set_y(trajectoryPoint.y);
            statePoint->mutable_orientation()->set_yaw(trajectoryPoint.yaw);
        }
    } else {
        auto followPathAction = trafficAction->mutable_follow_path_action();
        for (const auto& trajectoryPoint : trajectory.points)
        {
            auto statePoint = followPathAction->add_path_point();
            statePoint->mutable_position()->set_x(trajectoryPoint.x);
            statePoint->mutable_position()->set_y(trajectoryPoint.y);
            statePoint->mutable_orientation()->set_yaw(trajectoryPoint.yaw);
        }
    }
}



void FmuHelper::AddTrafficCommandActionFromOpenScenarioPosition(osi3::TrafficAction *trafficAction,
                                                                      const openScenario::Position &position,
                                                                      WorldInterface *const worldInterface,
                                                                      const std::function<void(const std::string &)> &errorCallback)
{
    auto acquireGlobalPositionAction = trafficAction->mutable_acquire_global_position_action();

    std::visit(variant_visitor{
                       [&acquireGlobalPositionAction](const openScenario::WorldPosition &worldPosition) {
                           acquireGlobalPositionAction->mutable_position()->set_x(worldPosition.x);
                           acquireGlobalPositionAction->mutable_position()->set_y(worldPosition.y);
                           if (worldPosition.z.has_value())
                               acquireGlobalPositionAction->mutable_position()->set_z(worldPosition.z.value());
                           if (worldPosition.r.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_roll(worldPosition.r.value());
                           if (worldPosition.p.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_pitch(worldPosition.p.value());
                           if (worldPosition.h.has_value())
                               acquireGlobalPositionAction->mutable_orientation()->set_yaw(worldPosition.h.value());
                       },
                       [&worldInterface, &errorCallback, &acquireGlobalPositionAction](const openScenario::RelativeObjectPosition &relativeObjectPosition) {
                           const auto entityRef = relativeObjectPosition.entityRef;
                           const auto referencedAgentInterface = worldInterface->GetAgentByName(entityRef);
                           if (!referencedAgentInterface)
                               errorCallback("Reference to agent '" + entityRef + "' could not be resolved");

                           acquireGlobalPositionAction->mutable_position()->set_x(referencedAgentInterface->GetPositionX() + relativeObjectPosition.dx);
                           acquireGlobalPositionAction->mutable_position()->set_y(referencedAgentInterface->GetPositionY() + relativeObjectPosition.dy);
                           if (relativeObjectPosition.orientation.has_value())
                           {
                               const auto orientation = relativeObjectPosition.orientation.value();
                               if (orientation.r.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_roll(orientation.r.value());
                               if (orientation.p.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_pitch(orientation.p.value());
                               if (orientation.h.has_value())
                                   acquireGlobalPositionAction->mutable_orientation()->set_yaw(orientation.h.value());
                           }
                       },
                       [&errorCallback](auto &&other) {
                           errorCallback("Position variant not supported for 'openScenario::AcquirePositionAction'");
                       }},
               position);
}
