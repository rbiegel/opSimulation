/********************************************************************************
 * Copyright (c) 2017 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <QtGlobal>
#include <vector>
#include "dynamics_twotrack_tire.h"
#define NUMBER_OF_WHEELS 4

class Tire;

//! Simple STATIC two-track vehicle model
class VehicleSimpleTT
{
public:
    VehicleSimpleTT();
    ~VehicleSimpleTT();

    /**
     *    \name Initialize parameters
     *    @{
    */

    /**
     * @brief Initialize tire characteristics
     * 
     * @param weight        weight of the vehicle
     * @param P_engine      Power of the engine
     * @param T_brakeLimit  Brake force limit
     */
    void InitSetEngine(double weight,
                       double P_engine, double T_brakeLimit);
    /**
     * @brief Initialize car's physics
     * 
     * @param x_wheelbase Wheel base of the vehicle
     * @param x_COG       x coordinate of center of gravity
     * @param y_track     Track width
     * @param y_COG       y coordinate of center of gravity
     */
    void InitSetGeometry(double x_wheelbase, double x_COG, double y_track, double y_COG);
    
    /**
     * @brief Initialize car's velocity
     * 
     * @param vel               Initial velocity
     * @param mu_tire_max       Peak tire force
     * @param mu_tire_slide     Force at full slide
     * @param s_max             Slip corresponding to peak tire force
     * @param r_tire            Radius of the tire
     * @param frictionScaleRoll Road/tire friction coefficient
     */
    void InitSetTire(double vel,
                     double mu_tire_max, double mu_tire_slide,
                     double s_max, double r_tire, double frictionScaleRoll);
    /**
     *    @}
    */

    /**
     *    \name Update state
     *    @{
    */
    //! Refresh car's position
    void UpdatePosition(double);
    /**
     * @brief Refresh car's velocity
     * 
     * @param velocityCars  Vehicle's longitudinal and lateral velocity in vehicle's CS
     * @param w             Vehicle's rotational velociy
     */
    void SetVelocity(Common::Vector2d, const double);
    /**
     *    @}
    */

    /**
     *    \name Make step
     *    @{
    */
    /**
     * @brief Calculate local tire torques
     * 
     * @param throttlePedal     Throttle pedal position in the range [0...1]
     * @param brakePedal        Brake pedal position in the range [0...1]
     * @param brakeSuperpose    Brake position for each tire
     */
    void DriveTrain(double throttlePedal, double brakePedal, std::vector<double> brakeSuperpose);
    
    /**
     * @brief Local forces and moments transferred onto road
     * 
     * @param timeStep          Time step as double in s
     * @param angleTireFront    Steering angle
     * @param forceVertical     Vertical force on wheels
     */
    void ForceLocal(double timeStep, double, std::vector<double> forceVertical);
    
    /**
     * @brief Global force and moment
     * 
     */
    void ForceGlobal();

    /**
     * @brief Get the Tire Force object
     * 
     * @param tireNumber    Number of wheels
     * @return double 
     */
    double GetTireForce(int tireNumber);

    /**
     * @brief Get the Force Tire Vertical Static object
     * 
     * @param tireNumber    Number of wheels
     * @return double 
     */
    double GetForceTireVerticalStatic(int tireNumber);
    /**
     *    @}
    */

    /**
     *    \name Output
     *    @{
    */
    //! Total force on vehicle's CoM
    Common::Vector2d forceTotalXY;
    //! Total momentum on the vehicle around the z-axes
    double momentTotalZ;
    /**
     *    @}
    */

    /**
     *    \name Parameters
     *    @{
    */
   //! Vertical tire force at four wheels
    double forceTireVerticalStatic[NUMBER_OF_WHEELS];
    /**
     *    @}
    */

private:

    /** \name Parameters
     *    @{
    */
    //! Inertial moment of tires [kg*m^2]
    double inertiaTireX[NUMBER_OF_WHEELS];

    //! Maximal engine power [W]
    double powerEngineLimit;
    //! Brake force limit [N]
    double torqueBrakeLimit;

    //! Mass of the car [kg]
    double massTotal;
    //! Tire positions in car CS [m]
    Common::Vector2d positionTire[NUMBER_OF_WHEELS];
    /**
     *  @}
    */


    /** \name Constants
     *    @{
    */
    //! Drag coefficient (Asbo from http://rc.opelgt.org/indexcw.php) []
    const double coeffDrag = 0.34;
    //! Face area (Asbo from http://rc.opelgt.org/indexcw.php) [m^2]
    const double areaFace = 1.94;
    //! Air density [kg/m^3]
    const double densityAir = 1.29;
    //! Earth's gravitation acceleration
    const double accelVerticalEarth = -9.81;
    //! Toe-in/-out
    const double anglePreSet = 0.0;//0.003;
    //! Brake balance
    const double brakeBalance = 0.67;
    //! Max. engine moment
    const double torqueEngineLimit = 10000.0;
    /**
     *  @}
    */

    // Dynamics to remember
    double rotationVelocityTireX[NUMBER_OF_WHEELS];
    double rotationVelocityGradTireX[NUMBER_OF_WHEELS];
    double yawVelocity;
    Common::Vector2d velocityCar;
    Common::Vector2d forceTire[NUMBER_OF_WHEELS];
    Common::Vector2d slipTire[NUMBER_OF_WHEELS];
    double torqueTireXthrottle[NUMBER_OF_WHEELS];
    double torqueTireXbrake[NUMBER_OF_WHEELS];
    double momentTireZ[NUMBER_OF_WHEELS];

    /** \name Container
     *    @{
    */
    std::vector<Tire *> tires;
    /**
     *  @}
    */

};
