/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef SWITCH_H
#define SWITCH_H

#include "controlData.h"

/// @brief Class representing a switch
class Switch        
{
public:
    Switch();

    /// @brief TODO
    /// @param index    index of the switch
    /// @param driver   Driver signal containing throttle, steer, brakePedal and 4x brakeSuperpose signals
    /// @param prio1    prio1 containing throttle, steer, brakePedal and 4x brakeSuperpose signals  
    /// @param prio2    prio2 containing throttle, steer, brakePedal and 4x brakeSuperpose signals
    /// @param prio3    prio3 containing throttle, steer, brakePedal and 4x brakeSuperpose signals
    /// @return Signal class containing throttle, steer, brakePedal and 4x brakeSuperpose signals
    ControlData Perform(int index, ControlData driver, ControlData prio1, ControlData prio2, ControlData prio3);
private:
    ControlData resultingControl;
    int collisionState;
    double collisionSteering;
};

#endif // SWITCH_H
