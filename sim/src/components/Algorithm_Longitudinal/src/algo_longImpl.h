/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//! @file algo_longImpl.h


/** \addtogroup Algorithm_Longitudinal
* @{

* \brief models the longitudinal controller of the driver.
*
* This component models the longitudinal controller of the driver / some ADAS.
* It calculates the positions of the acceleration pedal and the brake pedal,
* as well as the chosen gear and models the actuation of the pedals.
*
* \section Algorithm_Longitudinal_Inputs Inputs
* Input variables:
* name | meaning
* -----|------
* accelerationWish | wish acceleration
* velocity | current velocity
*
* Input channel IDs:
* Input ID | signal class | contained variables
* ------------|--------------|-------------
* 0 | AccelerationSignal | accelerationWish
* 101 | SensorDriverSignal | used from signal: velocity
*
* \section Algorithm_Longitudinal_InitInputs Init Inputs
* Init input variables:
* name | meaning
* -----|------
* vehicleModelParameters | VehicleModelParameters
*
* Init input channel IDs:
* Input Id | signal class | contained variables
* ------------|--------------|-------------
* 100 | ParametersVehicleSignal | vehicleModelParameters
*
* \section Algorithm_Lateral_Output Outputs
* Output variables:
* name | meaning
* -----|------
* out_desiredSteeringWheelAngle | The steering wheel angle wish of the driver in radian.
*
* Output channel IDs:
* Output Id | signal class | contained variables
* ------------|--------------|-------------
* 0 | SteeringSignal | out_desiredSteeringWheelAngle
*
* \section Algorithm_Longitudinal_ExternalParameters External parameters
* none
*
* \section Algorithm_Longitudinal_InternalParameters Internal paramters
* name | value | meaning
* -----|-------|------.
*
*   @} */

#pragma once

#include "include/modelInterface.h"
#include "common/primitiveSignals.h"
#include "longCalcs.h"
#include "components/Sensor_Driver/src/Signals/sensorDriverSignal.h"

/** \addtogroup Algorithm_Longitudinal
 * @{
 * \brief models the longitudinal controller of the driver.
 *
 * This component models the longitudinal controller of the driver / some ADAS.
 * It calculates the positions of the acceleration pedal and the brake pedal,
 * as well as the chosen gear and models the actuation of the pedals.
 *
 *   @} */

/// Class to define longitudinal controller of the driver
class AlgorithmLongitudinalImplementation : public AlgorithmInterface
{
public:

     //! Name of the current component
    const std::string COMPONENTNAME = "AlgorithmLongitudinal";

    /**
     * @brief Construct a new Algorithm Longitudinal Implementation object
     * 
     * \param [in] componentName   Name of the component
     * \param [in] isInit          Query whether the component was just initialized
     * \param [in] priority        Priority of the component
     * \param [in] offsetTime      Offset time of the component
     * \param [in] responseTime    Response time of the component
     * \param [in] cycleTime       Cycle time of this components trigger task [ms]
     * \param [in] stochastics     Provides access to the stochastics functionality of the framework
     * \param [in] parameters      Interface provides access to the configuration parameters
     * \param [in] publisher       Instance  provided by the framework
     * \param [in] callbacks       Interface for callbacks to framework
     * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
     */
    AlgorithmLongitudinalImplementation(
        std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        const ParameterInterface *parameters,
        PublisherInterface * const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent) :
        AlgorithmInterface(
            componentName,
            isInit,
            priority,
            offsetTime,
            responseTime,
            cycleTime,
            stochastics,
            parameters,
            publisher,
            callbacks,
            agent)
    {
    }
    AlgorithmLongitudinalImplementation(const AlgorithmLongitudinalImplementation&) = delete;
    AlgorithmLongitudinalImplementation(AlgorithmLongitudinalImplementation&&) = delete;
    AlgorithmLongitudinalImplementation& operator=(const AlgorithmLongitudinalImplementation&) = delete;
    AlgorithmLongitudinalImplementation& operator=(AlgorithmLongitudinalImplementation&&) = delete;

    virtual ~AlgorithmLongitudinalImplementation() = default;

    /*!
     * \brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update taks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * \param[in]     data           Referenced signal (copied by sending component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

    /*!
     * \brief Update outputs.
     *
     * Function is called by framework when this Component.has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * \param[out]    data           Referenced signal (copied by this component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    void Trigger(int time) override;

private:

    //! \brief Calculate the pedal positions and gear
    void CalculatePedalPositionAndGear();

    // --- Inputs

    ComponentState componentState {ComponentState::Armed};

    //! initialize module with sended prioritzer data
    bool initializedAccelerationInput{false};

    //! initialize module with sended vehicle parameters sended from ParametersAgent
    bool initializedVehicleModelParameters{false};

    //! initialize module with sended parameters sended from SensoDriver
    bool initializedSensorDriverData{false};

    //! The wish acceleration of the agent in m/s^2.
    double accelerationWish{0.0};

    //! current agent velocity
    double currentVelocity{0.0};

    //  --- Outputs

    //! Position of the accelerator pedal position in percent.
    double out_accPedalPos{0.0};
    //! Position of the brake pedal position in percent.
    double out_brakePedalPos{0.0};
    //! Currently choosen gear.
    int out_gear{0};

    //  --- Init Inputs

    //! contains: double carMass; double rDyn and more;
    VehicleModelParameters vehicleModelParameters;

}; /// endo of algorithm_longitudinal implementation