/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
/**********************************************
***********************************************/

/// @file  AlgorithmAFDM.h
//! @brief This file contains the implementation header file

#pragma once

#include <QtGlobal>

/// TODO
#if defined(ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_LIBRARY)
#  define ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT Q_DECL_EXPORT   //!< Export of the dll-functions
#else
#  define ALGORITHM_AGENTFOLLOWINGDRIVERMODEL_SHARED_EXPORT Q_DECL_IMPORT   //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"
