/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  signalPrioritizerImpl.h
*	@brief This file provides the exported methods.
*
*   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#pragma once

#pragma once
#include <QtGlobal>

/// TODO
#if defined(SIGNAL_PRIORITIZER_LIBRARY)
#  define SIGNAL_PRIORITIZER_SHARED_EXPORT Q_DECL_EXPORT    //!< Export of the dll-functions
#else
#  define SIGNAL_PRIORITIZER_SHARED_EXPORT Q_DECL_IMPORT    //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"

