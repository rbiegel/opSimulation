/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  sensor_collision_global.h
//! @brief This file contains DLL export declarations
//-----------------------------------------------------------------------------

#ifndef SENSOR_COLLISION_GLOBAL_H
#define SENSOR_COLLISION_GLOBAL_H

#include <QtGlobal>

/// TODO
#if defined(SENSOR_COLLISION_LIBRARY)
#  define SENSOR_COLLISION_SHARED_EXPORT Q_DECL_EXPORT  //!< Export of the dll-functions
#else
#  define SENSOR_COLLISION_SHARED_EXPORT Q_DECL_IMPORT  //!< Import of the dll-functions
#endif

#endif // SENSOR_COLLISION_GLOBAL_H
