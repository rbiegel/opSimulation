/********************************************************************************
 * Copyright (c) 2020-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * https://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdToSspNetworkParser.h"
#include "src/components/Algorithm_FmuWrapper/src/fmuFileHelper.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/SystemStructureDescription.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/SsdNameConnector.h"

#include "components/Algorithm_FmuWrapper/src/variant_visitor.h"
#include <filesystem>
#include <optional>

#include "OpenPassInterfaceData.h"

SsdToSspNetworkParser::SsdToSspNetworkParser() :
    componentName(&(OpenPassInterfaceData::get().lock()->componentName)),
    isInit(OpenPassInterfaceData::get().lock()->isInit),
    priority(OpenPassInterfaceData::get().lock()->priority),
    offsetTime(OpenPassInterfaceData::get().lock()->offsetTime),
    responseTime(OpenPassInterfaceData::get().lock()->responseTime),
    cycleTime(OpenPassInterfaceData::get().lock()->cycleTime),
    stochastics(OpenPassInterfaceData::get().lock()->stochasticsInterface),
    world(OpenPassInterfaceData::get().lock()->worldInterface),
    parameters(OpenPassInterfaceData::get().lock()->parameterInterface),
    publisher(OpenPassInterfaceData::get().lock()->publisherInterface),
    agent(OpenPassInterfaceData::get().lock()->agentInterface),
    callbacks(OpenPassInterfaceData::get().lock()->callbackInterface)
{
}

void SsdToSspNetworkParser::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message) const
{
    OpenPassInterfaceData::get().lock()->Log(logLevel, file, line, message);
}

const variant_visitor osmpConnectorNameVisitor{
    [](const SspParserTypes::OSMPTripleLink &link) {
        return link.first;
    },
    [](const SspParserTypes::SimpleNameConnector &name) {
        return name.substr(0,name.find('.'));
    },
    [](const SspParserTypes::OSMPSingleLink &link) {
        return link.second.name.substr(0,link.second.name.find('.'));
    }};

const variant_visitor connectorRoleVisitor{
    [](const SspParserTypes::OSMPTripleLink &link) {
        return link.first;
    },
    [](const SspParserTypes::SimpleNameConnector &name) {
        const std::string msg = "SimpleNameConnector has no role";
        throw SspParserLogicError(msg);
        return std::basic_string<char>();
    },
    [](const SspParserTypes::OSMPSingleLink &link) {
        return OSMPConnector::convertToSuffix(link.second.osmpLinkRole);
    }};

const variant_visitor fmuOSMPLinkNameVisitor{
    [](const SspParserTypes::OSMPTripleLink &link) {
        return link.second;
    },
    [](const SspParserTypes::SimpleNameConnector &name) {
        return name;
    },
    [](const SspParserTypes::OSMPSingleLink &link) {
        return link.second.osmpLinkName;
    }};

const variant_visitor osiTypeVisitor{
    [](const SspParserTypes::OSMPTripleLink &link) {
        return link.second;
    },
    [](const SspParserTypes::SimpleNameConnector &name) {
        const std::basic_string<char> msg = "SimpleNameConnector has no osi type";
        throw SspParserLogicError(msg);
        return std::basic_string<char>();
    },
    [](const SspParserTypes::OSMPSingleLink &link) {
        return OSMPConnector::stringFromOsiType(link.second.osiType);
    }};

constexpr variant_visitor connectorNameVisitor
    {
        [](const SspParserTypes::OSMPTripleLink &link)
        {
            return link.first;
        },
        [](const SspParserTypes::SimpleNameConnector &name) {
            return name;
        },
        [](const SspParserTypes::OSMPSingleLink &link)
        {
            return link.second.name;
        }
    };

std::shared_ptr<ssp::System> SsdToSspNetworkParser::getRootSystem(const std::vector<std::shared_ptr<SsdFile>> &ssdFiles)
{
    auto rootFileIterator = std::find_if(ssdFiles.cbegin(), ssdFiles.cend(), [](auto ssdFile) {
        return std::filesystem::path{ssdFile->fileName}.filename() == "SystemStructure.ssd";
    });
    if (rootFileIterator == ssdFiles.cend())
    {
        LOGERRORANDTHROW("SSP Parser: No 'SystemStructure.ssd' file found in System Structure Package.");
    }

    this->ssdFiles = &ssdFiles;
    auto rootFile = *rootFileIterator;
    const auto rootSystemName = rootFile->ssdSystem->getName();

    outputDir = generateAgentOutputDir();
    auto rootSystem = parseSsdFile(rootFile);
    LOGINFO("SSP Parser: Created root system " + rootSystemName + ".");
    rootSystem->SetOutputDir(outputDir);
    LOGINFO("SSP Parser: Created SSP root output directory: " + outputDir.toStdString() + ".");

    return rootSystem;
}



std::shared_ptr<ssp::System> SsdToSspNetworkParser::parseSsdFile(const std::shared_ptr<SsdFile> &ssdFile)
{
    const auto ssdSystem = ssdFile->ssdSystem;

    LOGINFO("SSP Parser: Started parsing the ssd file which describes the system " + ssdSystem->getName() + ".");

    std::vector<std::shared_ptr<ssp::ConnectorInterface>> systemInputConnectors {};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> systemOutputConnectors {};

    auto targetToTracesMap = std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>();
    auto outputDirComplete = outputDir + QDir::separator() + QString::fromStdString(ssdSystem->getName());

    handleSystemConnectors(ssdSystem, systemInputConnectors, systemOutputConnectors);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> components = handleComponents(ssdFile, ssdSystem, outputDirComplete, targetToTracesMap);

    LOGINFO("SSP Parser: Initialize system " + ssdSystem->getName() + ".");
    auto system = std::make_shared<ssp::System>(ssdSystem->getName(), std::move(components), std::move(systemInputConnectors), std::move(systemOutputConnectors), targetToTracesMap);
    system->SetOutputDir(outputDirComplete);

    handleConnections(ssdSystem, system);

    return system;
}

const std::vector<SspParserTypes::Connection> &SsdToSspNetworkParser::getConnections() const
{
    return connections;
}
const std::map<SspParserTypes::Component, int> &SsdToSspNetworkParser::getPriorities() const
{
    return priorities;
}
QString SsdToSspNetworkParser::generateAgentOutputDir()
{
    return QString::fromStdString(parameters->GetRuntimeInformation().directories.output) +
                       QDir::separator() + "ssp" + QDir::separator()
                       + "Agent" + QString::fromStdString(FmuFileHelper::createAgentIDString(agent->GetId()));
}

void SsdToSspNetworkParser::handleSystemConnectors(const std::shared_ptr<SsdSystem> &ssdSystem, std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemInputConnectors, std::vector<std::shared_ptr<ssp::ConnectorInterface>> &systemOutputConnectors)
{
    for(const auto &systemConnector : ssdSystem->getConnectors())
    {
        auto connectorType = systemConnector.first;
        if ( connectorType == "input" || connectorType == "inout")
        {
            systemInputConnectors.emplace_back(std::move(std::make_shared<ssp::SsdNameConnector>(std::visit(osmpConnectorNameVisitor,systemConnector.second))));
            inputConnectors.emplace(systemConnector.second);
        }

        if ( connectorType == "output" || connectorType == "inout")
        {
            systemOutputConnectors.emplace_back(std::move(std::make_shared<ssp::SsdNameConnector>(std::visit(osmpConnectorNameVisitor,systemConnector.second))));
            outputConnectors.emplace(systemConnector.second);
        }
    }
}

std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> SsdToSspNetworkParser::handleComponents(const std::shared_ptr<SsdFile> &ssdFile, const std::shared_ptr<SsdSystem> &ssdSystem, QString outputDirComplete, std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap)
{
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> components;

    for (const auto &ssdComponent : ssdSystem->getComponents())
    {
        LOGINFO("SSP Parser: Added new component " + ssdComponent->getName() + ".");

        if (ssdComponent->getPriority().has_value())
        {
            priorities.emplace(ssdComponent->getName(), ssdComponent->getPriority().value());
            LOGINFO("SSP Parser: " + ssdComponent->getName() + " has a priority of " + std::to_string(ssdComponent->getPriority().value()) + ".");
        }
        else
        {
            LOGWARN("SSP Parser: " + ssdComponent->getName() + " has no priority configured.");
        }

        LOGINFO("SSP Parser: Processing write message parameters for " + ssdComponent->getName() + ".");
        for(const auto& writingParameter : ssdComponent->getWriteMessageParameters())
        {
            connectorToMessageTypeList.emplace_back(std::make_pair(ssdComponent->getName(), writingParameter.second), writingParameter.first);
        }

        if (ssdComponent->getComponentType() == SspComponentType::x_fmu_sharedlibrary)
        {
            auto fmuWrapperParameters = getFmuParameters(ssdFile, ssdComponent);
            emplaceAlgorithmFmuWrapper(ssdSystem, ssdComponent, fmuWrapperParameters);
            components.emplace_back(createFmuComponent(ssdComponent, outputDirComplete, targetToTracesMap));
        }
        else if(ssdComponent->getComponentType() == SspComponentType::x_ssp_definition)
        {
            auto source = ssdComponent->getSource();
            auto path = source.path();
            auto pathFragment = source.fragmentId();
            bool foundFile = false;
            std::string fileNamesForDebugging = "";
            for(auto file : *this->ssdFiles)
            {
                auto filename = file.get()->fileName;
                QString seperator = QDir::separator();
                auto substrFilename = filename.substr(filename.find_last_of(seperator.toStdString())+1, filename.length());
                fileNamesForDebugging += filename + "_";
                if(path == substrFilename || pathFragment == substrFilename)
                {
                    auto subsystem = parseSsdFile(file);
                    components.emplace_back(subsystem);
                    foundFile = true;
                }
            }
            if(foundFile == false)
            {
                LOGERRORANDTHROW("SSP Parser: Could not find file described as source: " + path + "#" + pathFragment +
                                 ". These files have been found: " + fileNamesForDebugging);
            }
        }
        else
        {
            components.emplace_back(std::make_shared<ssp::System>(ssdComponent->getName()));
        }
    }
    return components;
}

void SsdToSspNetworkParser::handleConnections(const std::shared_ptr<SsdSystem> &ssdSystem, std::shared_ptr<ssp::System> &system)
{
    LOGINFO("SSP Parser: Read in connections of " + ssdSystem->getName() + ".");
    for (const auto &connection : ssdSystem->getConnections())
    {
        std::string startElement, endElement, startConnectorName, endConnectorName;
        for (const auto &connectionParameter : *connection)
        {
            if (connectionParameter.first == "startElement")
                startElement = connectionParameter.second;
            else if (connectionParameter.first == "endElement")
                endElement = connectionParameter.second;
            else if (connectionParameter.first == "startConnector")
                startConnectorName = connectionParameter.second;
            else if (connectionParameter.first == "endConnector")
                endConnectorName = connectionParameter.second;
            else
            {
                LOGERRORANDTHROW("SSP Parser: Unknown Element or Connector Type " + connectionParameter.first);
            }
        }
        if (startElement.empty() || endElement.empty() || startConnectorName.empty() || endConnectorName.empty())
        {
            LOGERRORANDTHROW("SSP Parser: Not all connection parameters provided in");
        }

        std::optional<SspParserTypes::Connector> startConnector{}, endConnector{};
        std::string osmpRole {};
        std::string osmpLinkName {};

        for (const auto &connector : outputConnectors)
        {
            if (startConnectorName == std::visit(connectorNameVisitor, connector))
            {
                startConnector = connector;
                osmpRole = std::visit(connectorRoleVisitor, connector);
                osmpLinkName = std::visit(fmuOSMPLinkNameVisitor, connector);
                break;
            }
        }
        for (const auto &connector : inputConnectors)
        {
            if (endConnectorName == std::visit(connectorNameVisitor, connector))
            {
                endConnector = connector;
                osmpRole = std::visit(connectorRoleVisitor, connector);
                osmpLinkName = std::visit(fmuOSMPLinkNameVisitor, connector);
                break;
            }
        }

        if (!startConnector.has_value() || !endConnector.has_value())
        {
            logWarningConnectionInvalid(startElement, startConnectorName, endElement, endConnectorName);
        }

        startConnectorName = startConnectorName.substr(0,startConnectorName.find('.'));
        endConnectorName = endConnectorName.substr(0,endConnectorName.find('.'));

        if(!osmpRole.empty())
        {
            system->ManageConnection({startElement, startConnectorName,
                                      endElement, endConnectorName,osmpRole, osmpLinkName});
        }
    }

    generateSubsystemConnections(system);
}

void SsdToSspNetworkParser::generateSubsystemConnections(std::shared_ptr<ssp::System> &system)
{
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectorGroupsIn;
    for(const auto& element : system->getElements())
    {
        auto* subSystem = dynamic_cast<ssp::System*>(element.get());
        if(subSystem == nullptr)
            continue;

        std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectors;
        for(const auto& connector : subSystem->systemInputConnector->connectors)
        {
            subsystemConnectors.emplace_back(connector);
        }
        ssp::GroupConnector subSystemConnectorGroup {subsystemConnectors};
        subsystemConnectorGroupsIn.emplace_back(std::make_shared<ssp::GroupConnector>(subSystemConnectorGroup));

    }
    ssp::GroupConnector subsSystemConnectorGroupGroupConnectorIn {subsystemConnectorGroupsIn};
    system->systemInputConnector->connectors.emplace_back(std::make_shared<ssp::GroupConnector>(subsSystemConnectorGroupGroupConnectorIn));


    std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectorGroupsOut;
    for(const auto& element : system->getElements())
    {
        auto* subSystem = dynamic_cast<ssp::System*>(element.get());
        if(subSystem == nullptr)
            continue;

        std::vector<std::shared_ptr<ssp::ConnectorInterface>> subsystemConnectors;
        for(const auto& connector : subSystem->systemOutputConnector->connectors)
        {
            subsystemConnectors.emplace_back(connector);
        }
        ssp::GroupConnector subSystemConnectorGroup {subsystemConnectors};
        subsystemConnectorGroupsOut.emplace_back(std::make_shared<ssp::GroupConnector>(subSystemConnectorGroup));

    }
    ssp::GroupConnector subsSystemConnectorGroupGroupConnectorOut {subsystemConnectorGroupsOut};
    system->systemOutputConnector->connectors.emplace_back(std::make_shared<ssp::GroupConnector>(subsSystemConnectorGroupGroupConnectorOut));
}

SimulationCommon::Parameters *SsdToSspNetworkParser::getFmuParameters(const std::shared_ptr<SsdFile> &ssdFile, const std::shared_ptr<SsdComponent> &ssdComponent)
{
    auto fmuWrapperParameters = new SimulationCommon::Parameters{parameters->GetRuntimeInformation()};

    LOGINFO("SSP Parser: Adding parameters to " + ssdComponent->getName());
    fmuWrapperParameters->AddParameterString("FmuPath", (std::filesystem::path{ssdFile->fileName}.parent_path() / ssdComponent->getSource().path()).string());
    for (const auto &parameter : ssdComponent->getParameters())
    {
        if (const auto stringParameter = std::get_if<std::string>(&parameter.second))
            fmuWrapperParameters->AddParameterString(parameter.first, *stringParameter);
        else if (const auto boolParameter = std::get_if<bool>(&parameter.second))
            fmuWrapperParameters->AddParameterBool(parameter.first, *boolParameter);
        else if (const auto intParameter = std::get_if<int>(&parameter.second))
            fmuWrapperParameters->AddParameterInt(parameter.first, *intParameter);
        else if (const auto doubleParameter = std::get_if<double>(&parameter.second))
            fmuWrapperParameters->AddParameterDouble(parameter.first, *doubleParameter);
        else
            callbacks->LOGWARN("SSP Parser: Ignoring Parameter " + parameter.first + " because of unsupported type.");
    }

    return fmuWrapperParameters;
}

void SsdToSspNetworkParser::emplaceAlgorithmFmuWrapper(const std::shared_ptr<SsdSystem> &ssdSystem, const std::shared_ptr<SsdComponent> &ssdComponent, SimulationCommon::Parameters * fmuWrapperParameters)
{
    componentToWrapperMap.emplace(ssdComponent->getName(),
                                  std::make_shared<AlgorithmFmuWrapperImplementation>(
                                      *componentName + "." + ssdSystem->getName() + "." + ssdComponent->getName(),
                                      isInit,
                                      priority,
                                      offsetTime,
                                      responseTime,
                                      cycleTime,
                                      world,
                                      stochastics,
                                      fmuWrapperParameters,
                                      publisher,
                                      callbacks,
                                      agent));
}

std::shared_ptr<ssp::FmuComponent> SsdToSspNetworkParser::createFmuComponent(const std::shared_ptr<SsdComponent> &ssdComponent, QString outputDirComplete, std::shared_ptr<std::map<std::string, FmuFileHelper::TraceEntry>> &targetToTracesMap)
{
    LOGINFO("SSP Parser: Read in connectors for " + ssdComponent->getName() + ".");
    OSMPConnectorFactory osmpConnectorFactory {ssdComponent->getName()};
    for (const auto &connector : ssdComponent->getConnectors())
    {
        auto connectorType = connector.first;
        if ( connectorType == "input" || connectorType == "inout")
        {
            inputConnectors.emplace(connector.second);
            osmpConnectorFactory.RegisterInputSingleConnector(
                std::visit(osmpConnectorNameVisitor, connector.second),std::visit(fmuOSMPLinkNameVisitor, connector.second), componentToWrapperMap.at(ssdComponent->getName()),
                ssdComponent->getPriority().value_or(0), OSMPConnector::roleFromString(std::visit(connectorRoleVisitor, connector.second)),std::visit(osiTypeVisitor, connector.second));

        }

        if ( connectorType == "output" || connectorType == "inout")
        {
            outputConnectors.emplace(connector.second);
            osmpConnectorFactory.RegisterOutputSingleConnector(
                std::visit(osmpConnectorNameVisitor, connector.second),std::visit(fmuOSMPLinkNameVisitor, connector.second), componentToWrapperMap.at(ssdComponent->getName()),
                ssdComponent->getPriority().value_or(0), OSMPConnector::roleFromString(std::visit(connectorRoleVisitor, connector.second)), std::visit(osiTypeVisitor, connector.second));
        }
    }

    std::vector<std::shared_ptr<ssp::ConnectorInterface>> input_connectors = osmpConnectorFactory.createInputOSMPConnectors(ssdComponent->getWriteMessageParameters(), outputDirComplete, targetToTracesMap);
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> output_connectors = osmpConnectorFactory.createOutputOSMPConnectors(ssdComponent->getWriteMessageParameters(), outputDirComplete, targetToTracesMap);
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;

    auto fmuComponent = std::make_shared<ssp::FmuComponent>(ssdComponent->getName(), std::move(emptyElements), std::move(input_connectors), std::move(output_connectors), componentToWrapperMap.at(ssdComponent->getName()));
    return fmuComponent;
}

void SsdToSspNetworkParser::logWarningConnectionInvalid(std::string startElement, std::string startConnectorName,
                                                        std::string endElement, std::string endConnectorName) const
{
    std::string message = "Connection specifies nonexistent connector: (";
    message.append(startElement);
    message.append(".");
    message.append(startConnectorName);
    message.append(")->(");
    message.append(endElement);
    message.append(".");
    message.append(endConnectorName);
    message.append(")");

    LOGWARN(message);
}
