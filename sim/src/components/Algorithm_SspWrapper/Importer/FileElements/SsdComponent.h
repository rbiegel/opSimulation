/********************************************************************************
 * Copyright (c) 2020-2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


#pragma once

#include "components/Algorithm_SspWrapper/OSMPConnectorFactory.h"
#include "src/common/parameter.h"

#include "SsdURI.h"
#include "include/parameterInterface.h"

enum class SspComponentType
{
    x_none,
    x_fmu_sharedlibrary,
    x_ssp_definition
};

class SsdComponent
{
public:
    SsdComponent(std::string name, std::string source, SspComponentType);

    SsdComponent(std::string name, SsdURI source, SspComponentType);

    void SetParameters(openpass::parameter::internal::ParameterSetLevel3 parameters);

    void emplaceConnector(std::string &&kind, SspParserTypes::Connector &&);

    [[nodiscard]] const std::string &getName() const;

    [[nodiscard]] const SsdURI &getSource() const;

    [[nodiscard]] const SspComponentType &getComponentType() const;

    [[nodiscard]] const openpass::parameter::internal::ParameterSetLevel3 &getParameters() const;

    [[nodiscard]] const std::vector<std::pair<std::string, SspParserTypes::Connector>> getConnectors() const;

    [[nodiscard]] const std::optional<int> getPriority() const;
    void setPriority(int newPriority);
    [[nodiscard]] const std::vector<std::pair<std::string, std::string>> &getWriteMessageParameters() const;
    void setWriteMessageParameters(std::vector<std::pair<std::string, std::string>> JsonParameters);
    [[nodiscard]] const std::vector<int> &getUpdateOutputParameter() const;
    void addUpdateOutputParameter(int &&);

private:
    std::string name;
    SsdURI source;
    SspComponentType componentType;
    std::optional<int> priority;
    std::vector<std::pair<std::string, std::string>> writeMessageParameters{};
    std::vector<int> updateOutputParameter{};
    std::vector<std::pair<std::string, SspParserTypes::Connector>> connectors{};
    openpass::parameter::internal::ParameterSetLevel3 parameters{};
};
