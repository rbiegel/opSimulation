/********************************************************************************
 * Copyright (c) 2020-2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "SsdFileImporter.h"
#include "FileElements/SsdURI.h"

#include "components/Algorithm_SspWrapper/OSMPConnectorFactory.h"
#include <filesystem>
#include <regex>
#include <utility>

template<class T>
void SsdFileImporter::ImportConnectors(const QDomElement &connectorsElement, const std::shared_ptr<T> &ssdComponent,
                                       const CallbackInterface *callbacks) {
    QDomElement connectorElement;
    if (!SimulationCommon::GetFirstChildElement(connectorsElement, "Connector", connectorElement)) {
        std::string errorMessage = "SSP Importer: Unable to retrieve connectors.";
        LOGERROR(errorMessage);
        throw std::runtime_error(errorMessage);
    }

    std::string connectorName, binaryVariableName, kind, role, mimeType;

    while (!connectorElement.isNull()) {
        SimulationCommon::ParseAttribute(connectorElement, "name", connectorName);
        SimulationCommon::ParseAttribute(connectorElement, "kind", kind);

        QDomElement annotations;
        if (SimulationCommon::GetFirstChildElement(connectorElement, "Annotations", annotations)) {
            QDomElement annotation;
            if (SimulationCommon::GetFirstChildElement(annotations, "Annotation", annotation)) {
                QDomElement binaryVariable;
                if (SimulationCommon::GetFirstChildElement(annotation, "osmp-binary-variable", binaryVariable)) {
                    SimulationCommon::ParseAttribute(binaryVariable, "name", binaryVariableName);
                    SimulationCommon::ParseAttribute(binaryVariable, "role", role);
                    SimulationCommon::ParseAttribute(binaryVariable, "mime-type", mimeType);

                    std::regex typeRegex {"type=([a-zA-Z]*);", std::regex::optimize};
                    std::smatch typeMatch;
                    if (std::regex_search(mimeType, typeMatch, typeRegex)) {
                        ssdComponent->emplaceConnector(std::move(kind), SspParserTypes::OSMPSingleLink{ssdComponent->getName(), {connectorName, binaryVariableName, role, typeMatch[1]}});
                    } else {
                        const std::string errorMessage{"SSP Importer: Unable to parse osi type from mime-type: " + mimeType};
                        LOGERROR(errorMessage);
                        throw std::runtime_error(errorMessage);
                    }
                }
                else {
                    std::string annotationType;
                    SimulationCommon::ParseAttribute(annotation, "type", annotationType);
                    LOGWARN("SSP Importer: Parse unknown annotation: ignore " + annotationType);
                }
            }
        }
        else {
            ssdComponent->emplaceConnector(std::move(kind), SspParserTypes::SimpleNameConnector{ssdComponent->getName()});
        }
        connectorElement = connectorElement.nextSiblingElement("Connector");
    }
}

void SsdFileImporter::ImportComponentParameterSets(QDomElement &parameterSetElement,
                                                   const std::shared_ptr <SsdComponent> &component,
                                                   const CallbackInterface *callbacks) {
    QDomElement parametersElement;
    if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement)) {
        std::string errorMsg = "SSP Importer: Unable to retrieve parameters.";
        LOGERRORANDTHROW(errorMsg);
    }
    while (!parameterSetElement.isNull()) {
        std::string name;
        SimulationCommon::ParseAttribute(parameterSetElement, "name", name);
        if (name == "FmuParameters") {
            QDomElement parametersElement;

            if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement)) {
                std::string errorMsg = "SSP Importer: Unable to retrieve FmuParameters.";
                LOGERRORANDTHROW(errorMsg);
            }

            QDomElement parameterElement;
            if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement)) {
                openpass::parameter::internal::ParameterSetLevel3 parameters;

                while (!parameterElement.isNull()) {
                    std::string parameterName;
                    SimulationCommon::ParseAttribute(parameterElement, "name", parameterName);

                    QDomElement valueElement;
                    if (SimulationCommon::GetFirstChildElement(parameterElement, "String", valueElement)) {
                        std::string value;
                        SimulationCommon::ParseAttribute(valueElement, "value", value);
                        parameters.emplace_back(parameterName, value);
                    }
                    if (SimulationCommon::GetFirstChildElement(parameterElement, "Boolean", valueElement)) {
                        std::string value;
                        SimulationCommon::ParseAttribute(valueElement, "value", value);

                        bool valueRobust = false;
                        if (value == "true") {
                            valueRobust = true;
                        }
                        parameters.emplace_back(parameterName, valueRobust);
                    }

                    parameterElement = parameterElement.nextSiblingElement("Parameter");
                }
                component->SetParameters(parameters);
            }
        } else if (name == "PriorityParameter") {
            QDomElement parametersElement;

            if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement)) {
                std::string errorMsg = "SSP Importer: Unable to retrieve PriorityParameter.";
                LOGERRORANDTHROW(errorMsg);
            }

            QDomElement parameterElement;
            if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement)) {
                QDomElement valueElement;
                std::string value;
                valueElement = parameterElement.firstChildElement("Integer");
                SimulationCommon::ParseAttribute(valueElement, "value", value);
                component->setPriority(std::stoi(value));
            }
        } else if (name == "WriteMessageParameters") {
            QDomElement parametersElement;

            if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement)) {
                std::string errorMsg = "SSP Importer: Unable to retrieve WriteMessageParameters.";
                LOGERRORANDTHROW(errorMsg);
            }

            QDomElement parameterElement;
            if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement)) {
                std::vector <std::pair<std::string, std::string>> parameters;

                while (!parameterElement.isNull()) {
                    std::string parameterName;
                    SimulationCommon::ParseAttribute(parameterElement, "name", parameterName);

                    QDomElement valueElement;
                    if (SimulationCommon::GetFirstChildElement(parameterElement, "String", valueElement)) {
                        std::string value;
                        SimulationCommon::ParseAttribute(valueElement, "value", value);
                        parameters.emplace_back(std::make_pair(parameterName, value));
                    }
                    parameterElement = parameterElement.nextSiblingElement("Parameter");
                }
                component->setWriteMessageParameters(parameters);
            }
        } else if (name == "UpdateOutputParameter") {
            QDomElement parametersElement;

            if (!SimulationCommon::GetFirstChildElement(parameterSetElement, "Parameters", parametersElement)) {
                std::string errorMsg = "SSP Importer: Unable to retrieve UpdateOutputParameter.";
                LOGERRORANDTHROW(errorMsg);
            }

            QDomElement parameterElement;
            if (SimulationCommon::GetFirstChildElement(parametersElement, "Parameter", parameterElement)) {
                QDomElement valueElement;
                std::string value;
                valueElement = parameterElement.firstChildElement("String");
                SimulationCommon::ParseAttribute(valueElement, "value", value);
                std::stringstream ss(value);
                std::vector<int> localLinkIds;
                while (ss.good()) {
                    std::string substr;
                    getline(ss, substr, ',');
                    component->addUpdateOutputParameter(stoi(substr));
                }
            }
        } else {
            LOGINFO("SSP Importer: Ignoring unknown parameterSet: " + name);
        }
        parameterSetElement = parameterSetElement.nextSiblingElement("ParameterSet");
    }
}


void SsdFileImporter::ImportComponentParameters(QDomElement &parameterBindingsElement,
                                                const std::shared_ptr <SsdComponent> &component,
                                                const CallbackInterface *callbacks, const std::string &filename) {
    QDomElement parameterBindingElement;
    if (!SimulationCommon::GetFirstChildElement(parameterBindingsElement, "ParameterBinding",
                                                parameterBindingElement)) {
        LOGERRORANDTHROW("SSP Importer: Unable to retrieve parameters.");
    }
    while (!parameterBindingElement.isNull()) {
        if (parameterBindingElement.hasAttribute("source")) {
            SsdURI ssvSourceElement;
            SimulationCommon::ParseAttribute(parameterBindingElement, "source", ssvSourceElement);
            std::filesystem::path ssvSource(filename);
            std::filesystem::path ssvSourceFolder(ssvSourceElement.path());
            if (!ssvSourceFolder.is_absolute()) {
                ssvSource = ssvSource.parent_path() / ssvSourceFolder;
            }
            QDomDocument document;

            ImportSsdFileContent(ssvSource.string(), document, callbacks);

            QDomElement documentRoot = document.documentElement();
            if (!documentRoot.isNull()) {
                ImportComponentParameterSets(documentRoot, component, callbacks);
            }
        } else {
            QDomElement parameterValuesElement;
            if (SimulationCommon::GetFirstChildElement(parameterBindingElement, "ParameterValues",
                                                       parameterValuesElement)) {
                QDomElement parameterSetElement;
                if (SimulationCommon::GetFirstChildElement(parameterValuesElement, "ParameterSet",
                                                           parameterSetElement)) {
                    ImportComponentParameterSets(parameterSetElement, component, callbacks);
                }
            }
        }
        parameterBindingElement = parameterBindingElement.nextSiblingElement("ParameterBinding");
    }
}

void SsdFileImporter::ImportSystemConnections(const QDomElement &connectionsElement, const std::shared_ptr<SsdSystem> &ssdSystem, const CallbackInterface *callbacks)
{
    QDomElement connectionElement;
    //ThrowIfFalse(SimulationCommon::GetFirstChildElement(connectionsElement, "Connection", connectionElement),
    //             "Unable to retrieve connections.");
    if (!SimulationCommon::GetFirstChildElement(connectionsElement, "Connection", connectionElement))
    {
        std::string logMsg = "SSP Importer: No Connection element present.";
        LOGINFO(logMsg);
        return;
    }

    std::string startElement;
    std::string endElement;
    std::string startConnector;
    std::string endConnector;

    while (!connectionElement.isNull())
    {
        std::map<std::string, std::string> map;

        if (!SimulationCommon::ParseAttribute(connectionElement, "startConnector", startConnector))
        {
            std::string errorMsg = "SSP Importer: Unable to retrieve connection startConnector.";
            LOGERRORANDTHROW(errorMsg);
        }
        map.insert(std::make_pair("startConnector", startConnector));

        if (!SimulationCommon::ParseAttribute(connectionElement, "endConnector", endConnector))
        {
            std::string errorMsg = "SSP Importer: Unable to retrieve connection endConnector.";
            LOGERRORANDTHROW(errorMsg);
        }
        map.insert(std::make_pair("endConnector", endConnector));

        if (SimulationCommon::ParseAttribute(connectionElement, "startElement", startElement))
            map.emplace("startElement", startElement);
        else
            map.emplace("startElement", ssdSystem->getName());
        if (SimulationCommon::ParseAttribute(connectionElement, "endElement", endElement))
            map.emplace("endElement", endElement);
        else
            map.emplace("endElement", ssdSystem->getName());
        ssdSystem->AddConnection(std::make_shared<std::map<std::string, std::string>>(map));
        connectionElement = connectionElement.nextSiblingElement("Connection");
    }
}

void SsdFileImporter::ImportSystemComponents(QDomElement &elementsElement, const std::shared_ptr <SsdSystem> &ssdSystem,
                                             const CallbackInterface *callbacks, const std::string &filename) {
    QDomElement componentsElement;
    if (SimulationCommon::GetFirstChildElement(elementsElement, "Component", componentsElement)) {
        while (!componentsElement.isNull()) {
            std::string componentName;
            if (!SimulationCommon::ParseAttribute(componentsElement, "name", componentName))
            {
                std::string errorMsg = "SSP Importer: Unable to retrieve component name: " + componentName;
                LOGERRORANDTHROW(errorMsg);
            }

            std::string componentSource;
            if (!SimulationCommon::ParseAttribute(componentsElement, "source", componentSource))
            {
                std::string errorMsg = "SSP Importer: Unable to retrieve component source: " + componentSource;
                LOGERRORANDTHROW(errorMsg);
            }

            std::string componentType;
            if (!SimulationCommon::ParseAttribute(componentsElement, "type", componentType))
            {
                std::string errorMsg = "SSP Importer: Unable to retrieve component type: " + componentType + "\nDefauting to application/x-fmu-sharedlibrary";
                LOGINFO(errorMsg);
                componentType = "application/x-fmu-sharedlibrary";
            }

            std::shared_ptr<SsdComponent> ssdComponent{};
            if (componentType == "application/x-fmu-sharedlibrary")
                ssdComponent = std::make_shared<SsdComponent>(std::move(componentName), std::move(componentSource), SspComponentType::x_fmu_sharedlibrary);
            else if (componentType == "application/x-ssp-definition")
                ssdComponent = std::make_shared<SsdComponent>(std::move(componentName), std::move(componentSource), SspComponentType::x_ssp_definition);

            LOGINFO("SSP Importer: import connectors");
            QDomElement connectorsElement;
            if (SimulationCommon::GetFirstChildElement(componentsElement, "Connectors", connectorsElement))
            {
                ImportConnectors(connectorsElement, ssdComponent, callbacks);
            }

            LOGINFO("SSP Importer: import parameters");
            QDomElement parameterBindingsElement;
            if (SimulationCommon::GetFirstChildElement(componentsElement, "ParameterBindings", parameterBindingsElement))
            {
                ImportComponentParameters(parameterBindingsElement, ssdComponent, callbacks, filename);
            }
                ssdSystem->AddComponent(std::move(ssdComponent));
            componentsElement = componentsElement.nextSiblingElement("Component");
        }
    }
}

bool SsdFileImporter::ImportSsdFileContent(const std::string &filename, QDomDocument &document, const CallbackInterface *callbacks)
{
    std::locale::global(std::locale("C"));

    if (!QFileInfo(QString::fromStdString(filename)).exists())
    {
        LOGINFO("SSP Importer: SsdFile: " + filename + " does not exist.");
        return false;
    }

    QFile xmlFile(QString::fromStdString(filename));
    if (!xmlFile.open(QIODevice::ReadOnly))
    {
        std::string errorMsg = "SSP Importer: an error occurred during SsdFile import: " + filename;
        LOGERRORANDTHROW(errorMsg);
    }

    QByteArray xmlData(xmlFile.readAll());
    QString errorMsg{};
    int errorLine{};
    if (!document.setContent(xmlData, true, &errorMsg, &errorLine))
    {
        std::string message = "SSP Importer: invalid xml file format of file " + filename + " in line " + std::to_string(errorLine) + " : " + errorMsg.toStdString();
        LOGERRORANDTHROW(message);
    }

    return true;
}

bool SsdFileImporter::Import(const std::string &filename, std::vector<std::shared_ptr<SsdFile>> &ssdFiles, const CallbackInterface *callbacks)
{
    try
    {
        QDomDocument document;
        if (!ImportSsdFileContent(filename, document, callbacks))
        {
            return false;
        }

        QDomElement documentRoot = document.documentElement();
        if (documentRoot.isNull())
        {
            return false;
        }

        QDomElement systemElement;
        QString localName = "ssd";
        QString defValue{};
        const QString &string = documentRoot.attributeNS("http://ssp-standard.org/SSP1/SystemStructureDescription", localName, defValue);
        if (SimulationCommon::GetFirstChildElement(documentRoot, "System", systemElement))
        {
            std::string systemId;
            if (!SimulationCommon::ParseAttribute(systemElement, "name", systemId)) {
                std::string errorMsg = "SSP Importer: Unable to retrieve system name: " + systemId;
                LOGERRORANDTHROW(errorMsg);
            }

            auto ssdSystem = std::make_shared<SsdSystem>(systemId);
            ImportSystem(filename, systemElement, ssdSystem, callbacks);
            ssdFiles.emplace_back(std::make_shared<SsdFile>(filename, ssdSystem));
        }
        return true;
    }
    catch (const std::runtime_error &e) {
        LOGERROR("SSP Importer: SsdFile import failed.");
        throw std::runtime_error(e.what());
    }
}

void SsdFileImporter::ImportSystem(const std::string &filename, const QDomElement &systemElement,
                                   const std::shared_ptr<SsdSystem> &ssdSystem,
                                   const CallbackInterface *callbacks) {


    LOGINFO("SSP Importer: Import components");
    QDomElement elementsElement;
    if (SimulationCommon::GetFirstChildElement(systemElement, "Elements", elementsElement)) {
        try {
            ImportSystemComponents(elementsElement, ssdSystem, callbacks, filename);
        }
        catch (const std::runtime_error &error) {
            LOGERROR("SSP Importer: Unable to import system components.");
            throw std::runtime_error(error.what());
        }

        LOGINFO("SSP Importer: Import systems");
        QDomElement subsystemElement;
        if (SimulationCommon::GetFirstChildElement(elementsElement, "System", subsystemElement)) {
            while (!subsystemElement.isNull()) {
                std::string systemId;
                if (!SimulationCommon::ParseAttribute(systemElement, "name", systemId)) {
                    std::string errorMsg = "SSP Importer: Unable to retrieve system name: " + systemId;
                    LOGERROR(errorMsg);
                    throw std::runtime_error(errorMsg);
                }

                auto ssdSubSystem = std::make_shared<SsdSystem>(systemId);
                ImportSystem(filename, subsystemElement, ssdSubSystem, callbacks);
                ssdSystem->AddSystem(std::move(ssdSubSystem));

                subsystemElement = subsystemElement.nextSiblingElement("System");
            }
        }

    }

    LOGINFO("SSP Importer: Import connectors");
    QDomElement connectorsElement;
    if (SimulationCommon::GetFirstChildElement(systemElement, "Connectors", connectorsElement)) {
        try {
            ImportConnectors(connectorsElement, ssdSystem, callbacks);
        }
        catch (const std::runtime_error &error) {
            callbacks->LOGERROR("SSP Importer: Unable to import system connectors.");
            throw std::runtime_error(error.what());
        }
    }

    LOGINFO("SSP Importer: Import connections");
    QDomElement connectionsElement;
    SimulationCommon::GetFirstChildElement(systemElement, "Connections", connectionsElement);

    try {
        ImportSystemConnections(connectionsElement, ssdSystem, callbacks);
    }
    catch (const std::runtime_error &error) {
        LOGERROR("SSP Importer: Unable to import system connections.");
        throw std::runtime_error(error.what());
    }

}

void SsdFileImporter::Log(CbkLogLevel logLevel, const char *file, int line, const std::string &message)
{
    OpenPassInterfaceData::get().lock()->Log(logLevel, file, line, message);
}