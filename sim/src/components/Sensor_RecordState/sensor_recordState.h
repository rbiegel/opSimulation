/********************************************************************************
 * Copyright (c) 2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  sensor_recordState.h
*	@brief This file provides the exported methods.
*
*   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#pragma once

#include <QtGlobal>

/// TODO
#if defined(SENSOR_RECORD_STATE_LIBRARY)
#  define SENSOR_RECORD_STATE_SHARED_EXPORT Q_DECL_EXPORT   //!< Export of the dll-functions
#else
#  define SENSOR_RECORD_STATE_SHARED_EXPORT Q_DECL_IMPORT   //!< Import of the dll-functions
#endif

#include "include/modelInterface.h"


