/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/events/speedActionEvent.h"
#include "common/speedActionSignal.h"
#include "transformerBase.h"

namespace openScenario::transformation {

/// Speed action signal
struct SpeedAction : public TransformerBase<SpeedAction, SpeedActionSignal, openpass::events::SpeedActionEvent>
{
    /**
     * @brief Convert to signal
     * 
     * @param event     Acquire position action
     * @param world     Pointer to the world interface
     * @param agent     Pointer to agent instance
     * @param cycleTime Corresponds to "cycleTime" of "Component"
     * @return TODO
     */
    static std::shared_ptr<SpeedActionSignal> ConvertToSignal(const openpass::events::SpeedActionEvent &event,
                                                              WorldInterface *world,
                                                              AgentInterface *agent,
                                                              int cycleTime);
};

} // namespace openScenario::transformation
