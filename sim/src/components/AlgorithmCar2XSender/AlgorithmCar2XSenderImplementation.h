/********************************************************************************
 * Copyright (c) 2018-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "common/accelerationSignal.h"
#include "common/primitiveSignals.h"
#include "common/sensorDataSignal.h"
#include "include/modelInterface.h"
#include "include/radioInterface.h"
#include <osi3/osi_sensordata.pb.h>

//! Type of information detected by a sensor
enum class SensorInformationType
{
    Distance,
    PositionX,
    PositionY,
    Yaw,
    Velocity,
    Acceleration
};

/**
 * @brief Implementation of algorithm for car2xsender
 * 
 */
class AlgorithmCar2XSenderImplementation : public UnrestrictedModelInterface
{
public:
    /// Name of this component
    const std::string COMPONENTNAME = "AlgorithmCar2XSender";

    /**
     * @brief Construct a new Algorithm Car 2 X Sender Implementation object
     * 
     * @param[in]     componentName  Name of the component
     * @param[in]     isInit         Corresponds to "init" of "Component"
     * @param[in]     priority       Corresponds to "priority" of "Component"
     * @param[in]     offsetTime     Corresponds to "offsetTime" of "Component"
     * @param[in]     responseTime   Corresponds to "responseTime" of "Component"
     * @param[in]     cycleTime      Corresponds to "cycleTime" of "Component"
     * @param[in]     stochastics    Pointer to the stochastics class loaded by the framework
     * @param[in]     world          Pointer to the world
     * @param[in]     parameters     Pointer to the parameters of the module
     * @param[in]     publisher      Pointer to the publisher instance
     * @param[in]     callbacks      Pointer to the callbacks
     * @param[in]     agent          Pointer to agent instance
     */
    AlgorithmCar2XSenderImplementation(
            std::string componentName,
            bool isInit,
            int priority,
            int offsetTime,
            int responseTime,
            int cycleTime,
            StochasticsInterface *stochastics,
            WorldInterface *world,
            const ParameterInterface *parameters,
            PublisherInterface * const publisher,
            const CallbackInterface *callbacks,
            AgentInterface *agent);
    AlgorithmCar2XSenderImplementation(const AlgorithmCar2XSenderImplementation&) = delete;
    AlgorithmCar2XSenderImplementation(AlgorithmCar2XSenderImplementation&&) = delete;
    AlgorithmCar2XSenderImplementation& operator=(const AlgorithmCar2XSenderImplementation&) = delete;
    AlgorithmCar2XSenderImplementation& operator=(AlgorithmCar2XSenderImplementation&&) = delete;
    virtual ~AlgorithmCar2XSenderImplementation() = default;

    /*!
     * \brief Update Inputs
     *
     * Function is called by framework when another component delivers a signal over
     * a channel to this component (scheduler calls update tasks of other component).
     *
     * Refer to module description for input channels and input ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentInput"
     * \param[in]     data           Referenced signal (copied by sending component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateInput(int localLinkId, const std::shared_ptr<SignalInterface const> &data, int time) override;

    /*!
     * \brief Update outputs.
     *
     * Function is called by framework when this Component.has to deliver a signal over
     * a channel to another component (scheduler calls update task of this component).
     *
     * Refer to module description for output channels and output ids.
     *
     * \param[in]     localLinkId    Corresponds to "id" of "ComponentOutput"
     * \param[out]    data           Referenced signal (copied by this component)
     * \param[in]     time           Current scheduling time
     */
    void UpdateOutput(int localLinkId, std::shared_ptr<SignalInterface const> &data, int time) override;

    /*!
     * \brief Process data within component.
     *
     * Function is called by framework when the scheduler calls the trigger task
     * of this component.
     *
     * Refer to module description for information about the module's task.
     *
     * \param[in]     time           Current scheduling time
     */
    void Trigger(int time) override;

private:
  
    //-----------------------------------------------------------------------------
    //! Sets information that the object can provide (e.g. Acceleration)
    //! Provided information is set in profile
    //! @param[in}] object instance to fill out
    //-----------------------------------------------------------------------------
    osi3::MovingObject FillObjectInformation();
	
	std::vector<SensorInformationType> informationToSend;

	double signalStrength = 0.0;

    int currentTimestep{0};                                  //! for observer
    int triggerTime = std::numeric_limits<int>::max();


    // event
    std::string sequenceName {};
};
