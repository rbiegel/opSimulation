/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmCar2XSenderGlobal.h
*	@brief This file contains DLL export declarations
**/
//-----------------------------------------------------------------------------

#pragma once

#include <QtGlobal>

/// TODO
#if defined(ALGORITHM_CAR_TO_X_SENDER_LIBRARY)
#  define ALGORITHM_CAR_TO_X_SENDER_SHARED_EXPORT Q_DECL_EXPORT //!< Export of the dll-functions
#else
#  define ALGORITHM_CAR_TO_X_SENDER_SHARED_EXPORT Q_DECL_IMPORT //!< Import of the dll-functions
#endif


