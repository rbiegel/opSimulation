/********************************************************************************
 * Copyright (c) 2018 in-tech GmbH
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  AlgorithmCar2XSender.h
*	@brief This file provides the exported methods.
*
*   This file provides the exported methods which are available outside of the library. */
//-----------------------------------------------------------------------------

#pragma once

#include "AlgorithmCar2XSenderGlobal.h"
#include "include/modelInterface.h"


