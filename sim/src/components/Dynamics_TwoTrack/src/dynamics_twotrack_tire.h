/********************************************************************************
 * Copyright (c) 2020-2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#ifndef TIRE_H
#define TIRE_H

//! Static tire model based on TMEASY by Rill et al.
class Tire
{
public:

    Tire();

    /**
     * @brief Construct a new Tire object
     * 
     * @param F_ref         Tire force
     * @param mu_tire_max   Peak tire force
     * @param mu_tire_slide Force at full slide
     * @param s_max         Slip corresponding to peak tire force
     * @param r             Radius of the tire
     * @param mu_scale      Road/tire friction coefficient
     */
    Tire(const double F_ref, const double mu_tire_max, const double mu_tire_slide, const double s_max,
         const double r, const double mu_scale);

    virtual ~Tire() = default;

    /// radius
    double radius;
    /// inertia of the vehicle
    const double inertia = 1.2;

    /**
     * @brief Get the Force object
     * 
     * @param slip
     * @return double 
     */
    double GetForce(const double);

    /**
     * @brief Get the longitudinal slip
     * 
     * @param tq    Torque
     * @return Longitudinal slip 
     */
    double GetLongSlip(const double tq);

    /**
     * @brief  Calculate slip in y direction (lateral slip)
     *  
     * @param slipX Longitudinal slip
     * @param vx    Sliding velocity in longitudinal direction
     * @param vy    Sliding velocity in lateral direction
     * @return Lateral slip 
     */
    double CalcSlipY(double slipX, double vx, double vy);

    /**
     * @brief Get the rolling friction
     * 
     * @param velTireX  Velocity of tires
     * @return Rolling friction 
     */
    double GetRollFriction(const double velTireX);

    /**
     * @brief Rescale
     * 
     * @param forceZ_update TODO
     */
    void Rescale(const double forceZ_update);

private:

    double forceZ_static;
    double forceZ;

    double forcePeak_static;
    double forceSat_static;
    double slipPeak;
    double slipSat;
    double forcePeak;
    double forceSat;

    const double frictionRoll = 0.01;
    const double stiffnessRoll = 0.3;
    const double velocityLimit = 0.27; // ca. 1 km/h
    const double s_slide = 0.4;

};

#endif // TIRE_H
