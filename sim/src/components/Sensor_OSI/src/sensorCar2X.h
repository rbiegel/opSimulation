/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  sensorCar2X.h
*	\brief This file models a receiver which detects Car2X senders and acquires their detectedObjects.
*/
//-----------------------------------------------------------------------------

#pragma once

#include "objectDetectorBase.h"
#include "common/sensorDataSignal.h"

//-----------------------------------------------------------------------------
/** \brief This file models a sensor which detects vehicles via Car2X capability.
* 	\details This sensor uses a broker-class concept to "sense" Car2X senders via a radio cloud.
*
* 	\ingroup SensorObjectDetector
*/
//-----------------------------------------------------------------------------
class SensorCar2X : public ObjectDetectorBase
{
public:
    /**
     * @brief Construct a new Sensor Car 2 X object
     * 
     * \param [in] componentName   Name of the component
     * \param [in] isInit          Query whether the component was just initialized
     * \param [in] priority        Priority of the component
     * \param [in] offsetTime      Offset time of the component
     * \param [in] responseTime    Response time of the component
     * \param [in] cycleTime       Cycle time of this components trigger task [ms]
     * \param [in] stochastics     Provides access to the stochastics functionality of the framework
     * \param [in] world           Provides access to world representation
     * \param [in] parameters      Interface provides access to the configuration parameters
     * \param [in] publisher       Instance  provided by the framework
     * \param [in] callbacks       Interface for callbacks to framework
     * \param [in] agent           This interface provides access to agent parameters, properties, attributes and dynamic states
     */
    SensorCar2X(
            std::string componentName,
            bool isInit,
            int priority,
            int offsetTime,
            int responseTime,
            int cycleTime,
            StochasticsInterface *stochastics,
            WorldInterface *world,
            const ParameterInterface *parameters,
            PublisherInterface * const publisher,
            const CallbackInterface *callbacks,
            AgentInterface *agent);

    void UpdateInput(int, const std::shared_ptr<SignalInterface const> &, int);
    void Trigger(int time);

    /**
    * \brief We detect objects via calling the broker-class function RadioInterface::Receive().
    * 
    * @return osi sensor data
    */
    osi3::SensorData DetectObjects();


private:
    double sensitivity;
    osi3::SensorData sensorData;

public:
    SensorCar2X(const SensorCar2X&) = delete;
    SensorCar2X(SensorCar2X&&) = delete;
    SensorCar2X& operator=(const SensorCar2X&) = delete;
    SensorCar2X& operator=(SensorCar2X&&) = delete;
    ~SensorCar2X() = default;
};