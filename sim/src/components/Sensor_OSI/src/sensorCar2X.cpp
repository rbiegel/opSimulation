/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \brief sensorCar2X.cpp */
//-----------------------------------------------------------------------------

#include "sensorCar2X.h"

#include "include/radioInterface.h"
#include "include/worldInterface.h"
#include "include/parameterInterface.h"


SensorCar2X::SensorCar2X(
        std::string componentName,
        bool isInit,
        int priority,
        int offsetTime,
        int responseTime,
        int cycleTime,
        StochasticsInterface *stochastics,
        WorldInterface *world,
        const ParameterInterface *parameters,
        PublisherInterface * const publisher,
        const CallbackInterface *callbacks,
        AgentInterface *agent) :
    ObjectDetectorBase(
        componentName,
        isInit,
        priority,
        offsetTime,
        responseTime,
        cycleTime,
        stochastics,
        world,
        parameters,
        publisher,
        callbacks,
        agent)
{
    sensitivity = parameters->GetParametersDouble().at("Sensitivity");
}

void SensorCar2X::Trigger(int time)
{
    auto newSensorData = DetectObjects();
    sensorData = ApplyLatency(time, newSensorData);
}

void SensorCar2X::UpdateInput(int, const std::shared_ptr<SignalInterface const> &, int)
{
}

osi3::SensorData SensorCar2X::DetectObjects()
{

    RadioInterface& radio = GetWorld()->GetRadio();

    Position absolutePosition = GetAbsolutePosition();

    sensorData = {};
    std::vector<osi3::MovingObject> detectedObjects = radio.Receive(absolutePosition.xPos, absolutePosition.yPos, sensitivity);

    const auto sensorPosition = GetSensorPosition();
    const auto ownYaw = GetAgent()->GetYaw() + position.yaw;
    const ObjectPointCustom mountingPosition{position.longitudinal, position.lateral};
    const auto ownVelocity = GetAgent()->GetVelocity(mountingPosition);
    const auto ownAcceleration = GetAgent()->GetAcceleration(mountingPosition);
    
    for (auto &object : detectedObjects)
    {
        osi3::DetectedMovingObject* detectedObject = sensorData.add_moving_object();
        detectedObject->mutable_header()->add_ground_truth_id()->set_value(object.id().value());
        detectedObject->mutable_header()->add_sensor_id()->set_value(id);
        
        if(object.base().position().has_x() && object.base().position().has_y())
        {
            point_t objectReferencePointGlobal{object.base().position().x(), object.base().position().y()};
            point_t objectReferencePointLocal = TransformPointToLocalCoordinates(objectReferencePointGlobal, sensorPosition, ownYaw);
            detectedObject->mutable_base()->mutable_position()->set_x(objectReferencePointLocal.x());
            detectedObject->mutable_base()->mutable_position()->set_y(objectReferencePointLocal.y());
        }
        if (object.base().orientation().has_yaw())
        {
            detectedObject->mutable_base()->mutable_orientation()->set_yaw(object.base().orientation().yaw() - ownYaw);
        }
        if (object.base().velocity().has_x() && object.base().velocity().has_y())
        {
            point_t objectVelocity{object.base().velocity().x(), object.base().velocity().y()};
            point_t relativeVelocity = CalculateRelativeVector(objectVelocity, {ownVelocity.x, ownVelocity.y}, ownYaw);
            detectedObject->mutable_base()->mutable_velocity()->set_x(relativeVelocity.x());
            detectedObject->mutable_base()->mutable_velocity()->set_y(relativeVelocity.y());
        }
        if (object.base().acceleration().has_x() && object.base().acceleration().has_y())
        {
            point_t objectAcceleration{object.base().acceleration().x(), object.base().acceleration().y()};
            point_t relativeAcceleration = CalculateRelativeVector(objectAcceleration, {ownAcceleration.x, ownAcceleration.y}, ownYaw);
            detectedObject->mutable_base()->mutable_acceleration()->set_x(relativeAcceleration.x());
            detectedObject->mutable_base()->mutable_acceleration()->set_y(relativeAcceleration.y());
        }
    }

    return sensorData;
}