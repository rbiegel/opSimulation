/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  rollSignal.h
//! @brief This file contains all functions for class
//! RollSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include "include/modelInterface.h"

/// This class contains all functionality of the roll signal
class RollSignal: public ComponentStateSignalInterface
{
public:
    /// component name
    const std::string COMPONENTNAME = "RollSignal";

    //-----------------------------------------------------------------------------
    //! Constructor
    //-----------------------------------------------------------------------------
    RollSignal()
    {
        componentState = ComponentState::Disabled;
    }

    /**
     * @brief Construct a new Roll Signal object
     * 
     * @param componentState State of the component
     * @param rollAngle      Roll angle
     */
    RollSignal(
            ComponentState componentState,
            double rollAngle
            ):
        rollAngle(rollAngle)
    {
        this->componentState = componentState;
    }

    /// default constructor
    RollSignal(const RollSignal&) = default;

    /// default constructor
    RollSignal(RollSignal&&) = default;

    /// @return roll signal with operator = overload
    RollSignal& operator=(const RollSignal&) = default;

    /// @return roll signal with operator = overload
    RollSignal& operator=(RollSignal&&) = default;

    virtual ~RollSignal() = default;

    //-----------------------------------------------------------------------------
    //! Returns the content/payload of the signal as an std::string
    //!
    //! @return                       Content/payload of the signal as an std::string
    //-----------------------------------------------------------------------------
    virtual operator std::string() const
    {
        std::ostringstream stream;
        stream << COMPONENTNAME;
        return stream.str();
    }

    /// roll angle
    double rollAngle {0.};

};
