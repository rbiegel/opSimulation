/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \addtogroup Event
* @{
* \file  basicEvent.h
* \brief This file contains all basic functionality of events.
*
* \details This file contains the common event member variables and the basic functions of all events.
* @} */
//-----------------------------------------------------------------------------

#pragma once

#include <vector>

#include "include/eventInterface.h"

namespace openpass::events {

//-----------------------------------------------------------------------------
/** Signal This class contains all functionality of the BasicEvent.
 *
 * \ingroup Event */
//-----------------------------------------------------------------------------
template <EventDefinitions::EventCategory EventCategory>
class BasicEvent : public EventInterface
{
public:
    /**
     * @brief Construct a new Basic Event object
     * 
     * @param time              Current time
     * @param eventName         Name of the event used for identification
     * @param source            Name of the current component
     * @param triggeringAgents  All agents which triggered the event
     * @param actingAgents      All agents which are affected by the event
     */
    BasicEvent(int time, std::string eventName, std::string source,
               const openpass::type::TriggeringEntities triggeringAgents, const openpass::type::AffectedEntities actingAgents) :
        EventInterface{std::move(triggeringAgents), std::move(actingAgents)},
        time(time),
        name(std::move(eventName)),
        source(std::move(source))
    {
    }

    /**
     * @brief Construct a new Basic Event object
     * 
     * @param time      Current time
     * @param eventName Name of the event used for identification
     * @param source    Name of the current component
     */
    BasicEvent(int time, std::string eventName, std::string source) :
        EventInterface{}, time(time), name(std::move(eventName)), source(std::move(source))
    {
    }

    /**
     * @brief Get the Event Time object
     * 
     * @return event time 
     */
    int GetEventTime() const override
    {
        return time;
    }

    /**
     * @brief Get the Category object
     * 
     * @return returns event category
     */
    EventDefinitions::EventCategory GetCategory() const override
    {
        return EventCategory;
    }

    /**
     * @brief Get the Name object
     * 
     * @return name of the event
     */
    const std::string& GetName() const override
    {
        return name;
    }

    /**
     * @brief Get the Source object
     * 
     * @return  Name of the current component
     */
    const std::string& GetSource() const override
    {
        return source;
    }

    /**
     * @brief Get the Triggering Agents object
     * 
     * @return returns triggering agents
     */
    const openpass::type::TriggeringEntities& GetTriggeringAgents() const override
    {
        return triggeringAgents;
    }

    /**
     * @brief Get the Acting Agents object
     * 
     * @return returns affected entities
     */
    const openpass::type::AffectedEntities& GetActingAgents() const override
    {
        return actingAgents;
    }

    /**
     * @brief Get the Parameter object
     * 
     * @return returns flat parameter 
     */
    const openpass::type::FlatParameter& GetParameter() const noexcept override
    {
        return parameter;
    }

private:
    const int time;
    const std::string name;
    const std::string source;

protected:
    /// flat parameter
    openpass::type::FlatParameter parameter{};
};

/// typedef OpenPASS event category as OpenPassEvent
using OpenPassEvent = BasicEvent<EventDefinitions::EventCategory::OpenPASS>;
/// typedef OpenSCENARIO event category as OpenScenarioEvent
using OpenScenarioEvent = BasicEvent<EventDefinitions::EventCategory::OpenSCENARIO>;

} // namespace openpass::events
