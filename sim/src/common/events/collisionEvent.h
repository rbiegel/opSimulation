/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  collisionEvent.h
* @brief This file contains all functions for collision based events.
*
* This class contains all functionality of the module. */
//-----------------------------------------------------------------------------
#pragma once

#include "common/events/basicEvent.h"

namespace openpass::events {

//-----------------------------------------------------------------------------
/** This class implements all functionality of the CollisionEvent.
 *
 * \ingroup Events */
//-----------------------------------------------------------------------------
class CollisionEvent : public OpenPassEvent
{
public:
    /**
     * @brief Construct a new Collision Event object
     * 
     * @param time                  Current time
     * @param source                Name of the current component
     * @param collisionWithAgent    Flag if the collision is with an agent
     * @param collisionAgentId      Id of the collided agent
     * @param collisionOpponentId   Id of the collided opponent
     */
    CollisionEvent(int time, std::string source, bool collisionWithAgent, int collisionAgentId, int collisionOpponentId) :
        BasicEvent{time, "Collision", std::move(source), {{std::move(collisionAgentId), std::move(collisionOpponentId)}}, {}},
        collisionWithAgent{collisionWithAgent},
        collisionAgentId{collisionAgentId},
        collisionOpponentId{collisionOpponentId}
    {
        parameter.try_emplace("CollisionWithAgent", collisionWithAgent);
    }

    /// if collision with agent
    bool collisionWithAgent;
    /// id of the collided agent
    int collisionAgentId;
    /// id of the collided opponent
    int collisionOpponentId;
};

} // namespace openpass::events