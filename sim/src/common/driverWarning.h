 /********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "areaOfInterest.h"
#include <map>
#include <optional>
#include <string>

enum class ComponentWarningLevel
{
    INFO = 0,
    WARNING
};

const std::map<ComponentWarningLevel, std::string> ComponentWarningLevelMapping = {{ComponentWarningLevel::INFO, "Info"},
                                                                                   {ComponentWarningLevel::WARNING, "Warning"}};

enum class ComponentWarningType
{
    OPTIC = 0,
    ACOUSTIC,
    HAPTIC
};

const std::map<ComponentWarningType, std::string> ComponentWarningTypeMapping = {{ComponentWarningType::OPTIC, "Optic"},
                                                                                 {ComponentWarningType::ACOUSTIC, "Acoustic"},
                                                                                 {ComponentWarningType::HAPTIC, "Haptic"}};

enum class ComponentWarningIntensity
{
    LOW = 0,
    MEDIUM,
    HIGH
};

const std::map<ComponentWarningIntensity, std::string> ComponentWarningIntensityMapping = {{ComponentWarningIntensity::LOW, "Low"},
                                                                                           {ComponentWarningIntensity::MEDIUM, "Medium"},
                                                                                           {ComponentWarningIntensity::HIGH, "High"}};


//! Defines a single warning of one component to the driver
struct ComponentWarningInformation
{
    /// is activity true or false
    bool activity; 
    /// component warning level                           
    ComponentWarningLevel level;
    /// component warning type              
    ComponentWarningType type;    
    /// component warning intensity            
    ComponentWarningIntensity intensity; 
    /// area of interest      
    std::optional<AreaOfInterest> direction; 
};
