/********************************************************************************
 * Copyright (c) 2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <functional>
#include <string>
#include <variant>
#include <map>

#include "namedType.h"

namespace openpass::type {

/// typedef agent id as an int
using AgentId = int;
/// typedef entity id as a named type
using EntityId = NamedType<AgentId, struct EntityIdType>;
/// typedef timestamp
using Timestamp = NamedType<int, struct TimestampType>;
/// typedef list of entity ids
using EntityIds = std::vector<EntityId>;

/*!
 * \brief Contains the triggering entities of an acyclic
 *
 *  Might be enhanced in the future to hold additional information about the affected entities
 */
struct TriggeringEntities
{
    /// entity ids type
    type::EntityIds entities;
};

/*!
 * \brief Contains the affected entities of an acyclic
 *
 *  Might be enhanced in the future to hold additional information about the affected entities
 */
struct AffectedEntities
{
    /// entity ids type
    type::EntityIds entities;
};

/// typedef flatparameterkey as a string
using FlatParameterKey = std::string;

/// typedef FlatParameterValue as a variant 
using FlatParameterValue = std::variant<
    bool, std::vector<bool>,
    char, std::vector<char>,
    int, std::vector<int>,
    size_t, std::vector<size_t>,
    float, std::vector<float>,
    double, std::vector<double>,
    std::string, std::vector<std::string>>;
    
/// typedef FlatParameter as a map of FlatparmeterKey and FlatParameterValue
using FlatParameter = std::map<FlatParameterKey, FlatParameterValue>;

} // namespace openpass::type
