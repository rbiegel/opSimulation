/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  dynamicsSignal.h
//! @brief This file contains all functions for class
//! DynamicsSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include "include/modelInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class DynamicsSignal : public ComponentStateSignalInterface
{
public:
    /// component name
    static constexpr char COMPONENTNAME[] = "DynamicsSignal";

    DynamicsSignal() : ComponentStateSignalInterface{ComponentState::Disabled} {}
    /**
     * @brief Construct a new Dynamics Signal object
     * 
     * @param componentState State of the component
     */
    DynamicsSignal(ComponentState componentState) : ComponentStateSignalInterface{componentState}{}

    /**
     * @brief Construct a new Dynamics Signal object
     * 
     * @param componentState TODO
     * @param acceleration 
     * @param velocityX 
     * @param velocityY 
     * @param positionX 
     * @param positionY 
     * @param yaw 
     * @param yawRate 
     * @param yawAcceleration 
     * @param roll 
     * @param steeringWheelAngle 
     * @param centripetalAcceleration 
     * @param travelDistance 
     */
    DynamicsSignal(ComponentState componentState,
                  double acceleration,
                  double velocityX,
                  double velocityY,
                  double positionX,
                  double positionY,
                  double yaw,
                  double yawRate,
                  double yawAcceleration,
                  double roll,
                  double steeringWheelAngle,
                  double centripetalAcceleration,
                  double travelDistance) :
        ComponentStateSignalInterface{componentState},
        acceleration(acceleration),
        velocityX(velocityX),
        velocityY(velocityY),
        positionX(positionX),
        positionY(positionY),
        yaw(yaw),
        yawRate(yawRate),
        yawAcceleration(yawAcceleration),
        roll(roll),
        steeringWheelAngle(steeringWheelAngle),
        centripetalAcceleration(centripetalAcceleration),
        travelDistance(travelDistance)
    {}

    /// copy constructor
    /// @param other another dynamic signal
    DynamicsSignal(DynamicsSignal &other) = default;                
    DynamicsSignal(const DynamicsSignal&) = default;                ///< copy constructor
    DynamicsSignal(DynamicsSignal&&) = default;                     ///< move constructor

    /// @return copy assignment operator
    DynamicsSignal& operator=(const DynamicsSignal&) = default;

    /// @return move assignment operator
    DynamicsSignal& operator=(DynamicsSignal&&) = default;
    virtual ~DynamicsSignal() = default;

    //-----------------------------------------------------------------------------
    //! Returns the content/payload of the signal as an std::string
    //!
    //! @return                       Content/payload of the signal as an std::string
    //-----------------------------------------------------------------------------
    virtual operator std::string() const
    {
        std::ostringstream stream;
        stream << COMPONENTNAME << std::endl;
        stream << "acceleration: " << acceleration << std::endl;
        stream << "velocityX: " << velocityX << std::endl;
        stream << "velocityY: " << velocityY << std::endl;
        stream << "positionX: " << positionX << std::endl;
        stream << "positionY: " << positionY << std::endl;
        stream << "yaw: " << yaw << std::endl;
        stream << "yawRate: " << yawRate << std::endl;
        stream << "yawAcceleration" << yawAcceleration << std::endl;
        stream << "roll: " << roll << std::endl;
        stream << "steeringWheelAngle: " << steeringWheelAngle << std::endl;
        stream << "centripetalAcceleration: " << centripetalAcceleration << std::endl;
        stream << "travelDistance: " << travelDistance << std::endl;
        return stream.str();
    }
    /// acceleration
    double acceleration = 0.0;    
    /// velocity in X        
    double velocityX = 0.0;               
      /// velocity in Y
    double velocityY = 0.0;             
    /// position in X
    double positionX = 0.0;               
    /// position in Y
    double positionY = 0.0;               
    /// yaw
    double yaw = 0.0;                     
    /// yaw rate
    double yawRate = 0.0;                 
    /// yaw acceleration
    double yawAcceleration = 0.0;         
    /// roll
    double roll = 0.0;                    
    /// steering wheel angle
    double steeringWheelAngle = 0.0;      
    /// centripetal acceleration
    double centripetalAcceleration = 0.0; 
    /// travel distance
    double travelDistance = 0.0;          
};

