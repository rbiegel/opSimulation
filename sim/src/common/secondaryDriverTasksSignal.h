/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2018-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @brief This file contains all functions for class
//! SecondaryDriverTasksSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include "include/modelInterface.h"

class SecondaryDriverTasksSignal : public ComponentStateSignalInterface
{
public:
    /// constructor
    const std::string COMPONENTNAME = "SecondaryDriverTasksSignal";

    //-----------------------------------------------------------------------------
    //! Constructor
    //-----------------------------------------------------------------------------
    SecondaryDriverTasksSignal()
    {
        componentState = ComponentState::Disabled;
    }

    /**
     * @brief Construct a new Secondary Driver Tasks Signal object
     * 
     * @param indicatorState        State of the turning indicator (left, center/off, right)
     * @param hornSwitch            Switch for activation of horn
     * @param headLightSwitch       Switch for activation of headlight
     * @param highBeamLightSwitch   Switch for activation of Highbeamlight
     * @param flasher               Switch for activation of Flasher
     * @param componentState        State of the component
     */
    SecondaryDriverTasksSignal(
            int  indicatorState,
            bool hornSwitch,
            bool headLightSwitch,
            bool highBeamLightSwitch,
            bool flasher,
            ComponentState componentState
            ):
        indicatorState(indicatorState),
        hornSwitch(hornSwitch),
        headLightSwitch(headLightSwitch),
        highBeamLightSwitch(highBeamLightSwitch),
        flasherSwitch(flasher)
    {
        this->componentState = componentState;
    }

    /**
     * @brief Construct a new Secondary Driver Tasks Signal object
     * 
     * @param other Another secondary driver tasks signal
     */
    SecondaryDriverTasksSignal(SecondaryDriverTasksSignal &other) :
        SecondaryDriverTasksSignal(other.indicatorState,
                                                     other.hornSwitch,
                                                     other.headLightSwitch,
                                                     other.highBeamLightSwitch,
                                                     other.flasherSwitch,
                                                     other.componentState)

    {

    }

    SecondaryDriverTasksSignal(const SecondaryDriverTasksSignal&) = delete;
    SecondaryDriverTasksSignal(SecondaryDriverTasksSignal&&) = delete;
    SecondaryDriverTasksSignal& operator=(const SecondaryDriverTasksSignal&) = delete;
    SecondaryDriverTasksSignal& operator=(SecondaryDriverTasksSignal&&) = delete;

    virtual ~SecondaryDriverTasksSignal()
    {}

    //-----------------------------------------------------------------------------
    //! Returns the content/payload of the signal as an std::string
    //!
    //! @return     Content/payload of the signal as an std::string
    //-----------------------------------------------------------------------------
    virtual operator std::string() const
    {
        std::ostringstream stream;
        stream << COMPONENTNAME;
        return stream.str();
    }

    //! State of the turning indicator (left, center/off, right)
    int  indicatorState;
    //! Switch for activation of horn
    bool hornSwitch;
    //! Switch for activation of headlight
    bool headLightSwitch;
    //! Switch for activation of Highbeamlight
    bool highBeamLightSwitch;
    //! Switch for activation of Flasher
    bool flasherSwitch;
};
