/********************************************************************************
 * Copyright (c) 2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "version.h"

namespace openpass::common {

/// @brief RuntimeInformation
struct RuntimeInformation
{
    /// versions
    struct Versions
    {
        /// framework version
        Version framework;
    } versions; ///< framework versions

    /// directory informations
    struct Directories
    {
        /// configuration directory
        std::string configuration;
        /// output directory
        std::string output;
        /// library directory
        std::string library;
    } directories;  ///< directories information
};

} // namespace openpass::common
