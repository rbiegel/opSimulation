/********************************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  controlSignal.h
//! @brief This file contains signals
//!
//-----------------------------------------------------------------------------

#pragma once

#include "include/modelInterface.h"
#include "controlData.h"

//-----------------------------------------------------------------------------
//! Signal class containing the control data
//-----------------------------------------------------------------------------

class ControlSignal : public SignalInterface
{
public:
    /**
     * @brief Construct a new Control Signal object
     * 
     * @param inData control data
     */
    ControlSignal(const ControlData &inData);
    /**
     * @brief Construct from SignalVector by copy
     * 
     * @param inSignal control signal
     */
    ControlSignal(const ControlSignal &inSignal) = default;
    /**
     * @brief Construct from SignalVector by moving
     * 
     * @param inSignal control signal
     */
    ControlSignal(ControlSignal &&inSignal) = default;

    /// Assign from SignalVector by copy
    /// @param inSignal control signal
    /// @return control signal
    ControlSignal &operator=(const ControlSignal &inSignal) = default;
    /// Assign from SignalVector by moving
    /// @param inSignal control signal
    /// @return control signal
    ControlSignal &operator=(ControlSignal &&inSignal) = default;

    virtual ~ControlSignal() = default;

    /**
     * @brief Converts signal to string
     * 
     * @return returns signal as string
     */
    virtual operator std::string() const;

    /// control data contains throttle, steer and 4x brake signals
    ControlData value;
};
