/********************************************************************************
 * Copyright (c) 2018 AMFD GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  accelerationSignal.h
//! @brief This file contains all functions for class
//! AccelerationSignal
//!
//! This class contains all functionality of the module.
//-----------------------------------------------------------------------------

#pragma once

#include "include/modelInterface.h"

//-----------------------------------------------------------------------------
//! Signal class
//-----------------------------------------------------------------------------
class AccelerationSignal : public ComponentStateSignalInterface
{
public:

    /// COMPONENT NAME
    const std::string COMPONENTNAME = "AccelerationSignal";


    //-----------------------------------------------------------------------------
    //! Constructor
    //-----------------------------------------------------------------------------
    AccelerationSignal()
    {
        componentState = ComponentState::Disabled;
    }

    /**
     * @brief Construct a new Acceleration Signal object
     * 
     * @param other acceleration signals
     */
    AccelerationSignal(AccelerationSignal &other) :
        AccelerationSignal(other.componentState,
                      other.acceleration)

    {

    }

    /**
     * @brief Construct a new Acceleration Signal object
     * 
     * @param componentState state of the component
     * @param acceleration   acceleration
     */
    AccelerationSignal(ComponentState componentState,
                  double acceleration) :
        acceleration(acceleration)
    {
        this->componentState = componentState;
    }

    AccelerationSignal(const AccelerationSignal&) = delete;
    AccelerationSignal(AccelerationSignal&&) = delete;
    AccelerationSignal& operator=(const AccelerationSignal&) = delete;
    AccelerationSignal& operator=(AccelerationSignal&&) = delete;

    virtual ~AccelerationSignal()

    {}

    /**
     * Returns the content/payload of the signal as an std::string
     *
     * @return string Content/payload of the signal as an std::string
    */
    virtual operator std::string() const
    {
        std::ostringstream stream;
        stream << COMPONENTNAME;
        return stream.str();
    }

    /// acceleration 
    double acceleration;
};

