/********************************************************************************
 * Copyright (c) 2018-2019 AMFD GmbH
 *               2016-2017 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  lateralSignal.h
//! @brief This file contains all functions for class
//! LateralSignal
//!
//! This class contains all functionality of the signal.
//-----------------------------------------------------------------------------
#pragma once

#include "include/modelInterface.h"

/// class contains all functionality of the lateral signal
class LateralSignal: public ComponentStateSignalInterface
{
public:
    /// component name
    const std::string COMPONENTNAME = "LateralSignal";

    //-----------------------------------------------------------------------------
    //! Constructor
    //-----------------------------------------------------------------------------
    LateralSignal()
    {
        componentState = ComponentState::Disabled;
    }

    /**
     * @brief Construct a new Lateral Signal object
     * 
     * @param componentState                    State of the component
     * @param laneWidth                         Lateral net distance that needs to be covered
     * @param lateralDeviation                  Current lateral deviation regarding trajectory
     * @param gainLateralDeviation              Gain of lateral deviation controller
     * @param headingError                      Current heading error regarding trajectory
     * @param gainHeadingError                  Gain of the heading error controller
     * @param kappaManoeuvre                    Current curvature adjustment due to manoevre e.g. lane changing
     * @param kappaRoad                         Current curvature of road
     * @param curvatureOfSegmentsToNearPoint    Curvature at the middle of each segment from reference point to the near point
     * @param curvatureOfSegmentsToFarPoint     Curvature of segments between near point and far point
     */
    LateralSignal(
            ComponentState componentState,
            double laneWidth,
            double lateralDeviation,
            double gainLateralDeviation,
            double headingError,
            double gainHeadingError,
            double kappaManoeuvre,
            double kappaRoad,
            std::vector <double> curvatureOfSegmentsToNearPoint,
            std::vector <double> curvatureOfSegmentsToFarPoint
            ):
        laneWidth(laneWidth),
        lateralDeviation(lateralDeviation),
        gainLateralDeviation(gainLateralDeviation),
        headingError(headingError),
        gainHeadingError(gainHeadingError),
        kappaManoeuvre(kappaManoeuvre),
        kappaRoad(kappaRoad),
        curvatureOfSegmentsToNearPoint(curvatureOfSegmentsToNearPoint),
        curvatureOfSegmentsToFarPoint(curvatureOfSegmentsToFarPoint)
    {
        this->componentState = componentState;
    }

    virtual ~LateralSignal()
    {}

    //-----------------------------------------------------------------------------
    //! Returns the content/payload of the signal as an std::string
    //!
    //! @return                       Content/payload of the signal as an std::string
    //-----------------------------------------------------------------------------
    virtual operator std::string() const
    {
        std::ostringstream stream;
        stream << COMPONENTNAME;
        return stream.str();
    }

    /// lane width
    double laneWidth {0.};
    /// lateral deviation
    double lateralDeviation {0.};
    /// lateral deviation gain
    double gainLateralDeviation {20.0};
    /// heading error
    double headingError {0.};
    /// gain in heading error
    double gainHeadingError {7.5};
    /// kappa manoeuvre
    double kappaManoeuvre {0.};
    /// kappa value for the road
    double kappaRoad {0.};
    /// curvature of segments to near point
    std::vector <double> curvatureOfSegmentsToNearPoint {0.};
    /// curvature of segments to far point
    std::vector <double> curvatureOfSegmentsToFarPoint {0.};

    /**
     * @brief Construct a new Lateral Signal object
     * 
     * @param other another lateral signal
     */
    LateralSignal(LateralSignal &other) :
        LateralSignal(
            other.componentState,
            other.laneWidth,
            other.lateralDeviation,
            other.gainLateralDeviation,
            other.headingError,
            other.gainHeadingError,
            other.kappaManoeuvre
            )
    {
    }

    /**
     * @brief Construct a new Lateral Signal object
     * 
     * @param componentState        State of the component
     * @param laneWidth             Lateral net distance that needs to be covered
     * @param lateralDeviation      Current lateral deviation regarding trajectory
     * @param gainLateralDeviation  Gain of lateral deviation controller
     * @param headingError          Current heading error regarding trajectory
     * @param gainHeadingError      Gain of the heading error controller
     * @param kappaManoeuvre        Current curvature adjustment due to manoevre e.g. lane changing
     */
    LateralSignal(
            ComponentState componentState,
            double laneWidth,
            double lateralDeviation,
            double gainLateralDeviation,
            double headingError,
            double gainHeadingError,
            double kappaManoeuvre
            ):
        laneWidth(laneWidth),
        lateralDeviation(lateralDeviation),
        gainLateralDeviation(gainLateralDeviation),
        headingError(headingError),
        gainHeadingError(gainHeadingError),
        kappaManoeuvre(kappaManoeuvre)
    {
        this->componentState = componentState;
    }

    /*!
     * \brief Operator function to compare two signals
     *
     * @param other another lateral signal
     * @return this lateral signal
    */
    LateralSignal& operator=(const LateralSignal& other)
    {
        componentState = other.componentState;
        laneWidth = other.laneWidth;
        lateralDeviation = other.lateralDeviation;
        gainLateralDeviation = other.gainLateralDeviation;
        headingError = other.headingError;
        gainHeadingError = other.gainHeadingError;
        kappaManoeuvre = other.kappaManoeuvre;

        return *this;
    }
};
