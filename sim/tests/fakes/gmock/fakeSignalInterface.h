/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <gmock/gmock.h>
#include "include/signalInterface.h"

struct FakeSignalInterface : public SignalInterface {

    int localLinkId{};
    int time{};

    FakeSignalInterface() = default;
    FakeSignalInterface(int localLinkId, int time) : localLinkId(localLinkId), time(time) {};

    MOCK_CONST_METHOD0(BracketOperator, std::string());

    explicit operator std::string() const override
    {
        return BracketOperator();
    }
};
