/*******************************************************************************
* Copyright (c) 2020, 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
*******************************************************************************/

#pragma once

#include "include/fmuHandlerInterface.h"
#include "include/fmuWrapperInterface.h"
#include "include/signalInterface.h"

#include <gmock/gmock.h>

class FakeFmuWrapper : public FmuWrapperInterface
{
public:
    ~FakeFmuWrapper() override = default;

    MOCK_METHOD(void, Init, (), (override));
    MOCK_METHOD(void, UpdateInput, (int, const std::shared_ptr<const SignalInterface> &, int), (override));
    MOCK_METHOD(void, Trigger, (int), (override));
    MOCK_METHOD(void, UpdateOutput, (int, std::shared_ptr<const SignalInterface> &, int), (override));
    MOCK_METHOD(const FmuHandlerInterface *, GetFmuHandler, (), (const, override));
    MOCK_METHOD(const FmuVariables &, GetFmuVariables, (), (const, override));
    MOCK_METHOD(const FmuValue &, GetValue, (int, VariableType), (const override));
    MOCK_METHOD(void, SetValue, (const FmuValue &, int, VariableType), (override));
    MOCK_METHOD(int, getPriority, (), (const override));
    MOCK_METHOD(fmi_version_enu_t, getFmiVersion, (), (override));

    MOCK_METHOD(void, SetFmuValues, (std::vector<int> valueReferences, std::vector<FmuValue> fmuValuesIn, VariableType dataType), (override));
    MOCK_METHOD(void, GetFmuValues, (std::vector<int> valueReferences, std::vector<FmuValue>& fmuValuesOut, VariableType dataType), (override));
};
