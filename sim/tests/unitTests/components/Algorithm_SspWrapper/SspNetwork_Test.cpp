/********************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


#include "fakeParameter.h"
#include "fakeRadio.h"
#include "fakeWorld.h"
#include "fakeFmuWrapper.h"

#include "OSMPConnectorFactory.h"
#include "dontCare.h"
#include "fakeAgent.h"
#include "fakeCallback.h"
#include "fakeSignalInterface.h"

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/TriggerSignalVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateInputVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/UpdateOutputVisitor.h"
#include "sim/src/components/Algorithm_SspWrapper/Visitors/Connector/TriggerVisit.h"

#include "sim/src/components/Algorithm_SspWrapper/Visitors/Network/SspTriggerVisitor.h"

#include "sim/src/components/Algorithm_SspWrapper/SSPElements/System.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Component.h"
#include "sim/src/components/Algorithm_SspWrapper/SSPElements/SystemStructureDescription.h"

#include "sim/src/components/Algorithm_SspWrapper/SSPElements/Connector/OSMPConnector.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "SSPElements/Connector/GroupConnector.h"

using ::testing::_;
using ::testing::DontCare;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::Return;
using ::testing::ReturnRef;


class SSPWrapperNetworkTest : public ::testing::Test
{
public:
    SSPWrapperNetworkTest()
    {
        init();
    }
    void init()
    {
        OpenPassInterfaceData::setValues("SspWrapper",
                                          false,
                                          0,
                                          0,
                                          0,
                                          100,
                                          nullptr,
                                          &fakeWorld,
                                          &fakeParameter,
                                          nullptr,
                                          &fakeAgent,
                                          &fakeCallback);
    }
protected:
    NiceMock<FakeWorld> fakeWorld;
    NiceMock<FakeAgent> fakeAgent;
    NiceMock<FakeParameter> fakeParameter;
    NiceMock<FakeCallback> fakeCallback;
};


TEST(SspNetwork_Test, UpdateInput_EmptyComponents_DoesNothing)
{
    FakeCallback fakeCallback{};
    ssp::System rootSystem("root");
    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    ssp::UpdateInputSignalVisitor updateOutputSignalVisitor{0, signalInterface, 0};
    for(const auto& system : rootSystem.elements)
    {
        ssp::GroupConnector updateOutputSystemConnector {system->getOutputConnectors()};
        updateOutputSystemConnector.accept(updateOutputSignalVisitor);
    }
}

TEST(SspNetwork_Test, Trigger_EmptyComponents_DoesNothing)
{
    FakeCallback fakeCallback{};
    ssp::System rootSystem("root");
    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    ssp::SspTriggerVisitor triggerVisitor{0};
    for(const auto& system : rootSystem.elements)
    {
        system->accept(triggerVisitor);
    }
}

TEST(SspNetwork_Test, UpdateOutput_EmptyComponents_DoesNothing)
{
    FakeCallback fakeCallback{};
    ssp::System rootSystem("root");
    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    ssp::UpdateInputSignalVisitor updateOutputSignalVisitor{0, signalInterface, 0};
    for(const auto& system : rootSystem.elements)
    {
        ssp::GroupConnector updateOutputSystemConnector {system->getOutputConnectors()};
        updateOutputSystemConnector.accept(updateOutputSignalVisitor);
    }
}

TEST_F(SSPWrapperNetworkTest, UpdateInput_OneComponent_ExpectCallsToFmuInterface)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorData", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(inputConnectors), std::move(emptyConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    system->systemInputConnector->connectors.emplace_back(osmpTestConnector);

    EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
    EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(42), signalInterface, Eq(0))).Times(1);

    ssp::UpdateInputSignalVisitor updateInputSignalVisitor{42, signalInterface, 0};

    ssp::GroupConnector updateInputSystemConnector{system->getInputConnectors()};
    updateInputSystemConnector.accept(updateInputSignalVisitor);
}

TEST_F(SSPWrapperNetworkTest, UpdateInput_OneComponentUpdateInputCallesTwice_InitOnlyCalledOnce)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    ssp::System rootSystem("root");
    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorData", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(inputConnectors), std::move(emptyConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    system->systemInputConnector->connectors.emplace_back(osmpTestConnector);

    EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
    EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(42), signalInterface, Eq(0))).Times(2);

    ssp::UpdateInputSignalVisitor updateInputSignalVisitor{42, signalInterface, 0};
    ssp::GroupConnector updateInputSystemConnector{system->getInputConnectors()};
    updateInputSystemConnector.accept(updateInputSignalVisitor);
    updateInputSystemConnector.accept(updateInputSignalVisitor);
}

TEST_F(SSPWrapperNetworkTest, UpdateInput_NoConnector_ExpectNoUpdateInput)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    ssp::System rootSystem("root");
    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorData", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> inputConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_unique<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    EXPECT_CALL(*fmuWrapperInterface, Init()).Times(0);
    EXPECT_CALL(*fmuWrapperInterface, UpdateInput(Eq(0), Eq(nullptr), Eq(0))).Times(0);

    ssp::UpdateInputSignalVisitor updateInputSignalVisitor{0, signalInterface, 0};
    for (const auto &system : rootSystem.elements)
    {
        ssp::GroupConnector updateInputSystemConnector{system->getInputConnectors()};
        updateInputSystemConnector.accept(updateInputSignalVisitor);
    }
}

TEST_F(SSPWrapperNetworkTest, Trigger_OneComponent_ExpectCallsToFmuInterface_TriggerWithNoConnectors)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorData", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> outConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
    EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(0))).Times(1);

    ssp::SspTriggerVisitor triggerVisitor{0};
    system->accept(triggerVisitor);
}

TEST_F(SSPWrapperNetworkTest, Trigger_OneComponentTriggerCallsTwice_InitOnlyCalledOnce)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorData", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> outConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(emptyConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    // EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1); //ToDo: Oversaturating here, need to implement init visitor
    EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(0))).Times(1);
    EXPECT_CALL(*fmuWrapperInterface, Trigger(Eq(100))).Times(1);

    ssp::SspTriggerVisitor triggerVisitor{0};
    system->accept(triggerVisitor);

    ssp::SspTriggerVisitor triggerVisitor100{100};
    system->accept(triggerVisitor100);
}


TEST_F(SSPWrapperNetworkTest, UpdateOutput_OneComponent_ExpectCallsToFmuInterface)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorDataOut", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> outputConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(outputConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_unique<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    system->systemOutputConnector->connectors.emplace_back(osmpTestConnector);

    EXPECT_CALL(*fmuWrapperInterface, Init()).Times(1);
    EXPECT_CALL(*fmuWrapperInterface, UpdateOutput(Eq(6), signalInterface, Eq(0))).Times(1);

    ssp::UpdateOutputSignalVisitor updateOutputSignalVisitor{6, signalInterface, 0};
    ssp::GroupConnector updateOutputSystemConnector{system->getOutputConnectors()};
    updateOutputSystemConnector.accept(updateOutputSignalVisitor);

}

TEST_F(SSPWrapperNetworkTest, UpdateOutput_OneComponentUpdateOutputFmuWrapperSetsSignal_GetsSignal)
{
    FakeCallback fakeCallback{};
    auto fmuWrapperInterface = std::make_shared<FakeFmuWrapper>();

    fmi2_integer_t lo{}, hi{}, size{0};
    FmuVariables fmuVariables;
    fmuVariables.emplace<FMI2>();
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.lo"] = std::pair<fmi2_value_reference_t, VariableType>(0, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.base.hi"] = std::pair<fmi2_value_reference_t, VariableType>(1, VariableType::Int);
    std::get<FMI2>(fmuVariables)["SensorDataOut.size"] = std::pair<fmi2_value_reference_t, VariableType>(2, VariableType::Int);
    EXPECT_CALL(*fmuWrapperInterface, GetFmuVariables()).WillRepeatedly(ReturnRef(fmuVariables));

    auto baseLoValue = FmuHandlerInterface::FmuValue{.intValue = lo};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(0, VariableType::Int)).WillRepeatedly(ReturnRef(baseLoValue));
    auto baseHiValue = FmuHandlerInterface::FmuValue{.intValue = hi};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(1, VariableType::Int)).WillRepeatedly(ReturnRef(baseHiValue));
    auto sizeValue = FmuHandlerInterface::FmuValue{.intValue = size};
    EXPECT_CALL(*fmuWrapperInterface, GetValue(2, VariableType::Int)).WillRepeatedly(ReturnRef(sizeValue));

    std::shared_ptr<const SignalInterface> signalInterface = std::make_shared<const FakeSignalInterface>();
    auto osmpTestConnector = std::make_shared<ssp::OsmpConnector<osi3::SensorData>>("Connector", "SensorDataOut", fmuWrapperInterface, 10);

    std::shared_ptr<ssp::OSMPConnectorBase> sensorViewConnectorFMU1_in = osmpTestConnector;
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> emptyConnectors{};
    std::vector<std::shared_ptr<ssp::ConnectorInterface>> outputConnectors{osmpTestConnector};
    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> emptyElements;
    auto fmu1Test = std::make_shared<ssp::FmuComponent>("fmu1", std::move(emptyElements), std::move(emptyConnectors), std::move(outputConnectors), fmuWrapperInterface);

    std::vector<std::shared_ptr<ssp::VisitableNetworkElement>> elements{};
    elements.push_back(std::move(fmu1Test));
    auto system = std::make_shared<ssp::System>("system1", std::move(elements), std::make_shared<std::map<std::string, FmuFileHelper::TraceEntry>>());

    system->systemOutputConnector->connectors.emplace_back(osmpTestConnector);

    EXPECT_CALL(*fmuWrapperInterface, UpdateOutput(Eq(6), signalInterface, Eq(100))).Times(1);


    ON_CALL(*fmuWrapperInterface, UpdateOutput).WillByDefault([](int localLinkId, std::shared_ptr<const SignalInterface> &data, int time) mutable {
        data = std::make_shared<const FakeSignalInterface>(localLinkId, time);
    });

    ssp::UpdateOutputSignalVisitor updateOutputSignalVisitor{6, signalInterface, 100};

    ssp::GroupConnector updateOutputSystemConnector{system->getOutputConnectors()};
    updateOutputSystemConnector.accept(updateOutputSignalVisitor);


    ASSERT_TRUE(signalInterface);
    auto retrievedSignal = std::dynamic_pointer_cast<const FakeSignalInterface>(signalInterface);
    ASSERT_TRUE(retrievedSignal);
    ASSERT_EQ(6, retrievedSignal->localLinkId);
    ASSERT_EQ(100, retrievedSignal->time);
}

