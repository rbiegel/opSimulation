/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "FmuHandler.h"

#include "fakeCallback.h"
#include "fakeAgent.h"
#include "fakeRadio.h"
#include "fakeParameter.h"
#include "fakeWorld.h"
#include "fakeEgoAgent.h"
#include "OWL/fakes/fakeWorldData.h"


using ::testing::_;
using ::testing::DoubleEq;
using ::testing::Eq;
using ::testing::NiceMock;
using ::testing::NotNull;
using ::testing::Return;
using ::testing::ReturnArg;
using ::testing::ReturnRef;
using ::testing::AnyNumber;

std::shared_ptr<CallbackInterface> callback = std::make_shared<FakeCallback>();



template <size_t FMI>
class FmuHandlerPartialMock : FmuHandler<FMI2>
{
public:
    FmuHandlerPartialMock(std::string componentName,
                                fmu_check_data_t& cdata,
                                FmuVariables& fmuVariables,
                                std::map<ValueReferenceAndType, FmuValue> &fmuVariableValues,
                                const ParameterInterface *parameters,
                                WorldInterface *world,
                                AgentInterface *agent,
                                const CallbackInterface *callbacks) :
            FmuHandler<FMI>(componentName,
                          cdata,
                          fmuVariables,
                          fmuVariableValues,
                          parameters,
                          world,
                          agent,
                          callbacks)
    {

    }

    void PrepareInit()
    {
        FmuHandler<FMI>::PrepareInit();
    }
    void Init()
    {
        FmuHandler<FMI>::Init();
    }

    void ReadValues()
    {
        FmuHandler<FMI>::ReadValues();
    }

    void WriteValues()
    {
        FmuHandler<FMI>::WriteValues();
    }


    MOCK_METHOD(FmuVariables, GetFmuVariables, (), (override));

    MOCK_METHOD(void, SetFmuValue, (int, FmuValue, VariableType), (override));
    MOCK_METHOD(void, SetFmuValues, (std::vector<int>, std::vector<FmuValue>, VariableType), (override));
    MOCK_METHOD(void, GetFmuValue, (int, FmuValue&, VariableType), (override));
    MOCK_METHOD(void, GetFmuValues, (std::vector<int>, std::vector<FmuValue>&, VariableType), (override));

    MOCK_METHOD(jm_status_enu_t, PrepareFmuInit, (), (override));
    MOCK_METHOD(jm_status_enu_t, FmiEndHandling, (), (override));
    MOCK_METHOD(jm_status_enu_t, FmiSimulateStep, (double), (override));
    MOCK_METHOD(jm_status_enu_t, FmiPrepSimulate, (), (override));

    MOCK_METHOD(void, HandleFmiStatus, (jm_status_enu_t status, std::string logPrefix), (override));
};



class FmuWriteAndReadValuesTests : public ::testing::Test
{
public:
    FmuWriteAndReadValuesTests()
    {
        ON_CALL(fakeParameter, GetParametersString()).WillByDefault(ReturnRef(stringParameter));
        ON_CALL(fakeParameter, GetParametersBool()).WillByDefault(ReturnRef(boolParameter));
        ON_CALL(fakeParameter, GetParametersInt()).WillByDefault(ReturnRef(intParameter));
        ON_CALL(fakeParameter, GetParametersDouble()).WillByDefault(ReturnRef(doubleParameter));
        ON_CALL(fakeParameter, GetParameterLists()).WillByDefault(ReturnRef(parameterLists));

        ON_CALL(fakeWorld, GetWorldData()).WillByDefault(Return(&fakeWorldData));
        ON_CALL(fakeAgent, GetEgoAgent()).WillByDefault(ReturnRef(fakeEgoAgent));

        InitFmu();
    }

    void fillParameters()
    {
        intParameter.insert(std::make_pair("Parameter_ParameterConstant", 0));
        intParameter.insert(std::make_pair("Parameter_ParameterFixed", 1));
        intParameter.insert(std::make_pair("Parameter_ParameterTunable", 2));
    }

    void fillVariables()
    {
        FmuVariable2 fmuVarParaConst(0, VariableType::Int, fmi2_causality_enu_parameter, fmi2_variability_enu_constant);
        FmuVariable2 fmuVarParaFixed(1, VariableType::Int, fmi2_causality_enu_parameter, fmi2_variability_enu_fixed);
        FmuVariable2 fmuVarParaTunable(2, VariableType::Int, fmi2_causality_enu_parameter, fmi2_variability_enu_tunable);

        FmuVariable2 fmuVarOutConst(3, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_constant);
        FmuVariable2 fmuVarOutFixed(4, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_fixed);
        FmuVariable2 fmuVarOutContinuous(5, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_continuous);
        FmuVariable2 fmuVarOutDiscrete(6, VariableType::Int, fmi2_causality_enu_output, fmi2_variability_enu_discrete);

        FmuVariable2 fmuVarCalcParaConst(7, VariableType::Int, fmi2_causality_enu_calculated_parameter , fmi2_variability_enu_constant);
        FmuVariable2 fmuVarCalcParaFixed(8, VariableType::Int, fmi2_causality_enu_calculated_parameter, fmi2_variability_enu_fixed);
        FmuVariable2 fmuVarCalcParaTunable(9, VariableType::Int, fmi2_causality_enu_calculated_parameter, fmi2_variability_enu_tunable);

        FmuVariable2 fmuVarInConst(10, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_constant);
        FmuVariable2 fmuVarInFixed(11, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_fixed);
        FmuVariable2 fmuVarInContinuous(12, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_continuous);
        FmuVariable2 fmuVarInDiscrete(13, VariableType::Int, fmi2_causality_enu_input, fmi2_variability_enu_discrete);

        std::vector<std::pair<std::string, FmuVariable2>> fmuVariables2;

        fmuVariables2.push_back(std::make_pair("ParameterConstant", fmuVarParaConst));
        fmuVariables2.push_back(std::make_pair("ParameterFixed", fmuVarParaFixed));
        fmuVariables2.push_back(std::make_pair("ParameterTunable", fmuVarParaTunable));

        fmuVariables2.push_back(std::make_pair("InputConstant", fmuVarOutConst));
        fmuVariables2.push_back(std::make_pair("InputFixed", fmuVarOutFixed));
        fmuVariables2.push_back(std::make_pair("InputTunable", fmuVarOutContinuous));
        fmuVariables2.push_back(std::make_pair("Inputunable", fmuVarOutDiscrete));

        fmuVariables2.push_back(std::make_pair("CalculatedParameterConstant", fmuVarCalcParaConst));
        fmuVariables2.push_back(std::make_pair("CalculatedParameterFixed", fmuVarCalcParaFixed));
        fmuVariables2.push_back(std::make_pair("CalculatedParameterTunable", fmuVarCalcParaTunable));

        fmuVariables2.push_back(std::make_pair("OutputConstant", fmuVarInConst));
        fmuVariables2.push_back(std::make_pair("OutputFixed", fmuVarInFixed));
        fmuVariables2.push_back(std::make_pair("OutputTunable", fmuVarInContinuous));
        fmuVariables2.push_back(std::make_pair("OutputDiscrete", fmuVarInDiscrete));

        FmuVariables fmuVariables = std::unordered_map<std::string, FmuVariable2>(fmuVariables2.begin(), fmuVariables2.end());
        this->fmuVariables = std::make_shared<FmuVariables>(fmuVariables);
    }

    void fillVariableValues()
    {
        ValueReferenceAndType valueReferenceAndType0 = std::make_pair(0, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType1 = std::make_pair(1, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType2 = std::make_pair(2, VariableType::Int);

        ValueReferenceAndType valueReferenceAndType3 = std::make_pair(3, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType4 = std::make_pair(4, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType5 = std::make_pair(5, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType6 = std::make_pair(6, VariableType::Int);

        ValueReferenceAndType valueReferenceAndType7 = std::make_pair(7, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType8 = std::make_pair(8, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType9 = std::make_pair(9, VariableType::Int);

        ValueReferenceAndType valueReferenceAndType10 = std::make_pair(10, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType11 = std::make_pair(11, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType12 = std::make_pair(12, VariableType::Int);
        ValueReferenceAndType valueReferenceAndType13 = std::make_pair(13, VariableType::Int);

        FmuValue fmuValue0;
        fmuValue0.intValue = 0;
        FmuValue fmuValue1;
        fmuValue0.intValue = 1;
        FmuValue fmuValue2;
        fmuValue0.intValue = 2;

        FmuValue fmuValue3;
        fmuValue3.intValue = 3;
        FmuValue fmuValue4;
        fmuValue4.intValue = 4;
        FmuValue fmuValue5;
        fmuValue5.intValue = 5;
        FmuValue fmuValue6;
        fmuValue6.intValue = 6;

        FmuValue fmuValue7;
        fmuValue7.intValue = 7;
        FmuValue fmuValue8;
        fmuValue8.intValue = 8;
        FmuValue fmuValue9;
        fmuValue9.intValue = 9;

        FmuValue fmuValue10;
        fmuValue10.intValue = 10;
        FmuValue fmuValue11;
        fmuValue11.intValue = 11;
        FmuValue fmuValue12;
        fmuValue12.intValue = 12;
        FmuValue fmuValue13;
        fmuValue13.intValue = 13;

        std::map<ValueReferenceAndType, FmuValue> fmuVariableValues;

        fmuVariableValues.insert(std::make_pair(valueReferenceAndType0, fmuValue0));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType1, fmuValue1));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType2, fmuValue2));

        fmuVariableValues.insert(std::make_pair(valueReferenceAndType3, fmuValue3));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType4, fmuValue4));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType5, fmuValue5));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType6, fmuValue6));

        fmuVariableValues.insert(std::make_pair(valueReferenceAndType7, fmuValue7));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType8, fmuValue8));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType9, fmuValue9));

        fmuVariableValues.insert(std::make_pair(valueReferenceAndType10, fmuValue10));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType11, fmuValue11));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType12, fmuValue12));
        fmuVariableValues.insert(std::make_pair(valueReferenceAndType13, fmuValue13));

        this->fmuVariableValues = std::make_shared<std::map<ValueReferenceAndType, FmuValue>>(fmuVariableValues);
    }

    void InitFmu()
    {
        fmuCheckData = std::make_shared<fmu_check_data_t>();
        fillVariables();
        fillVariableValues();
        fillParameters();

        fmuHandlerPartialMock = std::make_unique<FmuHandlerPartialMock<FMI2>>(
                "FmuHandler",
                *fmuCheckData,
                *fmuVariables,
                *fmuVariableValues,
                &fakeParameter,
                &fakeWorld,
                &fakeAgent,
                &fakeCallback);

        ON_CALL(*fmuHandlerPartialMock, GetFmuVariables).WillByDefault(Return(*fmuVariables));
        ON_CALL(*fmuHandlerPartialMock, PrepareFmuInit()).WillByDefault(Return(jm_status_success));
        ON_CALL(*fmuHandlerPartialMock, FmiEndHandling()).WillByDefault(Return(jm_status_success));
        ON_CALL(*fmuHandlerPartialMock, FmiSimulateStep(_)).WillByDefault(Return(jm_status_success));
        ON_CALL(*fmuHandlerPartialMock, FmiPrepSimulate()).WillByDefault(Return(jm_status_success));

        fmuHandlerPartialMock->PrepareInit();

    }

protected:
    std::unique_ptr<FmuHandler<FMI2>> fmuHandler;
    std::shared_ptr<FmuVariables> fmuVariables;
    std::shared_ptr<std::map<ValueReferenceAndType, FmuValue>> fmuVariableValues;
    std::shared_ptr<fmu_check_data_t> fmuCheckData;

    NiceMock<FakeWorld> fakeWorld;
    NiceMock<OWL::Fakes::WorldData> fakeWorldData;
    NiceMock<FakeAgent> fakeAgent;
    NiceMock<FakeEgoAgent> fakeEgoAgent;
    NiceMock<FakeCallback> fakeCallback;
    NiceMock<FakeParameter> fakeParameter;

    std::unique_ptr<FmuHandlerPartialMock<FMI2>> fmuHandlerPartialMock;

    std::map<std::string, const std::string> stringParameter{};
    std::map<std::string, bool> boolParameter{};
    std::map<std::string, int> intParameter{};
    std::map<std::string, double> doubleParameter{};
    std::map<std::string, FakeParameter::ParameterLists> parameterLists{};
};



TEST_F(FmuWriteAndReadValuesTests, TestWriteParameters)
{
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(0,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(1,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(2,_,_)).Times(1);
    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();

    fmuHandlerPartialMock->Init();

    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(0,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(1,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(2,_,_)).Times(1);

    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestReadCalculatedParameters)
{
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(7,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(8,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(9,_,_)).Times(1);
    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();

    fmuHandlerPartialMock->Init();

    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(7,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(8,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(9,_,_)).Times(1);

    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestWriteInputs)
{
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(10,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(11,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(12,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(13,_,_)).Times(1);
    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();

    fmuHandlerPartialMock->Init();

    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(10,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(11,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(12,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, SetFmuValue(13,_,_)).Times(1);

    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();
}

TEST_F(FmuWriteAndReadValuesTests, TestReadOuputs)
{
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(3,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(4,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(5,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(6,_,_)).Times(1);
    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();

    fmuHandlerPartialMock->Init();

    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(_,_,_)).Times(AnyNumber());
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(3,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(4,_,_)).Times(0);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(5,_,_)).Times(1);
    EXPECT_CALL(*fmuHandlerPartialMock, GetFmuValue(6,_,_)).Times(1);

    fmuHandlerPartialMock->ReadValues();
    fmuHandlerPartialMock->WriteValues();
}