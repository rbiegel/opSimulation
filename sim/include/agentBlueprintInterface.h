/********************************************************************************
 * Copyright (c) 2016 ITK Engineering GmbH
 *               2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  agentBlueprintInterface.h
//! @brief This file contains the interface of the agent blueprint to interact
//!        with the framework.
//-----------------------------------------------------------------------------

#include <string>
#include <unordered_map>

#include "common/globalDefinitions.h"
#include "common/worldDefinitions.h"

#include "include/agentTypeInterface.h"
#include "include/profilesInterface.h"

#pragma once

//! Container for vehicle components and their possible profiles
//! The key is the VehicleComponent type and the value is the name of the component profile
using VehicleComponentProfileNames = std::unordered_map<std::string, std::string>;

//! Route from OpenSCENARIO converted into graph
struct Route
{
    RoadGraph roadGraph;       //!< Directed graph representing the road network
    RoadGraphVertex root;      //!< Root of the roadGraph
    RoadGraphVertex target;    //!< Target in the roadGraph
};

/// struct represents paramters of the spawn object
struct SpawnParameter
{
    double positionX = -999;        ///< X coordinate
    double positionY = -999;        ///< y coordinate
    double velocity = -999;         ///< velocity of the spawned object
    double acceleration = -999;     ///< acceleration of the spawned object
    double gear = -999;             ///< gear of the spawned object
    double yawAngle = -999;         ///< yaw angle of the spawned object
    Route route{};                  ///< Route of the spawned object
};

//-----------------------------------------------------------------------------
//! Class representing a agent blue print interface
//-----------------------------------------------------------------------------
class AgentBlueprintInterface
{
public:
    AgentBlueprintInterface() = default;
    virtual ~AgentBlueprintInterface() = default;

    /**
     * @brief Set vehicle component profile names
     * 
     * @param[in] vehicleComponentProfileName Profile name of the vehicle component
     */
    virtual void SetVehicleComponentProfileNames(VehicleComponentProfileNames vehicleComponentProfileName) = 0;

    /**
     * @brief Set Agent category
     * 
     * @param[in] agentCategory Category of the agent
     */
    virtual void SetAgentCategory(AgentCategory agentCategory) = 0;

    /**
     * @brief Set profile name of the agent
     * 
     * @param[in] agentTypeName Profile name of the agent
     */
    virtual void SetAgentProfileName(std::string agentTypeName) = 0;

    /**
     * @brief Set profile name of the vehicle
     * 
     * @param[in] vehicleModelName profile name of the vehicle
     */
    virtual void SetVehicleProfileName(std::string vehicleModelName) = 0;

    /**
     * @brief Set model name of the vehicle
     * 
     * @param[in] vehicleModelName Model name of the vehicle
     */
    virtual void SetVehicleModelName(std::string vehicleModelName) = 0;

    /**
     * @brief Set Model parameters of the vehicle
     * 
     * @param[in] vehicleModelParameters Parameters of the vehicle model
     */
    virtual void SetVehicleModelParameters(VehicleModelParameters vehicleModelParameters) = 0;

    /**
     * @brief Set Profile Name of the driver
     * 
     * @param[in] driverProfileName Profile Name of the driver
     */
    virtual void SetDriverProfileName(std::string driverProfileName) = 0;

    /**
     * @brief Set spawn parameter
     * 
     * @param[in] spawnParameter spawn parameter
     */
    virtual void SetSpawnParameter(SpawnParameter spawnParameter) = 0;

    /**
     * @brief Set minimum speed goal
     * 
     * @param[in] speedGoalMin minimum speed goal
     */
    virtual void SetSpeedGoalMin(double speedGoalMin) = 0;

    /**
     * @brief Set type of the agent
     * 
     * @param[in] agentType Type of the agent
     */
    virtual void SetAgentType(std::shared_ptr<core::AgentTypeInterface> agentType) = 0;

    /**
     * @brief Add sensor to the agent
     * 
     * @param[in] parameters Parameters of the sensor to be added
     */
    virtual void AddSensor(openpass::sensors::Parameter parameters) = 0;

    /**
     * @return Get category of the agent
    */
    virtual AgentCategory                           GetAgentCategory() const = 0;

    /// @return Get profile name of the agent
    virtual std::string                             GetAgentProfileName() const = 0;

    /// @return Get profile name of the Vehicle
    virtual std::string                             GetVehicleProfileName() const = 0;

    /// @return Get Model name of the vehicle
    virtual std::string                             GetVehicleModelName() const = 0;

    /// @return Get profile name of the driver
    virtual std::string                             GetDriverProfileName() const = 0;

    /// @return Get object name
    virtual std::string                             GetObjectName() const = 0;

    /// @return Get parameters of the vehicle model
    virtual VehicleModelParameters                  GetVehicleModelParameters() const = 0;

    /// @return Get parameters of the sensor
    virtual openpass::sensors::Parameters           GetSensorParameters() const = 0;

    /// @return Get profile names of the vehicle component
    virtual VehicleComponentProfileNames            GetVehicleComponentProfileNames() const = 0;

    /// @return Get type of the agent interface
    virtual core::AgentTypeInterface&    GetAgentType() const = 0;

    /// @return Get vehicle spawn parameters
    virtual SpawnParameter&                         GetSpawnParameter() = 0;

    /// @return Get vehicle spawn parameters
    virtual const SpawnParameter&                   GetSpawnParameter() const = 0;

    /// @return Get minimum speed goal
    virtual double                                  GetSpeedGoalMin() const = 0;

    /**
     * @brief Set object name
     * 
     * @param[in] objectName object name
    */
    virtual void                                    SetObjectName(std::string objectName) = 0;
};


