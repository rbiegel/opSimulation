/********************************************************************************
 * Copyright (c) 2016-2018 ITK Engineering GmbH
 *               2017-2019 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <array>
#include <map>
#include <string>
#include <iostream>

enum class ComponentState
{
    Undefined = 0,
    Disabled,
    Armed,
    Acting
};

enum class MovementDomain
{
    Undefined = 0,
    Lateral,
    Longitudinal,
    Both
};

namespace openpass::utils {

/// @brief constexpr map for transforming the a corresponding enumeration into
///        a string representation: try to_cstr(EnumType) or to_string(EnumType)
static constexpr std::array<const char *, 4> ComponentStateMapping{
    "Undefined",
    "Disabled",
    "Armed",
    "Acting"};

constexpr const char *to_cstr(ComponentState state)
{
    return ComponentStateMapping[static_cast<size_t>(state)];
}

inline std::string to_string(ComponentState state) noexcept
{
    return std::string(to_cstr(state));
}

} // namespace openpass::utils

const std::map<std::string, ComponentState> ComponentStateMapping = {{"Acting", ComponentState::Acting},
                                                                     {"Armed", ComponentState::Armed},
                                                                     {"Disabled", ComponentState::Disabled}};

//-----------------------------------------------------------------------------
//! This interface provides access to common signal operations
//-----------------------------------------------------------------------------
class SignalInterface
{
public:
    SignalInterface() = default;

    /// @brief default constructor
    SignalInterface(const SignalInterface &) = default;

    /// @brief default constructor
    SignalInterface(SignalInterface &&) = default;

    /// @brief Operator= overloading
    /// @return signal interface
    SignalInterface &operator=(const SignalInterface &) = default;

    /// @brief Operator= overloading
    /// @return signal interface
    SignalInterface &operator=(SignalInterface &&) = default;
    virtual ~SignalInterface() = default;

    //-----------------------------------------------------------------------------
    //! @return String conversion of signal
    //-----------------------------------------------------------------------------
    explicit virtual operator std::string() const = 0;

private:
    friend std::ostream& operator<<(std::ostream&, const SignalInterface&);
};

//-----------------------------------------------------------------------------
//! Provides functionality to print information of signals
//!
//! @param[in]     stream  Output stream for printing information
//! @param[in]     signal  Signal to be printed
//! @return                Output stream for concatenation
//-----------------------------------------------------------------------------
inline std::ostream& operator<<(std::ostream& stream, const SignalInterface& signal)
{
    return stream << static_cast<std::string>(signal);
}

//-----------------------------------------------------------------------------
//! This interface provides access to a component's state
//-----------------------------------------------------------------------------
class ComponentStateSignalInterface : public SignalInterface
{
public:
    ComponentStateSignalInterface() = default;

    /// @brief Create an component state signal interface object
    /// @param componentState state of the component
    ComponentStateSignalInterface(ComponentState componentState) :
        componentState{componentState}
    {
    }

    /// @brief default constructor
    ComponentStateSignalInterface(const ComponentStateSignalInterface &) = default;
    
    /// @brief default constructor
    ComponentStateSignalInterface(ComponentStateSignalInterface &&) = default;

    /// @brief Operator= overloading
    /// @return component state signal interface
    ComponentStateSignalInterface &operator=(const ComponentStateSignalInterface &) = default;

    /// @brief Operator= overloading
    /// @return component state signal interface
    ComponentStateSignalInterface &operator=(ComponentStateSignalInterface &&) = default;
    virtual ~ComponentStateSignalInterface() = default;

    ComponentState componentState;  ///< state of the component
};
