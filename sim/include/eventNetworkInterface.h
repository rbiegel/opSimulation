/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  eventNetworkInterface.h
*	\brief This file provides the interface for the EventNetwork
*/
//-----------------------------------------------------------------------------

#pragma once

#include "include/eventInterface.h"
#include "include/runResultInterface.h"

/// Shared event as a pointer to the event interface
using SharedEvent = std::shared_ptr<EventInterface>;

/// event container as the vector of shared event
using EventContainer = std::vector<SharedEvent>;

namespace core {

//-----------------------------------------------------------------------------
/** \brief This class provides the interface for the EventNetwork
*
* 	\ingroup EventNetwork */
//-----------------------------------------------------------------------------
class EventNetworkInterface
{
public:
    virtual ~EventNetworkInterface() = default;

    /// Function to get events
    /// @param eventCategory Category of an event
    /// @return Returns event container as the vector of shared event
    virtual EventContainer GetEvents(const EventDefinitions::EventCategory eventCategory) const = 0;
    
    /// Insert a shared event to the event network interface
    /// @param event Shared event as a pointer to the event interface
    virtual void InsertEvent(SharedEvent event) = 0;

    /// Clears all EventNetworkInterface.
    virtual void Clear() = 0;

    //-----------------------------------------------------------------------------
    //! Adds a collision to a list of collisions with information about the collided
    //! agents.
    //!
    //! @param[in] agentId     Id of the collided agent
    //-----------------------------------------------------------------------------
    virtual void AddCollision(const int agentId) = 0;

    /*!
    * \brief Initalizes the EventNetwork
    *
    * \details Initalizes the EventNetwork.
    *
    *
    * @param[in]     runResult    Pointer to the runResult.
    */
    virtual void Initialize(RunResultInterface *runResult) = 0;

    /// \brief Add a trigger, which is being relayed to the components
    /// \param identifier for identification of the specific trigger
    /// \param event      The according event, which is being sent
    virtual void InsertTrigger(const std::string &identifier, std::unique_ptr<EventInterface> event) = 0;
    
    /// \brief Returns a collection of active trigger
    /// \param identifier for identification of the specific triggers
    /// \return Collection of active trigger
    virtual std::vector<EventInterface const *> GetTrigger(const std::string &identifier) const = 0;
};

} //namespace core
