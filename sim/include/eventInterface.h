/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** \file  eventInterface.h
 *	\brief This file provides the interface for the events
 */
//-----------------------------------------------------------------------------
#pragma once

#include <vector>

#include "common/eventTypes.h"
#include "common/openPassTypes.h"

/// @brief Parameters of an event
struct EventParameter
{
    std::string key;      ///< TODO
    std::string value;    ///< TODO
};

/// List of event parameter as event parameters
using EventParameters = std::vector<EventParameter>;

//-----------------------------------------------------------------------------
/** \brief This interface provides access to common event operations
*
* \ingroup Event
*/
//-----------------------------------------------------------------------------
class EventInterface
{
public:
    EventInterface() = default;
    
    /**
     * @brief Construct a new Event Interface object
     * 
     * @param triggeringAgents Triggering entities
     * @param actingAgents     Acting agents
     */
    EventInterface(const openpass::type::TriggeringEntities triggeringAgents, const openpass::type::AffectedEntities actingAgents) :
        triggeringAgents{std::move(triggeringAgents)},
        actingAgents{std::move(actingAgents)}{}

    /// @brief default constructor
    EventInterface(const EventInterface &) = default;
    
    /// @brief default constructor
    EventInterface(EventInterface &&) = default;

    /// @brief operator= overloading
    /// @param rhs event interface
    /// @return event interface
    EventInterface &operator=(const EventInterface & rhs) = default;

    /// @brief operator= overloading
    /// @return event interface
    EventInterface &operator=(EventInterface &&) = default;
    virtual ~EventInterface() = default;

    /*!
    * \brief Returns the time of the event.
    * \return	     Time in milliseconds.
    */
    virtual int GetEventTime() const = 0;

    /*!
    * \brief Returns the category of the event.
    * \return	     EventCategory.
    */
    virtual EventDefinitions::EventCategory GetCategory() const = 0;

    /*!
    * \brief Returns the name of the event.
    * \return	     Name of the event as string.
    */
    virtual const std::string& GetName() const = 0;

    /*!
    * \brief Returns the name of the source component.
    * \return	     Name of source component as string.
    */
    virtual const std::string& GetSource() const = 0;

    /*!
     * \brief GetTriggeringAgents
     * \return  List of triggering agents (might me empty)
     */
    virtual const openpass::type::TriggeringEntities& GetTriggeringAgents() const = 0;

    /*!
     * \brief GetActingAgents
     * \return  List of acting agents (might me empty)
     */
    virtual const openpass::type::AffectedEntities& GetActingAgents() const = 0;

    /*!
     * \brief Returns all parameter in their raw form
     * \see openpass::events::Parameter
     * \return Parameter
     */
    virtual const openpass::type::FlatParameter& GetParameter() const = 0;

public:
    //! All agents which triggered the event
    openpass::type::TriggeringEntities triggeringAgents {};

    //! All agents which are affected by the event
    openpass::type::AffectedEntities actingAgents {};
};
