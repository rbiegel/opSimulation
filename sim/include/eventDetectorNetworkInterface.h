/********************************************************************************
 * Copyright (c) 2017-2019 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "include/eventNetworkInterface.h"
#include "include/scenarioInterface.h"
#include "include/stochasticsInterface.h"

namespace core
{

//Forward declarations
class EventDetector;

/// @brief Interface representing network of event detectors
class EventDetectorNetworkInterface
{
public:
    EventDetectorNetworkInterface() = default;
    ~EventDetectorNetworkInterface() = default;

    /// @brief Instantiate a Event detector network interface
    /// @param libraryPath  Path to all libraries
    /// @param scenario     Reference to scenario interface
    /// @param eventNetwork Reference to event network
    /// @param stochastics  Reference to Stochastics interface
    /// @return Returns true when an instatiation is successfull
    virtual bool Instantiate(const std::string libraryPath,
                     const ScenarioInterface *scenario,
                     EventNetworkInterface* eventNetwork,
                     StochasticsInterface *stochastics) = 0;

    //-----------------------------------------------------------------------------
    //! Clears the modules mapping and deletes the EventDetectorNetworkInterface module instances
    //! within and also unloads the EventDetectorNetworkInterface bindings.
    //-----------------------------------------------------------------------------
    virtual void Clear() = 0;

    /// @return Returns the list of references to all event detectors
    virtual const std::vector<const EventDetector*> GetEventDetectors() const = 0;

    /// Reset all event detector network
    virtual void ResetAll() = 0;
};

}// namespace core


