/********************************************************************************
 * Copyright (c) 2017-2021 in-tech GmbH
 *               2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once

#include <string>
#include "roadElementTypes.h"

//-----------------------------------------------------------------------------
//! Class representing a road object interface
//-----------------------------------------------------------------------------
class RoadObjectInterface
{
public:
    RoadObjectInterface() = default;
    RoadObjectInterface(const RoadObjectInterface&) = delete;
    RoadObjectInterface(RoadObjectInterface&&) = delete;
    RoadObjectInterface& operator=(const RoadObjectInterface&) = delete;
    RoadObjectInterface& operator=(RoadObjectInterface&&) = delete;
    virtual ~RoadObjectInterface() = default;

    //-----------------------------------------------------------------------------
    //! @brief Returns the type of the road object
    //! @return                     type
    //-----------------------------------------------------------------------------
    virtual RoadObjectType GetType() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the unique identification string of the road object
    //! @return                     id
    //-----------------------------------------------------------------------------
    virtual std::string GetId() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the s coordinate of the road object
    //! @return                     s coordinate [m]
    //-----------------------------------------------------------------------------
    virtual double GetS() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the t coordinate of the road object
    //! @return                     t coordinate [m]
    //-----------------------------------------------------------------------------
    virtual double GetT() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the zOffset of the road object
    //! @return                     zOffset [m]
    //-----------------------------------------------------------------------------
    virtual double GetZOffset() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Check, if road object is valid for provided lane id
    //!
    //! @param[in]  laneId          lane id to test validity for
    //!
    //! @return                     True if valid, false otherwise.
    //-----------------------------------------------------------------------------
    virtual bool IsValidForLane(int laneId) const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the length of the road object
    //! @return                     length [m]
    //-----------------------------------------------------------------------------
    virtual double GetLength() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the width of the road object
    //! @return                     width [m]
    //-----------------------------------------------------------------------------
    virtual double GetWidth() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the height of the road object
    //! @return                     height [m]
    //-----------------------------------------------------------------------------
    virtual double GetHeight() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the heading of the road object (relative to road direction)
    //! @return                     heading [rad]
    //-----------------------------------------------------------------------------
    virtual double GetHdg() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the pitch of the road object
    //! @return                     pitch [rad]
    //-----------------------------------------------------------------------------
    virtual double GetPitch() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the roll of the road object
    //! @return                     roll [rad]
    //-----------------------------------------------------------------------------
    virtual double GetRoll() const = 0;

    //-----------------------------------------------------------------------------
    //! @return Returns wether this is a continuous object (e.g. guard rail)
    //-----------------------------------------------------------------------------
    virtual bool IsContinuous() const = 0;

    //-----------------------------------------------------------------------------
    //! @brief Returns the name of the road object
    //! @return                     name
    //-----------------------------------------------------------------------------
    virtual std::string GetName() const = 0;
};



