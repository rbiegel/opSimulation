/********************************************************************************
 * Copyright (c) 2018-2020 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
#pragma once


enum class RoadLaneRoadMarkType;
enum class RoadLaneRoadMarkColor;
enum class RoadLaneRoadMarkLaneChange;
enum class RoadLaneRoadDescriptionType;
enum class RoadLaneRoadMarkWeight;

/// @brief Class representing a road lane road mark
class RoadLaneRoadMark
{
public:
    /**
     * @brief Construct a new Road Lane Road Mark object
     * 
     * @param sOffset           s offset
     * @param descriptionType   TODO
     * @param type 
     * @param color 
     * @param laneChange 
     * @param weight 
     */
    RoadLaneRoadMark(double sOffset,
                     RoadLaneRoadDescriptionType descriptionType,
                     RoadLaneRoadMarkType type,
                     RoadLaneRoadMarkColor color,
                     RoadLaneRoadMarkLaneChange laneChange,
                     RoadLaneRoadMarkWeight weight) :
        sOffset(sOffset),
        type(type),
        color(color),
        laneChange(laneChange),
        descriptionType(descriptionType),
        weight(weight)
    {
    }

    /**
     * @brief Get the Type object TODO
     * 
     * @return RoadLaneRoadMarkType 
     */
    RoadLaneRoadMarkType GetType() const
    {
        return type;
    }
    
    /**
     * @brief Get s offset
     * 
     * @return s offset
     */
    double GetSOffset() const
    {
        return sOffset;
    }

    /**
     * @brief get s end
     * 
     * @return s end
     */
    double GetSEnd() const
    {
        return  sEnd;
    }

    /**
     * @brief Get road mark Color 
     * 
     * @return Road mark Color 
     */
    RoadLaneRoadMarkColor GetColor() const
    {
        return color;
    }

    /**
     * @brief Get the Weight TODO
     * 
     * @return RoadLaneRoadMarkWeight 
     */
    RoadLaneRoadMarkWeight GetWeight() const
    {
        return  weight;
    }

    /**
     * @brief Get the Lane Change 
     * 
     * @return Road mark Lane Change 
     */
    RoadLaneRoadMarkLaneChange GetLaneChange() const
    {
        return laneChange;
    }

    /**
     * @brief Get the Description Type 
     * 
     * @return Road mark description
     */
    RoadLaneRoadDescriptionType GetDescriptionType() const
    {
        return descriptionType;
    }

    /**
     * @brief limit the end of the lane in s coordinate
     * 
     * @param limit limit TODO
     */
    void LimitSEnd (double limit)
    {
        sEnd = std::min(sEnd, limit);
    }


private:

double sOffset;
double sEnd = std::numeric_limits<double>::max();
RoadLaneRoadMarkType type;
RoadLaneRoadMarkColor color;
RoadLaneRoadMarkLaneChange laneChange;
RoadLaneRoadDescriptionType descriptionType;
RoadLaneRoadMarkWeight weight;

};
