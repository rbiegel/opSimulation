/********************************************************************************
 * Copyright (c) 2017-2020 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
//! @file  vehicleModelsInterface.h
//! @brief This file provides the interface of the vehicle model container.
//-----------------------------------------------------------------------------

#pragma once

#include <unordered_map>
#include <algorithm>
#include <cmath>

#include "common/globalDefinitions.h"
#include "common/openScenarioDefinitions.h"

//! Resolves a parametrized attribute
//!
//! \param attribute                attribute is defined in the catalog
//! \param parameterAssignments     parameter assignments in the catalog reference
//! \return
template <typename T>
T GetAttribute(openScenario::ParameterizedAttribute<T> attribute, const openScenario::Parameters& parameterAssignments)
{
    const auto& assignedParameter = parameterAssignments.find(attribute.name);
    if (assignedParameter != parameterAssignments.cend())
    {
        auto valueString = std::get<std::string>(assignedParameter->second);
        if constexpr (std::is_same_v<T, std::string>)
        {
            return valueString;
        }
        try
        {
            if constexpr (std::is_same_v<T, double>)
            {
                return std::stod(valueString);
            }
            else if constexpr (std::is_same_v<T, int>)
            {
                return std::stoi(valueString);
            }
        }
        catch (const std::invalid_argument&)
        {
            throw std::runtime_error("Type of assigned parameter \"" + attribute.name + "\" in scenario does not match.");
        }
        catch (const std::out_of_range&)
        {
            throw std::runtime_error("Value of assigned parameter \"" + attribute.name + "\" is out of range.");
        }
    }
    else
    {
        return attribute.defaultValue;
    }
}

//! Contains the VehicleModelParameters as defined in the VehicleModelCatalog.
//! Certain values may be parametrized and can be overwriten in the Scenario via ParameterAssignment
struct ParametrizedVehicleModelParameters
{
    AgentVehicleType vehicleType;           ///< type of agent vehicle

    /// @brief center of bounding box
    struct BoundingBoxCenter
    {
        openScenario::ParameterizedAttribute<double> x; ///< x coordinate of bounding box center
        openScenario::ParameterizedAttribute<double> y; ///< y coordinate of bounding box center
        openScenario::ParameterizedAttribute<double> z; ///< z coordinate of bounding box center

        /**
         * @brief Get bounding box center
         * 
         * @param assignedParameters open scenario parameters
         * @return bounding box center
         */
        VehicleModelParameters::BoundingBoxCenter Get(const openScenario::Parameters& assignedParameters) const
        {
            return {GetAttribute(x, assignedParameters),
                    GetAttribute(y, assignedParameters),
                    GetAttribute(z, assignedParameters)};
        }
    } boundingBoxCenter;    ///< center of bounding box

    /// @brief Bounding box dimensions
    struct BoundingBoxDimensions
    {
        openScenario::ParameterizedAttribute<double> width;     ///< width of bounding box
        openScenario::ParameterizedAttribute<double> length;    ///< length of bounding box
        openScenario::ParameterizedAttribute<double> height;    ///< height of bounding box

        /**
         * @brief Get bounding box dimensions
         * 
         * @param assignedParameters openscenario parameters
         * @return bounding box dimensions 
         */
        VehicleModelParameters::BoundingBoxDimensions Get(const openScenario::Parameters& assignedParameters) const
        {
            return {GetAttribute(width, assignedParameters),
                    GetAttribute(length, assignedParameters),
                    GetAttribute(height, assignedParameters)};
        }
    } boundingBoxDimensions;    ///< bounding box dimensions

    /// @brief performance of the Vehicle
    struct Performance
    {
        openScenario::ParameterizedAttribute<double> maxSpeed;          ///< maximum speed of the vehicle
        openScenario::ParameterizedAttribute<double> maxAcceleration;   ///< maximum acceleration of the vehicle
        openScenario::ParameterizedAttribute<double> maxDeceleration;   ///< maximum deceleartion of the vehicle

        /**
         * @brief Get performance attributes of the vehicle
         * 
         * @param assignedParameters openscenario parameters
         * @return max speed, max acceleration, max deceleration of the vehicle
         */
        VehicleModelParameters::Performance Get(const openScenario::Parameters& assignedParameters) const
        {
            return {GetAttribute(maxSpeed, assignedParameters),
                    GetAttribute(maxAcceleration, assignedParameters),
                    GetAttribute(maxDeceleration, assignedParameters)};
        }
    } performance;  ///< performance attributes of the vehicle

    /// @brief Axle parameters of the vehicle
    struct Axle
    {
        openScenario::ParameterizedAttribute<double> maxSteering;   //!< Maximum steering angle which can be performed by the wheels on this axle
        openScenario::ParameterizedAttribute<double> wheelDiameter; //!< Diameter of the wheels on this axle
        openScenario::ParameterizedAttribute<double> trackWidth;    //!< Distance of the wheels center lines at zero steering
        openScenario::ParameterizedAttribute<double> positionX;     //!< Longitudinal position of the axle with respect to the vehicles reference point
        openScenario::ParameterizedAttribute<double> positionZ;     //!< Z-position of the axle with respect to the vehicles reference point. Usually this is half of wheel diameter

        /**
         * @brief Get axle parameter of the vehicle model
         * 
         * @param assignedParameters openscenario parameters
         * @return axle parameters of the vehicle
         */
        VehicleModelParameters::Axle Get(const openScenario::Parameters& assignedParameters) const
        {
            return {GetAttribute(maxSteering, assignedParameters),
                    GetAttribute(wheelDiameter, assignedParameters),
                    GetAttribute(trackWidth, assignedParameters),
                    GetAttribute(positionX, assignedParameters),
                    GetAttribute(positionZ, assignedParameters)};
        }
    };
    Axle frontAxle; ///< front axle
    Axle rearAxle;  ///< rear axle

    //! Container for axle, geometric and performance properties of the vehicle
    std::map<std::string, openScenario::ParameterizedAttribute<double>> properties;

    /**
     * @brief Get vehicle model parameters
     * 
     * @param assignedParameters    Parameter assignments in the catalog reference
     * @return vehicle model parameters 
     */
    VehicleModelParameters Get(const openScenario::Parameters& assignedParameters) const
    {
        std::map<std::string, double> assignedProperties;   
        for (const auto[key, value] : properties)
        {
            assignedProperties.insert({key, GetAttribute(value, assignedParameters)});
        }

        return {vehicleType,
                boundingBoxCenter.Get(assignedParameters),
                boundingBoxDimensions.Get(assignedParameters),
                performance.Get(assignedParameters),
                frontAxle.Get(assignedParameters),
                rearAxle.Get(assignedParameters),
                assignedProperties
        };
    }
};

//! Mapping from vehicle model name to its assigned parameters
using VehicleModelMap = std::unordered_map<std::string, ParametrizedVehicleModelParameters>;

/// @brief Interface representing vehicle models
class VehicleModelsInterface
{
public:
    VehicleModelsInterface() = default;
    ~VehicleModelsInterface() = default;

    /// @return Returns a map of name of the vehicle model and its assigned parameters, that overwrite the default parameters
    virtual const VehicleModelMap& GetVehicleModelMap() const = 0;

    //! Add the VehicleModel with the specified name
    //!
    //! \param name                 name of the vehicle model
    //! \param vehicleModel         assigned parameters, that overwrite the default parameters
    virtual void AddVehicleModel(const std::string& name, const ParametrizedVehicleModelParameters& vehicleModel) = 0;

    //! Returns the VehicleModel with the specified name
    //!
    //! \param vehicleModelType     name of the vehicle model
    //! \param parameters           assigned parameters, that overwrite the default parameters
    //! \return vehicle model parameters
    virtual VehicleModelParameters GetVehicleModel(std::string vehicleModelType, const openScenario::Parameters& parameters = {}) const = 0;
};
