..
  *******************************************************************************
  Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _sim_install_guide:

Getting Started
===============

This guide will lead you through the installation steps to set up the |op| simulator and run your first simulation. 
The process is described for Windows and Linux as target platforms. 
Please check the :ref:`system_requirements` for the required hardware setup.  

The installation manual is structured as follows:

.. tabs::

  .. tab:: Building manually

    **Setting up your system**

    - :ref:`Prerequisites` contains instructions for installing the thirdparty software required by |op|

    **Installation of the simulator**

    - :ref:`Download_and_install_openpass` describes how to compile and run |op| once all prerequisites are installed
    
  .. tab:: Building with Conan

    .. warning:: Not Recommended to install this way. Not finished and fully tested yet.

    **Setting up Conan** 
  
    - :ref:`building_with_conan` contains the instructions to set Conan up.

    **Installation of the simulator**

    - :ref:`building_with_conan` describes how to compile and run |op| once all prerequisites are installed


**Run a default simulation**

- :ref:`tutorials` shows how to run your first simulations

**Further guidance**

- :ref:`Cmake` holds information about available variables and options for the cross-platform build environment CMake
- :ref:`Qtcreator` describes the basic setup for QtCreator
- :ref:`Vscode` describes the basic setup for Visual Studio Code
